# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Solving the optimal area polygonalization problem. 
Given a set S of n points in the plane, compute a simple polygonalization of S (a simple polygon whose vertex set is precisely the set S) that has maximum or minimum area among all polygonalizations of S. 
(Every set S of n points in the plane has at least one polygonalization. The number of different polygonalizations is finite (though possibly exponentially large) for any finite points set S.) 
The optimization problems, both for maximization or for minimization, are known to be NP-hard; see S. P. Fekete, On Simple Polygonalizations with Optimal Area, Discrete and Computational Geometry 23:73-110 (2000). 
Thus, the Challenge encourages implementations of solutions that are based on heuristics, on methods of combinatorial optimization, guided search, etc.

### How do I get set up? ###

* Install CGAL and its dependencies
* Install Python
* Install CGAL Python bindings

### Contribution guidelines ###

Do not push generated files, e.g., executables.

### Who do I talk to? ###

efifogel@gmail.com