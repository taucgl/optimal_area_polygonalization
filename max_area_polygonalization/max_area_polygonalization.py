from tkinter import *
import pickle
import functools
import re
import itertools
from Polygon_2 import *

def pointCompare(p1, p2):
  xDiff = p1[0]-p2[0]
  if(xDiff != 0):
    return xDiff
  else:
    return p1[1]-p2[1]



class Polygon(Frame):
  polygon = Polygon_2()
  vertexCount = 0
  vertexLst = []
  vertexTup = ()
  pointSize = 8
  lineWidth = 2
  scale = 1
  filename = ""
  solution = ""
  permTxt = ""
  width = 800
  height = 600
  center = (width/2, height/2)
  xOffset = 0
  yOffset = 0
  try:
    d = pickle.load(open("maxAreas.db.pickle", "rb"))
  except FileNotFoundError:
    d = {}

  def permutations(self, n = 10):
    self.getVertices((1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    self.drawVertices()
    for p in itertools.permutations((list(i for i in range(1, n + 1)))[1:3]):
      self.getVertices(p)
      self.displayInfo()
      pass

  def getVertices(self, perm = ()):
    self.clear()
    self.filename = self.inputBox.get()
    self.solution = self.solutionBox.get()
    try:
      input = open(self.filename, "r")
    except FileNotFoundError:
      self.clear()
      self.updateText("invalid input file name")
      return
    for line in input:
      if not line[0] == "#":
        self.vertexCount += 1
        c = line.split()
        x = int(c[1])
        y = int(c[2])
        self.vertexLst.append((x, y))
    input.close()
    self.vertexTup = tuple(self.vertexLst)
    self.vertexLst = []
    if self.solution != "" and perm == ():
      try:
        sol = open(self.solution)
        self.permTxt = sol.readline()
      except FileNotFoundError:
        self.clear()
        self.updateText("invalid solution file name")
        return
      perm = self.permTxt.split()
      if len(perm) != len(self.vertexTup):
        self.clear()
        self.updateText("invalid solution file")
        sol.close()
        return
    else:
      perm = (i for i in range(1, len(self.vertexTup)+1))
    for i in perm:
      self.vertexLst.append(self.vertexTup[int(i)-1])
    self.createPolygon()

  def createPolygon(self):
    temp = []
    for p in self.vertexLst:
      x = p[0]
      y = p[1]
      p2 = Point_2(x,y)
      temp.append((x,y))
      self.polygon.push_back(p2)
    self.vertexLst = temp
    bbox = self.polygon.bbox()
    self.scale = min((self.width-20)/(bbox.xmax()-bbox.xmin()), (self.height-20)/(bbox.ymax()-bbox.ymin()))
    center = ((bbox.xmax()+bbox.xmin())//2, (bbox.ymax()+bbox.ymin())//2)

    self.xOffset = 0
    self.yOffset = 0

    self.xOffset = (self.center[0] - center[0] * self.scale)
    self.yOffset = (self.center[1] - center[1] * self.scale)

  def displayInfo(self):
    simple = self.polygon.is_simple()
    area = abs(self.polygon.area())
    currentMax = 0
    currentMaxFileName = ""
    if self.vertexTup in self.d:
      currentMax = self.d[self.vertexTup][0]
      currentMaxFileName = self.d[self.vertexTup][1]
      if simple and area > currentMax:
        currentMax = area
        currentMaxFileName =  self.filename + ".best"
        s = open(currentMaxFileName, "w")
        s.write(self.permTxt)
        s.close()
        self.d[self.vertexTup] = (area, currentMaxFileName, tuple(self.vertexLst))
    else:
      currentMax = 0
      currentMaxFileName = ""
      if simple and area > 0:
        currentMax = area
        currentMaxFileName = self.filename + ".best"
        s = open(currentMaxFileName, "w")
        s.write(self.permTxt)
        print("1")
        s.close()
        self.d[self.vertexTup] = (area, currentMaxFileName, tuple(self.vertexLst))
    s = ""
    s += "simple: " + str(simple) + "\n"
    s += "area: " + str(area) + "\n"
    s += "current max area for these vertices: " + str(int(currentMax)) + "\n"
    s += "found in: " + currentMaxFileName + "\n"
    self.updateText(s)


  def updateText(self, s):
    self.clearText()
    self.infoText.config(state=NORMAL)
    self.infoText.insert(END, s, "center")
    self.infoText.config(state=DISABLED)

  def drawVertices(self):
    if self.vertexCount == 0: return
    x, y = self.center[0], self.center[1]
    #self.canvas.create_oval(x, y, x + self.pointSize, y + self.pointSize, fill="green", width=1)
    for p in self.vertexLst:
      x, y = (p[0]+self.xOffset) * self.scale, (p[1]+self.yOffset) * self.scale
      self.canvas.create_oval(x, y, x + self.pointSize, y + self.pointSize, fill="red", width=1)


  def drawPolygon(self):
    if self.vertexCount == 0: return
    lst = []
    for p in self.vertexLst:
      lst.append((p[0]+self.xOffset) * self.scale + self.pointSize / 2)
      lst.append((p[1]+self.yOffset) * self.scale + self.pointSize / 2)
    lst = tuple(lst)
    polygon = self.canvas.create_polygon(lst, fill="yellow", outline="blue", width=self.lineWidth)
    self.canvas.tag_lower(polygon)

  def initialize(self):
    self.getVertices()
    self.drawVertices()

  def clear(self):
    self.canvas.delete("all")
    self.clearText()
    self.vertexLst = []
    self.vertexCount = 0
    self.polygon = Polygon_2()
    self.xOffset = 0
    self.yOffset = 0

  def clearText(self):
    self.infoText.config(state=NORMAL)
    self.infoText.delete("1.0", END)
    self.infoText.config(state=DISABLED)

  def onClosing(self):
    pickle.dump(self.d, open("maxAreas.db.pickle", "wb"))
    root.destroy()

  def createWidgets(self):
    self.winfo_toplevel().title("Polygon")

    self.inputLabel = Label(self, text = "Input file:")
    self.inputLabel.pack()

    self.inputBox = Entry(self, justify='center')
    self.inputBox.insert(END, "uniform-0000010-1.instance")

    self.inputBox.pack()

    self.solutionLabel = Label(self, text="Solution file:")
    self.solutionLabel.pack()

    self.solutionBox = Entry(self, justify='center')
    self.solutionBox.insert(END, "")

    self.solutionBox.pack()

    self.initializeButton = Button(self)
    self.initializeButton["text"] = "Initialize"
    self.initializeButton["command"] = self.initialize

    self.initializeButton.pack()

    self.drawButton = Button(self)
    self.drawButton["text"] = "Draw Polygon"
    self.drawButton["command"] = self.drawPolygon

    self.drawButton.pack()

    self.infoButton = Button(self)
    self.infoButton["text"] = "Information"
    self.infoButton["command"] = self.displayInfo

    self.infoButton.pack()

    self.infoText = Text(self, height=4, width=70)
    self.infoText.pack()
    self.infoText.insert(END, "", "center")
    self.infoText.tag_configure("center", justify='center')
    self.infoText.config(state=DISABLED)

    # self.naiveButton = Button(self)
    # self.naiveButton["text"] = "naive solution"
    # self.naiveButton["command"] = self.permutations
    #
    # self.naiveButton.pack()

    self.clearButton = Button(self)
    self.clearButton["text"] = "Clear"
    self.clearButton["command"] = self.clear

    self.clearButton.pack()

    self.canvas = Canvas(self, height=self.height, width=self.width, bg = "white")
    self.canvas.pack(fill=BOTH, expand=1)

  def __init__(self, master=None):
    Frame.__init__(self, master)
    self.pack()
    self.createWidgets()


root = Tk()
app = Polygon(master=root)
root.protocol("WM_DELETE_WINDOW", app.onClosing)
app.mainloop()