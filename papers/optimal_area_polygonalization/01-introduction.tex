% =============================================================================
\section{Introduction}
\label{sec:introduction}
% =============================================================================
Given a set of points $\mathcal{S}$ in the plane, finding a simple
polygon $P$ of minimum area, such that every point in $\mathcal{S}$
appears exactly once in the boundary of $P$, is referred to as the
\emph{Min-Area Polygonization} (MnAP) problem. The similar problem,
where the objective is finding a polygon of maximum area, is referred
to as the \emph{Max-Area Polygonization} (MxAP) problem.

Both MnAP and MxAP are combinatorial optimization problems; here we
need to find an optimal object from a finite set of objects.  Every
set of $n$ non-collinear points admits at least one polygonization.
However, the number of different polygonizations of a set of $n$
points is large. The trivial bound is $(n-1)!/2$. The quest for upper
and lower bounds on this number has a long history; see,
e.g.,~\cite{ssw-cpgpm-13}, and the references therein. Both MnAP and
MxAP problems are NP-complete~\cite{f-spoa-20}.  Therefore, exhaustive
search is not tractable. Several approaches to solve combinatorial
optimization problems with non-tractable search spaces, also referred
to as metaheuristics, have been proposed~\cite{gp-im-03}. We have
opted for a variant of \emph{simulated annealing}
(SA)~\cite{kgv-osa-83} (thus named because it mimics the process
undergone by misplaced atoms in a metal when it is heated and then
slowly cooled).  Many of these approaches consist of two phases, and
SA is not an exception. In the first phase a feasible initial object
(a.k.a. state) is found. The next phase consists of a sequence of
iterations that aims to converge to an optimal state. (While these
techniques are unlikely to find an optimum solution, they can often
find a very good solution.) Each iteration accepts a feasible state as
input, applies a transition step, and results in a different
feasible state, hopefully but not necessarily, of better quality
compared to the input state. For simplicity of exposition, we assume
henceforth that every iteration results in a feasible (valid) state;
on rare occasions and for particularly small inputs, this may not be
the case (and this can be easily detected and handled). However, for
all our experiments reported below and for all the contest inputs, an
iteration always ended with a valid new state.

Simulated annealing improves the exhaustive search strategy through
the introduction of two key elements. The first is the so-called
``Metropolis criterion''~\cite{mrrtt-escfc-53}, in which some
transitions that do not improve the state are accepted, as they enable
the further exploration of the solution space. Such ``bad''
transitions are allowed under the condition that
\begin{equation}\label{eqn:sa}
  e^{(-\Delta D/T)} > R(0,1),
\end{equation}
where $\Delta D$ is the change of quality implied by the transition
(negative for a ``good'' transition; positive for a ``bad''
transition), $T$ is a ``synthetic temperature'', and $R(0,1)$ is a
random number in the interval $[0,1]$. $D$ is called a ``cost
function'', and corresponds to the excess energy in the case of
annealing a metal (in which case the temperature parameter would
actually be $k T$, where $k$ is Boltzmann's Constant and $T$ is the
physical temperature, in the Kelvin absolute temperature scale). If
$T$ is large, many ``bad'' transitions are accepted, and a large part
of the solution space is accessed.
% States to be traded are generally chosen randomly, though more
% sophisticated techniques can be used.
%
The second key element is, again by analogy with annealing of a metal,
the lowering of the ``temperature''. After making many transitions and
observing that the cost function declines only slowly, one lowers the
temperature, and thus limits the number of allowed ``bad''
transitions. After lowering the temperature several times, one may
then ``quench'' the process by accepting only ``good'' transitions in
order to find the local minimum of the cost function. There are
various ``annealing schedules'' for lowering the temperature. Using
different settings can increase the convergence rate.
%% , but the
%% results are generally not very sensitive to the details.
%% \dan{we need a reference to justify this, or remove this clause.}

We have created an application that computes near-optimal solutions
for the MnAP and MxAP problems. The application consists of a modular
pipeline formed of several steps, where each step can be implemented
in various ways and is governed by several parameters. The first step
in the pipeline accepts as input a set of points in the plane and
produces as output a simple polygon defined by the input set. All
other steps accept as input a simple polygon and produce as output a
different simple polygon defined by the same set of points. The code
depends on \cgal~\cite{cgal:eb-20} (Computational Geometry Algorithm
Library). In particular, we have used the Linear Geometry Kernel
package~\cite{cgal:bfghhkps-lgk23-20}, the \kdtree{} search structure
provided by the ``dD Spatial Searching''
package~\cite{cgal:tf-ssd-20}, and the convex hull computation from
the ``2D Convex Hulls and Extreme Points''
package~\cite{cgal:hs-chep2-20a}.  The code resides in a git
repository publicly accessible at
\url{https://bitbucket.org/taucgl/optimal_area_polygonalization/}; see
the Wiki section within for instructions for building the application
and executing it. We have used our application to compete in a contest
that was part of the CG Challenge~2019~\cite{dfkkm-aosp-21} and won
third place. For the other triumphant participants
see~\cite{lms-oaptr-21,cfg-glssm-21,ehjmp-tmfao-21,rjrsu-haop-21}.

In this article we describe the various algorithms we used and the
considerations we took into account while implementing these algorithms,
and executing their implementations.  We also list the results that we
have obtained while participating in the contest.  Finally, we mention
several ideas for improving our application, which we have conceived,
but have not had the time to explore yet, and conclude with final remarks.
