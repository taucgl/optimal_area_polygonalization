% =============================================================================
\section{Results}
\label{sec:results}
% =============================================================================
We have divided the entire set of instances into subsets of identical
point-set sizes. Notice that the subset of instances of the largest
size, that is~1,000,000 points, consists of a single member. All the
experiments described in this section apply to these subsets. Given a
subset $\mathcal{I}$ of a specific size, we either compute the average
of the percentage coverage ratio for the members of $\mathcal{I}$ or
provide the worst coverage ratio or the best coverage ratio. For MnAP
(resp. MxAP) the worst and best are the largest (resp. smallest) and
smallest (resp. largest), respectively. We used an Intel Core i5
clocked at~3.5GHz with~16GB of RAM to obtain the benchmarks; see
Figure~\ref{fig:bench} and Figure~\ref{fig:bench:spatial}.

\begin{figure}[!ht]
  %\vspace{0pt}
  \centering%
  \subfloat[]{\label{fig:energy:MnAP}%
    \begin{tikzpicture}[x=0.001cm,y=0.1cm]
      \begin{axis}[
          ymin=0,
          xmode=log,
          log ticks with fixed point,
          % for log axes, x filter operates on LOGS.
          % and log(x * 1000) = log(x) + log(1000):
          x filter/.code=\pgfmathparse{#1},
          yticklabel={\pgfmathprintnumber{\tick}\%},
          xlabel=Point set size,
          % ylabel=Average ratio percentage
        ]
        \addplot[mark=*,mark size=1pt] table {statistics-min.txt};
        \legend{MnAP}
      \end{axis}
    \end{tikzpicture}%
  }
  \subfloat[]{\label{fig:energy:MxAP}%
    \begin{tikzpicture}[x=0.0009cm,y=0.1cm]
      \begin{axis}[
          ymax=100,
          xmode=log,
          log ticks with fixed point,
          % for log axes, x filter operates on LOGS.
          % and log(x * 1000) = log(x) + log(1000):
          x filter/.code=\pgfmathparse{#1},
          yticklabel={\pgfmathprintnumber{\tick}\%},
          xlabel=Point set size,
          % ylabel=Average ratio percentage
        ]
        \addplot[mark=*,mark size=1pt] table {statistics-max.txt};
        \legend{MxAP}
      \end{axis}
    \end{tikzpicture}%
  }
  \caption[]{The percentage computed coverage ratio, averaged over the
    different instances of the same input size, as a function of the
    point set size for MnAP (\subref*{fig:energy:MnAP}), and for MxAP
    (\subref*{fig:energy:MxAP}). Note that the horizontal axis is on a
    logarithmic scale.}
  \label{fig:energy}
\end{figure}

Figure~\ref{fig:energy:MnAP} shows a graph of the computed coverage
ratio $\coverage(\minBestPoly)$, averaged over the different instances
of the same input size, as a function of the input set size for
MnAP. The graph is roughly monotone decreasing in the range where the
point set size is smaller than~1,000. We believe that the graph of the
true optimum is monotone decreasing throughout (at least for points
chosen uniformly at random). Notice though that there are cases where
adding a point to a given set of points $\mathcal{S}$ inside the
convex hull of $\mathcal{S}$ increases the area of the minimum-area
polygon of $\mathcal{S}$; see Figure~\ref{fig:add-point}.  In our
results the graph increases past~1,000 points probably due to
limitations of the methodology.  Similarly,
Figure~\ref{fig:energy:MxAP} shows a graph of the computed coverage
ratio $\coverage(\maxBestPoly)$, averaged over the different instances
of the same input size, as a function of the input set size for MxAP.
Observe that spatial subdivision was not applied for instances of
sizes smaller than~1,000. The sudden increase of the coverage
for~1,000,000 points can be accounted for the fact that, as noted
above, the set of instances includes only a single input set
with~1,000,000 points, which were not uniformly sampled at random, as
well as for the relatively large amount of sources we invested in this
case.  The computed coverage ratio $\coverage(\minBestPoly)$, averaged
over the different instances of the same input size, as a function of
the input set size, behaves differently for the MnAP and MxAP
problems. It hints, that the same strategy is not equally suitable for
the two different problems, and a better approach would have been to
treat each of them separately.

\begin{figure}[!ht]
  \begin{tikzpicture}
    \def\n{6}
    \def\radius{2cm}
    \foreach \s in {0,...,\n}{
      \coordinate (p\s) at ({360/(\n+2) * (\s - 1)}:\radius);%
      \coordinate (q\s) at ({360/(\n+2) * (\s - 1)}:\radius-0.3cm);%
    }
    \draw [fill=\pgncolor]
    (p0)
    \foreach \i in {1,...,\n}{ -- (p\i) }
    \foreach[evaluate=\i as \j using int(\n-\i)] \i in {0,...,\n}{ -- (q\j) }
    -- cycle;
    \foreach \i in {0,...,\n}{%
      \node[point] at (p\i){};
      \node[point] at (q\i){};
    }
    \node[point=blue,label=60:{$r$}] at (0,0){};
  \end{tikzpicture}
  \caption[]{The polygonization of~14 points with the minimum coverage ratio.
    Adding the new point $p$ increases the minimum coverage ratio.}
  \label{fig:add-point}
\end{figure}

\begin{figure}[!ht]
  \pgfplotstableread[col sep = comma]{global_summary.csv}{\tableMaxOps}
  \pgfplotsset{%
    log y ticks with fixed point/.style={%
      yticklabel={%
        \pgfkeys{/pgf/fpu=true}
        \pgfmathparse{0.001*exp(\tick)}%
        \pgfmathprintnumber[fixed relative, precision=3]{\pgfmathresult}
        \pgfkeys{/pgf/fpu=false}
      }}}
  \subfloat[]{\label{fig:bench:score}%
  \begin{tikzpicture}[]
   \begin{axis}[width=7cm,view={30}{30},
        x tick label style={rotate=30,anchor=east,outer sep=2pt},
        xtick=data,
        xlabel style={sloped,outer sep=-4pt},
        ylabel style={sloped,outer sep=-4pt},
        xlabel=\textbf{Point set size},
        ymode=log,
        log y ticks with fixed point,
        % log ticks with fixed point,
        ytick={5000,10000,20000,40000,80000,160000},
        y tick label style={rotate=-10,anchor=west,outer sep=2pt},
        ylabel style={sloped,outer sep=-4pt},
        ylabel=\textbf{\# of steps in thousands},
        ztick={80,90,100},
        zmax=100,
        zticklabel={\pgfmathprintnumber{\tick}\%},
        zlabel=\textbf{Average ratio percentage}
      ]
      \addplot3[surf,mesh/ordering=y varies,mesh/rows=6] table[x={nop},y={nos},z={score}] {\tableMaxOps};
    \end{axis}
  \end{tikzpicture}}
  \subfloat[]{\label{fig:bench:time}%
  \begin{tikzpicture}[]
    \begin{axis}[width=7cm,view={30}{30},
        x tick label style={rotate=30,anchor=east,outer sep=2pt},
        xtick=data,
        xlabel style={sloped,outer sep=-4pt},
        xlabel=\textbf{Point set size},
        ymode=log,
        log y ticks with fixed point,
        ytick={5000,10000,20000,40000,80000,160000},
        y tick label style={rotate=-10,anchor=west,outer sep=2pt},
        ylabel style={sloped,outer sep=-4pt},
        ylabel=\textbf{\# of steps in thousands},
        zlabel=\textbf{Running time in secs},
        zticklabel pos=upper,
      ]
      \addplot3[surf,mesh/ordering=y varies,mesh/rows=6] table[x={nop},y={nos},z={time}] {\tableMaxOps};
    \end{axis}
  \end{tikzpicture}}
  \caption[]{The percentage computed coverage ratio
    (\subref*{fig:bench:score}) and the running times
    (\subref*{fig:bench:time}) as a function of the point set size and
    the number of successive global transition steps for MxAP.
    Note that the $y$-axis (which measures steps in thousands) is on a
    logarithmic scale.}
  \label{fig:bench}
\end{figure}

We have created benchmarks using the uniform instances with point-set
sizes of $100,200,\ldots,900$ for the global transition steps we
implemented for MxAP. The benchmarks show the effect of the input size
and the number of successive steps on (i) the quality of the result
(Figure~\ref{fig:bench:score}), and (ii) the running times
(Figure~\ref{fig:bench:time}). Naturally, increasing the number of
steps improves (increases for MxAP) the coverage ratio. An examination
of the numbers reveals that the coverage ratio is approximately
$n^{0.05}$ where $n$ is the number of steps for the point set size of~900. This
figure monotonically decreases with the decrease of the point set
size, and reaches approximately~$n^{0.01}$ for the size
of~100. Evidently, the time consumption is approximately linear in the
number of steps and quadratic in the point set size, as expected.

% Spatial subdivision.
\begin{figure}[!ht]
  \pgfplotstableread[col sep = comma]{spatial_summary.csv}{\tableMaxSpatial}
  \subfloat[t][]{\vspace{0pt}\label{fig:bench:spatial:chart}%
  \begin{tikzpicture}[]
    \begin{axis}[height=6cm,name=plotSpatialMax,
        xmin=50,
        xmax=500,
        ymax=82,
        yticklabel={\pgfmathprintnumber{\tick}\%},
        xtick=data,
        xlabel=Subset size,
      ]
      \addplot[mark=*,mark size=1pt] table[x={method},y={score}] {\tableMaxSpatial};
      \legend{MxAP}
    \end{axis}
  \end{tikzpicture}%
  }\quad
  \subfloat[t][]{\vspace{0pt}\label{fig:bench:spatial:table}%
    %\begin{minipage}[b]{0.4\textwidth}
    \raisebox{2.8cm}{
    \begin{tabular}{|r|r|r|r|}
      \hline
      \multicolumn{1}{|c}{\textbf{Subset}} &
      \multicolumn{1}{|c}{\textbf{\# of Global}} &
      \multicolumn{1}{|c}{\textbf{Time}} &
      \multicolumn{1}{|c|}{\multirow{2}{*}{\textbf{Coverage \%}}}\\
      \multicolumn{1}{|c}{\textbf{Size}}&
      \multicolumn{1}{|c}{\textbf{Steps}} &
      \multicolumn{1}{|c|}{\textbf{(secs)}} &\\
      \hline
      \hline
       50 & 50000 & 444.152 & 75.7512\\
      100 & 25000 & 377.554 & 79.5115\\
      150 & 16667 & 413.646 & 80.5798\\
      200 & 12500 & 417.640 & 80.2118\\
      250 & 10000 & 394.022 & 79.7532\\
      300 &  8333 & 422.329 & 78.8530\\
      350 &  7143 & 450.608 & 77.9252\\
      400 &  6250 & 398.978 & 76.9165\\
      450 &  5556 & 430.564 & 75.0562\\
      500 &  5000 & 407.572 & 74.7731\\
      \hline
    \end{tabular}}
  }
  \caption[]{(\subref*{fig:bench:spatial:chart}) The percentage
    coverage ratio as a function of the subset size of
    different subdivisions for MxAP (computed only for
    \texttt{uniform-0002000-1.instance}).
    (\subref*{fig:bench:spatial:table}) The same information including
    the number of global steps and the running times.}
  \label{fig:bench:spatial}
\end{figure}

We have also created benchmarks using the
\texttt{uniform-0002000-1.instance} input file (which has 2,000~points)
to measure the effect of the size of the subproblems in the spatial
subdivision (see Section~\ref{ssec:algorithms:spatial-subdivision}) on
the quality of the results for MxAP. Recall that solving each
subproblem is done by applying global steps, as these steps are more
effective. Observe that as long as the sizes of the subproblems are
limited, the running times remain under control. In an attempt to obtain
comparable benchmarks with approximately the same running times we
have linearly decreased the number of steps with the increase in the
subproblem size; see Table~\ref{fig:bench:spatial:table}. As can be
seen in Figure~\ref{fig:bench:spatial:chart}, dividing the input into
subproblems of~150 points each yields the best coverage ratio for this
specific instance and for a specific amount of allotted time.  The
optimal subproblem size is likely to grow with the increase in the
number of global steps (and the consequent growth in running
times). Also, the overhead and potential loss caused by the merging
process lessens with the growth of the subproblem size.  Indeed,
for the competition we used different subproblem sizes for different
instances based on their total size; see
Section~\ref{ssec:engineering:tuning}.

% Summary of all results
\begin{table}[!ht]
  \caption[]{Our results for the polygonization of several
    instances provided as part of the CG:SHOP~2019 contest.}
  \label{tab:table}
  \centering
  \begin{tabular}{|l|r|r|r|r|r|}
    \hline
    \multicolumn{1}{|c|}{\multirow{2}{*}{\textbf{Instance Name}}} &
    \textbf{Subset} &
    \multicolumn{2}{|c|}{\textbf{MnAP}} &
    \multicolumn{2}{|c|}{\textbf{MxAP}}\\\cline{3-4}\cline{5-6}
    & \multicolumn{1}{c}{\textbf{size}} & \multicolumn{1}{|c|}{\textbf{Coverage \%}} &
    \textbf{\# of global steps} &
    \multicolumn{1}{|c|}{\textbf{Coverage \%}} &
    \multicolumn{1}{|c|}{\textbf{\# of global steps}}\\
    \hline
    \hline
    euro-night-0000100 & 100 & 12.49 & 100,000 & 93.19 & 100,000\\
    paris-0001000      & 500 & 9.30 & 1,000,000 & 87.81 & 1,000,000\\
    uniform-0010000-1  & 200 & 16.55 & 1,000,000 & 80.53 & 1,000,000\\
    world-0010000      & 250 & 9.85 & 2,000,000 & 87.15 & 2,000,000\\
    sun-0050000        & 200 & 12.13 & 5,000,000 & 72.56 & 5,000,000\\
    uniform-0100000-2  & 150 & 16.62 & 13,333,000 & 79.86 & 13,333,000\\
    mona-lisa-1000000  & 100 & 17.52 & 100,000,000 & 77.55 & 100,000,000\\
    \hline
  \end{tabular}
\end{table}

We executed our application in a High Throughput Computing (HTC)
environment controlled by a software tool called
HTCondor\footnote{https://research.cs.wisc.edu/htcondor/} and
supported by the school of Computer Science at Tel Aviv
University. The tool, among other things, harnesses wasted CPU power
from otherwise idle workstations, and enables grid computing. The
contest included 247 instances. We submitted two jobs for each
instance, which executed our application to solve the MnAP and MxAP
problems, respectively. We saved the results and then executed
additional jobs this time without applying spatial subdivision and
bypassing the computation of the initial state. We repeated the above
for instances that exhibit progress above a certain
threshold. Table~\ref{tab:table} lists selected
results. Figure~\ref{fig:res} shows the final results as bar charts.
%% 32.323772900000016
%% 0.0481567 0.1308654773279353 0.351727
%% 207.77869900000002
%% 0.565774 0.8412093076923077 0.970415

\begin{figure}[!ht]
  \pgfdeclareplotmark{rect}{%
    \pgfpathmoveto{\pgfpoint{0}{-0.09cm}}
    \pgfpathlineto{\pgfpoint{0}{0.09cm}}
    \pgfsetlinewidth{1}
    \pgfusepath{stroke}
  }
  \pgfplotsset{basicPlot/.style={mark=rect}}
  \pgfplotsset{bestPlot/.style={basicPlot,draw=blue}}
  \pgfplotsset{averagePlot/.style={basicPlot,draw=gray}}
  \pgfplotsset{worstPlot/.style={basicPlot,draw=orange}}
  \pgfplotsset{basicAxis/.style={width=0.5\linewidth,only marks}}
  \pgfplotsset{mainAxis/.style={height=12cm,
      xtick style={draw=none},xtick=\empty,ymin=0,ymax=43,ytick=data,
      y tick label style={%
        font=\scriptsize,
        /pgf/number format/.cd,set thousands separator={,}},
      yticklabels from table={\tablemin}{[index] 1},
      ylabel={\textbf{Point set size}}}}
  \pgfplotsset{summaryAxis/.style={anchor=north,height=2.05cm,
      xticklabel={\pgfmathprintnumber{\tick}\%},
      xlabel={\textbf{Coverage ratio: \textcolor{blue}{best}, average, \textcolor{orange}{worst}}},
      ymin=0,ymax=2,y tick label style={font=\small},ytick=data,
      yticklabels={{Summary}}
  }}
  \pgfplotsset{minAxis/.style={xmin=0,xmax=50}}
  \pgfplotsset{maxAxis/.style={xmin=50,xmax=100,yticklabel pos=upper}}
  \pgfplotstableread[col sep=space]{statistics-max.csv}{\tablemax}
  \pgfplotstableread[col sep=space]{statistics-min.csv}{\tablemin}
  \pgfplotstableread[col sep=space]{
1 56.5774 84.12093076923077 97.0415
  }\tablemaxSum
  \pgfplotstableread[col sep=space]{
1 4.81567 13.08654773279353 35.1727
  }\tableminSum
  \centering
  %%%%%%%% MnAP %%%%%%%%
  \subfloat[\textbf{MnAP}]{\label{fig:res:min}%
    \begin{tikzpicture}[]
      \begin{axis}[name=plotmin,basicAxis,minAxis,mainAxis]
        \addplot[worstPlot] table[x index=4,y index=0]{\tablemin};
        \addplot[averagePlot] table[x index=3,y index=0]{\tablemin};
        \addplot[bestPlot] table[x index=2,y index=0]{\tablemin};
      \end{axis}
      \begin{axis}[at={($(plotmin.south)-(0,0.25cm)$)},
          basicAxis,minAxis,summaryAxis]
        \addplot[bestPlot] table[x index=1,y index=0]{\tableminSum};
        \addplot[worstPlot] table[x index=3,y index=0]{\tableminSum};
        \addplot[averagePlot] table[x index=2,y index=0]{\tableminSum};
      \end{axis}
  \end{tikzpicture}}
  %%%%%%%% MxAP %%%%%%%%
  \subfloat[\textbf{MxAP}]{\label{fig:res:max}%
    \begin{tikzpicture}[]
      \begin{axis}[name=plotmax,basicAxis,mainAxis,maxAxis]
        \addplot[worstPlot] table[x index=2,y index=0]{\tablemax};
        \addplot[averagePlot] table[x index=3,y index=0]{\tablemax};
        \addplot[bestPlot] table[x index=4,y index=0]{\tablemax};
      \end{axis}
      \begin{axis}[at={($(plotmax.south)-(0,0.25cm)$)},
          basicAxis,maxAxis,summaryAxis]
        \addplot[worstPlot] table[x index=1,y index=0]{\tablemaxSum};
        \addplot[bestPlot] table[x index=3,y index=0]{\tablemaxSum};
        \addplot[averagePlot] table[x index=2,y index=0]{\tablemaxSum};
      \end{axis}
  \end{tikzpicture}}
  \caption[]{Final results. In the summary row (at the bottom of the
    charts), the worst, best, and average values correspond respectively
    to the worst, best, and average coverage ratios over the coverage
    ratios for all instances.
    %
    %% the indicated average is the average of the average figures in the
    %% respective chart above. The indicated best and worst figures are
    %% the best of the best and the worst of the worst figures in the
    %% respective chart above, respectively.
    %
    As there is a single instance with~1,000,000 points, the best, worst,
    and average values are equal.}
  \label{fig:res}
\end{figure}
