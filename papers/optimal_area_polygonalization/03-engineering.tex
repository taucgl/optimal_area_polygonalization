% =============================================================================
\section{Algorithm Engineering}
\label{sec:engineering}
% =============================================================================
As mentioned above, the application consists of a modular pipeline
formed of several steps, where each step can be implemented in various
ways and its execution is governed by several parameters. In the
following we describe the structure of the pipeline in detail.

% -----------------------------------------------------------------------------
\subsection{Program Structure and Options}
\label{ssec:engineering:options}
% -----------------------------------------------------------------------------
The application supports several command line options that can be used
to set the parameters that govern the pipeline execution. In
particular, one option enables the application of the spatial
subdivision, and another option sets the size of the subsets in case
spatial subdivision is indeed enabled. Another option selects the
algorithm used to compute the initial state; see
Section~\ref{ssec:engineering:initial-states}. Other options control
the usage of local and global steps, e.g., the type of steps and the
number of steps. Several additional options control the
simulated annealing process, e.g., the rate of cooling. Using command
line options, the user can select the geometric kernel and the number
type used; see Section~\ref{ssec:engineering:number-type}. Finally,
there exists an option that indicates whether to bypass the
computation of the initial state. Bypassing the computation of the
initial state is allowed under the precondition that the input
consists of a valid simple polygon.  This option enables progressive
refinement of the result using successive executions.

We have experimented with the various parameters and attempted to
predict the best settings for the various input instances. We found
out that the following strategy achieves good results for all
instances. First we apply spatial subdivision solving the
polygonization problem on each subset and merging the individual
intermediate results. We bound the subset size to enable the
relatively quick application of simulated annealing mainly using
global steps. Recall, that the validity check of a global step takes
time linear in the size of the input, which is expensive; thus,
applying a large sequence of global steps on a large input set may be
prohibitively time consuming. Then, we continue applying simulated
annealing, this time on the merged result, mainly using local steps.

% -----------------------------------------------------------------------------
\subsection{Computing the Initial State}
\label{ssec:engineering:initial-states}
% -----------------------------------------------------------------------------
The first phase in the pipeline accepts as input a set $\mathcal{S}$ of
$n$ points and computes an initial state, that is, a polygonization of
$\mathcal{S}$ that yields a simple polygon. For inputs with points
distributed uniformly in a square, the area of the polygon obtained by
the first method was observed to be roughly 50\% of the area of the
convex hull of the input points, compared to 44\% with the second
method (when the pivot is chosen to be the lexicographically middle
point between the leftmost and rightmost points in the input).
However, no significant difference has been observed between the
results obtained after applying the simulated annealing process on the
polygons obtained by either methods. The first method allowed for a
simple implementation of the spatial subdivision strategy, and in
particular of the merging sub-strategy described in
Section~\ref{ssec:algorithms:spatial-subdivision}, and thus was the
one used for most input instances.

% -----------------------------------------------------------------------------
\subsection{Evaluating the Quality of a State}
\label{ssec:engineering:quality}
% -----------------------------------------------------------------------------
The energy of a state $P$ for MnAP is set to be proportional to
$\coverage(P)$. Empirically, we have noticed that the difference
between the coverage ratios of consecutive states $P_i$ and $P_{i+1}$
(where $P_{i+1}$ is obtained as a result of either a local or a global
transition step) decreases with the increase in the input-set
size. Thus, the exact energy is set to be the coverage ratio
multiplied by the input-set size.  Applying this amplification was
justified by our experiments, as it increased the convergence
rate. More precisely, for MnAP the energy of a state $P$, which is a
polygonization of a point set $\mathcal{S}$ of size $n$, is given by
the expression $n \times \coverage(P)$. For MxAP the energy of a state
$P$ is given by $n \times (1 - \coverage(P))$. Recall, that the
algorithm only requires computing the energy difference, which is
$n\times \Delta\coverage(P)$ and $n\times -\Delta\coverage(P)$ for
MnAP and MxAP, respectively.

Calculating the difference in the areas of two consecutive states is
done in constant time, simply by adding and subtracting the area of
the relevant triangles. For example, if the new state is obtained as
the result of the local transition step illustrated in
Figure~\ref{fig:local}, then the area of the triangle $(s,r,q)$ is
added and the area of the triangle $(p,q,r)$ is subtracted (assuming
that the polygon interior is to the left of the directed polygonal
chain). If the new state is obtained as the result of the global
transition step illustrated in Figure~\ref{fig:global}, then the area
of the triangle $(p,q,r)$ is added and the area of the triangle
$(s,q,t)$ is subtracted.

% -----------------------------------------------------------------------------
\subsection{Tuning the Simulated Annealing Parameters}
\label{ssec:engineering:tuning}
% -----------------------------------------------------------------------------
The starting temperature is chosen to be~1. The simulated annealing
process is terminated after a specified number of iterations $\ell$
(transition steps).  (This was the only termination
criterion we used.)  As the number of simulated annealing iterations
is predetermined and set by the user, the temperature is decreased by
$1/\ell$ at the end of each iteration. We apply a transition if the
difference between the energy of the source and target states, $\Delta
D$, is negative, or $\Delta D$ is non-negative and
Condition~\ref{eqn:sa} (Metropolis) holds.

% -----------------------------------------------------------------------------
\subsection{Tuning the Subset Sizes for the Spatial Subdivision}
\label{ssec:engineering:spatial-subdivision}
% -----------------------------------------------------------------------------
Using global steps achieves much better results compared to using
local steps when applying the same number of steps. However, due to
the low probability of producing a (valid) step for large point sets,
and the high (linear) time consumption of the validity check for large
point sets (which must be performed for each potential transition), it
proved to be an effective strategy for input sets containing not more
than few hundreds of points.  We tried to strike a balance between the
size of the subsets for each instance, and the number of (global)
steps applied to each subset.  With smaller subset sizes, the best
area this method is able to obtain is further from the true optimum,
since the search space is more limited and the simple merging process
we used does not aim to optimize the area of the merged polygon.
However, the larger the subset is, the more time it takes to validate
a potential global transition.

We quickly executed our program on small size input sets (smaller
than~1,000) without applying spatial subdivision to solve the MxAP
problem, and extracted preliminary values of the computed coverage
ratio $\coverage(\minBestPoly)$, averaged over the size of the fed
input point set (see Figure~\ref{fig:energy} for the final values). We
noticed that even for these small size input sets, we hardly obtained
more than~90\% coverage. Then, we tuned the subset size as follows: We
observed that the time it takes to achieve a certain coverage using
global steps is roughly cubic in the number of points. Recall, that
the time required to validate a step grows linearly with the input
size. Both the total number of (valid) steps required and the average
number of potential transitions needed to find a new valid state were
observed to grow linearly with the input size as well.  We estimated
the number of steps required and the time it would take to
achieve~90\% coverage for a subset of a certain size.  For each
instance, we aimed to split it into as large as possible subsets so
that for each,~90\% coverage would be obtained in the allotted
time. We did not apply spatial subdivision for instances of sizes
smaller than~1,000. We used the same subset sizes that we used to
solve the MxAP, to solve the MnAP problem.
%% (We could
%% have repeated the quick execution above to extract potentially better
%% subset sizes for the MnAP problem.)

% -----------------------------------------------------------------------------
\subsection{Data Structures}
\label{ssec:engineering:data-structures}
% -----------------------------------------------------------------------------
When spatial subdivision is applied, we subdivide the input set into
subsets. Then, we compute an initial polygon for each subset and store
the points of the polygonal chains in corresponding lists. In
addition, for each subset of size $m$ we maintain an array that stores
the $m$ iterators to the points in the list. When we perform a
transition as part of the application of simulated annealing on a
subset, we quickly update the list that stores the polygonal chain
(that is, the current state), and the relevant entries in the
array. We use the array to efficiently access random points as part of
the transition steps.
%% More precisely, when we choose a random point,
%% we, in fact, choose a number $0 \leq i < m$.  We use the number as an
%% index to the array to extract the iterator to the point in the list,
%% which in turn is used to access the point in the list.
When we change
the position of a point in the list, we use the index stored in the
record that extends the point type to access the corresponding entry
in the array and update it; see
Section~\ref{ssec:engineering:number-type}.

% -----------------------------------------------------------------------------
\subsection{Geometric Kernel and Number Type}
\label{ssec:engineering:number-type}
% -----------------------------------------------------------------------------
Our application includes conditional steps that are based on the
evaluations of predicates, carried out through the computation of the
sign of an arithmetic expression. For example, the computation of the
convex hull of the input points, requires the computation of the
orientation of three points, which requires the calculation of the
sign of the determinant of a matrix that consists of the point
coordinates. Such predicates are sensitive to limited precision
arithmetic, and constitute a genuine threat to program correctness and
normal termination when limited precision arithmetic is actually used.

The coordinates of the points in the input sets are even integral
values. While the area is ensured to be an integral
value~\cite{o-cgc-98}, overflow may still occur while evaluating an
arithmetic expression during the execution of the application. To
overcome this potential pitfall, we based our implementation on \cgal,
which follows the Exact Geometric Computation (EGC) paradigm, and thus
guarantees robust execution (and exact results).

The developed software adheres to generic programming paradigm; it
enables the convenient selection of a geometric kernel and a specific
number type; see, e.g.,~\cite[Chapter~1]{fhw-caass-12}, for
details. In particular, we used an extended filtered Cartesian kernel,
provided by \cgal~\cite{cgal:bfghhkps-lgk23-20}, instantiated with
either the \texttt{long} standard number type or the \texttt{Gmpz}
number type from the The GNU Multiple Precision Arithmetic (GMP)
library~\cite{g-gmp-20}.  The former target type typically has width
of~64 bits.  The latter target type is of unlimited precision (up to
memory limitation). For all the given instances the limited precision
type turned out to be sufficient. However, future instances may cause
overflow, and thus, may require the selection of the unlimited
precision based kernel.

The point type of the kernel is extended with a record
%"Point_data"
that contains several fields as follows:
%
(a) The index to the point in the input sequence of points.  The
output format (defined by the contest rules) was a permutation of the
input points given as a sequence of indices. The index above was used
to construct this permutation from the resulting sequence of points
(defining the final polygonal chain) in linear time.  Upon
initialization, we store the input points in a list, if spatial
subdivision is suppressed, or in several lists otherwise, and set
their indices.
%
(b) A Boolean flag that indicates whether the point lies on the lower
hull. This flag is used when constructing an $x$-monotone polygon, which
serves as an initial state.
%
(c) An enumeration that indicates whether the point is leftmost or
rightmost in the subset, and thus shared with the left or right
neighboring subset, respectively, or whether the point is the other
endpoint of the leftmost or rightmost segment of the lower
hull. Recall, that this segment must remain in the polygonal chain;
see Section~\ref{ssec:algorithms:spatial-subdivision}.
%
(d) An integral value used in the computation of a constrained
triangulation; see Section~\ref{ssec:outlook:global}.
