\def\globalCoords{%
  \coordinate(cp) at (4,0);
  \coordinate(cq) at (2,1);
  \coordinate(cr) at (0,0);
  \coordinate(cs) at (0,2);
  \coordinate(ct) at (4,2);
  %
  \coordinate(ca) at (5,0.5);
  \coordinate(cb) at (-1,0.5);
  \coordinate(cc) at (-1,1.5);
  \coordinate(cd) at (5,1.5);
  %
  \node[point,label=above:{$p$}] (p) at (cp) {};
  \node[point,label=above:{$q$}] (q) at (cq) {};
  \node[point,label=above:{$r$}] (r) at (cr) {};
  \node[point,label=below:{$s$}] (s) at (cs) {};
  \node[point,label=below:{$t$}] (t) at (ct) {};
  %
  \draw[] (ca) to[out=180,in=0] (p);
  \draw[] (r) to[out=180,in=0] (cb);
  \draw[] (cc) to[out=0,in=180] (s);
  \draw[] (t) to[out=0,in=180] (cd);%
}
\def\slab{%
  \coordinate (a1) at (0,-2);
  \coordinate (a2) at (-0.75,-1);
  \coordinate (cr) at (-1,-1);
  \coordinate (cs) at (-1.25,0);
  \coordinate (a3) at (-1,0);
  \coordinate (a4) at (0,2);
  \coordinate (a5) at (1,0);
  \coordinate (cq) at (1.25,0);
  \coordinate (cp) at (1,-1);
  \coordinate (a6) at (0.75,-1);
  %
  \draw[dashed] (-1.25,2)--(-1.25,-2);
  \node[point] (q) at (cq) {};
  \node[point] (p) at (cp) {};
  \node[point] (r) at (cr) {};
  \node[point] (s) at (cs) {};
  \draw[mySanke] (a1) to[out=180,in=-45](a2);
  \draw[thick] (a2)--(r);
  \draw[thick] (r)--(s);
  \draw[thick] (s)--(a3);
  \draw[mySanke] (a3) to[out=90,in=180](a4);
  \draw[mySanke] (a4) to[out=0,in=90](a5);
  \draw[thick] (a5)--(q);
  \draw[thick] (q)--(p);
  \draw[thick] (p)--(a6);
  \draw[mySanke] (a6) to[out=225,in=0](a1);%
}
% =============================================================================
\section{Algorithms}
\label{sec:algorithms}
% =============================================================================
When applying Simulated Annealing to solve an optimization problem an
initial state must be provided to start with. In the physical world
metaphor this would be the heated metal. In our case, this is a valid
polygonization of our input points. In addition the process uses an
operation that determines the cost of a given state. In the case of
annealing a metal this would be the excess energy of the metal that
can be released. In our case, this is a measure that indicates how
``bad'' our polygonization is with respect to the objective function
(being either the minimum area or the maximum area). Finally, a
transition operation must be available. When the process is
implemented in software, three corresponding functions listed below
must be implemented.
\begin{enumerate}
\item Computing an initial state.
\item Performing a transition.
\item Evaluating the quality of a given state.
\end{enumerate}

% -----------------------------------------------------------------------------
\subsection{Notations}
\label{ssec:algorithms:terms}
% -----------------------------------------------------------------------------
Let $\area(P)$ denote the area of a polygon $P$. Let
$\convexHull(\mathcal{S})$ denote the convex hull of a point set
$\mathcal{S}$. Let
$\coverage(P)=\frac{\area(P)}{\area(\convexHull(P))}$, referred to as
the coverage ratio, denote the ratio between the area of the polygon
$P$ and the area of the convex hull of the polygon. Given a point set
$\mathcal{S}$, let $\maxOptPoly$ and $\minOptPoly$ be the maximum-area
and minimum-area polygons defined by $\mathcal{S}$, respectively.
Similarly, let $\maxBestPoly$ and $\minBestPoly$ be the maximum-area
and minimum-area polygons defined by $\mathcal{S}$ obtained by our
application, respectively.  Naturally, $0
< \coverage(\minOptPoly) \leq \coverage(\minBestPoly)$ and
$\coverage(\maxBestPoly) \leq \coverage(\maxOptPoly) \leq 1$.

% -----------------------------------------------------------------------------
\subsection{Computing the Initial State}
\label{ssec:algorithms:initial-states}
% -----------------------------------------------------------------------------
We have implemented two methods for computing an initial state---one
that computes an $x$-monotone polygon and another that computes a
star-shaped polygon; see Figure~\ref{fig:example} for an
illustration. Computing an $x$-monotone polygon is done by obtaining
the lower hull of the input points, then sorting the remaining points
in decreasing lexicographical order, and finally, connecting the two
sequences. Special care must be taken in the degenerate cases where
the first or last segment in one or both sequences is
vertical. Computing a star-shaped polygon is done by computing the
convex hull of the points, then choosing a point inside the convex
hull (e.g., the centroid of the convex hull), then sorting the input
points by their polar angle at the chosen point and connecting them in
that order.

\begin{figure}[!ht]
  \subfloat[]{\label{fig:example:monotone}%
    \begin{tikzpicture}[scale=0.675]
      \begin{axis}[yticklabel={~}]
        \addplot[fill=\bkgcolor] table {ch_polyline.txt} --cycle;
        \addplot[fill=\pgncolor] table {hc_polyline.txt} --cycle;
      \end{axis}
    \end{tikzpicture}}
  \subfloat[]{\label{fig:example:star}%
    \begin{tikzpicture}[scale=0.675]
      \begin{axis}[]
        \addplot[fill=\bkgcolor] table {ch_polyline.txt} --cycle;
        \addplot[fill=\pgncolor] table {star_polyline.txt} --cycle;
      \end{axis}
    \end{tikzpicture}}
  \subfloat[]{\label{fig:example:best}%
    \begin{tikzpicture}[scale=0.675]
      \begin{axis}[yticklabel={~}]
        \addplot[fill=\bkgcolor] table {ch_polyline.txt} --cycle;
        \addplot[fill=\pgncolor] table {best_polyline.txt} --cycle;
      \end{axis}
    \end{tikzpicture}}
  \caption[]{Different polygonizations of the point set provided in the file
    \texttt{uniform-0000050-1.instance}. The obtained polygons are rendered
    in orange and the respective convex hulls excluding the polygons are
    rendered in gray.
    (\subref*{fig:example:monotone}) An $x$-monotone polygon (54.93\%).
    (\subref*{fig:example:star}) A star-shaped polygon (61.83\%).
    (\subref*{fig:example:best}) The largest-area polygon obtained by our
      application (91.34\%).
    The numbers in parentheses indicate the coverage ratio.}
  \label{fig:example}
\end{figure}

% -----------------------------------------------------------------------------
\subsection{Simulated Annealing Transition Steps}
\label{ssec:algorithms:sa-transition-steps}
% -----------------------------------------------------------------------------
A transition function accepts a valid state as input and results in
a new valid state, hopefully but not necessarily, of improved (lower)
cost compared to the input state. We devised two kinds of transition
steps for the simulated annealing process, referred to as
the \emph{local} and \emph{global} steps.

In a local step we swap the positions of a randomly chosen point $q$
and its successor $r$, in the polygonal chain; see
Figure~\ref{fig:local}. Let $p$ be the point that precedes $q$, and
let $s$ be the point that succeeds $r$, in the polygonal chain. The
resulting polygon is valid iff (a) the newly introduced segments
$\overline{pr}$ and $\overline{qs}$ do not intersect each other, and
(b) these new segments do not intersect any other segment in the
polygonal chain.

\begin{figure}[!ht]
  \def\localCoords{%
    \begin{scope}[local bounding box=a]
      \coordinate(cp) at (0,1);
      \coordinate(cq) at (2,0);
      \coordinate(cr) at (2,2);
      \coordinate(cs) at (4,1);
      %
      \coordinate(cx) at (0.5,1.75);
      \coordinate(cu) at (-0.75,1);
      \coordinate(cv) at (1,2.5);
      %
      \coordinate(cm) at (-1,0.25);
      \coordinate(cn) at (5,1.75);
    \end{scope}
    \coordinate (a) at (0,0);
    \coordinate (b) at (0,2);
    \coordinate (c) at (4,2);
    \coordinate (d) at (4,0);
    %% \node[label={[label distance=-5pt]225:{$a$}}] at (a) {};
    %% \node[label={[label distance=-5pt]135:{$b$}}] at (b) {};
    %% \node[label={[label distance=-5pt] 45:{$c$}}] at (c) {};
    %% \node[label={[label distance=-5pt]315:{$d$}}] at (d) {};
    \draw[dashed,name path global=bbox] (a) rectangle (c);
    %
    \node[point,label=60:{$p$}] (p) at (cp) {};
    \node[point,label=below:{$q$}] (q) at (cq) {};
    \node[point,label=above:{$r$}] (r) at (cr) {};
    \node[point,label=-60:{$s$}] (s) at (cs) {};
    %
    \node[point=blue,label=right:{$x$}] (x) at (cx) {};
    \node[point=white] (u) at (cu) {};
    \node[point=white] (v) at (cv) {};
    %
    \draw[] (u)--(x);
    \draw[] (v)--(x);
    \draw[] (cm) to[out=0,in=180] (p);
    \draw[] (cn) to[out=180,in=0] (s);
  }
  \vspace{0pt}\centering%
  \begin{tabular}{lcr}
    \begin{tikzpicture}
      \localCoords
      \draw[thick] (p)--(q)--(r)--(s);
      % \draw[name path global=bbox] (a.south west) rectangle (a.north east);
    \end{tikzpicture} &
    \begin{tikzpicture}\Arrow{16mm}{1.4}\end{tikzpicture} &
    \begin{tikzpicture}
      \localCoords
      \draw[thick] (p)--(r)--(q)--(s);
    \end{tikzpicture}
  \end{tabular}
  \caption[]{A local transition step.}
  \label{fig:local}
\end{figure}

\begin{observation}\label{observation:local-step-validation}
  If a segment $\overline{xy}$ intersects one of the two newly
  introduced segments ($\overline{pr}$ or $\overline{qs}$), then
  either the point $x$ or the point $y$ are located inside the open
  axis-parallel bounding rectangle of the points $p$, $q$, $r$, and
  $s$.
\end{observation}

\begin{proof}
  Assume, that there exists a segment $\overline{xy}$ that intersects
  either $\overline{pr}$ or $\overline{qs}$. Since the input state is
  a valid polygonization, $\overline{xy}$ cannot intersect any
  segments in the input state, and in particular, none of
  $\overline{pq}$, $\overline{qr}$, or $\overline{rs}$; thus, it does
  not cross $\convexHull(p,q,r,s)$. Therefore, to intersect either
  $\overline{pr}$ or $\overline{qs}$, either $x$ or $y$ must be
  properly inside $\convexHull(p,q,r,s)$.  As $\convexHull(p,q,r,s)$
  is contained inside the bounding box $B$, point $x$ or point $y$ must
  be properly inside $B$.
  %% There may be segments that cross
  %% the bounding box $B$ with corners $a$, $b$, $c$, and $d$. However,
  %% they can only cross one of the triangles $(p,a,q),(r,b,p),(s,c,r)$
  %% or $(q,d,s)$ and for each of these triangles they can only intersect
  %% the boundary of the respective triangle in portions of edges of $B$,
  %% which means that they cannot intersect $\overline{pr}$ or
  %% $\overline{qs}$.  Therefore, to intersect either $\overline{pr}$ or
  %% $\overline{qs}$, either $x$ or $y$ must be properly inside $B$.
\end{proof}

We efficiently check the validity of the resulting polygonization
using a \kdtree{} search structure~\cite[Chapter~5]{aps-sugor-08}. We
construct the \kdtree{} once as part of the initialization of the entire
process and insert all input points into the tree. When a local
transition step is carried out, we first randomly choose a point $q$
in the current polygonal chain, and obtain a new polygon as described
above. Then, we check the validity of the new polygon.  In particular,
we check whether the two newly introduced segments, namely
$\overline{pr}$ and $\overline{qs}$, intersect each other, and we
check whether either $\overline{pr}$ or $\overline{qs}$ intersect any
other segment in the polygonal chain as follows: we submit a
range-search query, where the query range is the rectangular
axis-parallel bounding box of the points $p$, $q$, $r$, and $s$. For
every point located in the range, we test whether one of the two
segments incident to the point intersects one of $\overline{pr}$ or
$\overline{qs}$. If an intersection is detected, the new polygon is
discarded and the process is repeated. In practice we used the \kdtree{}
provided by \cgal~\cite{cgal:tf-ssd-20}. While the worst case
asymptotic running time of \kdtree{} queries is
high~\cite[Chapter~5]{aps-sugor-08}, in practice, as we observe here
as well, queries are answered very quickly. The local transition step
described above can be generalized; see
Section~\ref{ssec:outlook:lsg}. However, we have not implemented the
generalization.

In a global step we randomly pick a point $q$, then randomly pick a
point $s$. Let $p$ and $r$ be the points that precedes and succeeds
$q$, respectively, and let $t$ be the point that succeeds $s$, in the
polygonal chain. We first connect $p$ and $r$ (removing $q$ from the
polygonal chain) and then insert $p$ between $s$ and $t$; see
Figure~\ref{fig:global}. The resulting polygon is valid iff (a) the
newly introduced segment $\overline{pr}$ does not intersect one of the
other newly introduced segments $\overline{sq}$ and $\overline{qt}$,
and (b) each of these new segments does not intersect any other
segment in the polygonal chain.

\begin{figure}[!ht]
  \vspace{0pt}\centering%
  \begin{tabular}{lcr}
    \begin{tikzpicture}
      \globalCoords
      \draw[thick] (p)--(q)--(r);
      \draw[thick] (s)--(t);
    \end{tikzpicture} &
    \begin{tikzpicture}\Arrow{16mm}{1.0}\end{tikzpicture} &
    \begin{tikzpicture}
      \globalCoords
      \draw[thick] (p)--(r);
      \draw[thick] (s)--(q)--(t);
    \end{tikzpicture}
  \end{tabular}
  \caption[]{A successful global transition.}
  \label{fig:global}
\end{figure}

In a global step there is no guarantee that an endpoint of a segment
that intersects any one of the newly introduced segments is located
inside the axis-parallel bounding box of the involved points, as
illustrated in Figure~\ref{fig:global-invalid}. Thus, to check whether
the resulting polygon is valid, we need to check whether any one of
the newly introduced segments intersects any other remaining segment
in the polygonal chain. Observe that a local step is a special case of
a global step. If $t$ and $r$ denote the same point, then the linear
time validity check can be replaced with the much more efficient validity
check of a local step.

\begin{figure}[!ht]
  \vspace{0pt}\centering%
  \begin{tabular}{lcr}
    \begin{tikzpicture}
      \globalCoords
      \draw[thick] (p)--(q)--(r);
      \draw[thick] (s)--(t);
      \draw[thick](-1,1.25)--(5,1.25);
    \end{tikzpicture} &
    \begin{tikzpicture}
      \Arrow{16mm}{1.0}
      \node[cross out,draw=red,very thick,minimum size=25,inner sep=0pt,outer sep=0pt,line width=2mm]
      at (0.8,1.0) {};
    \end{tikzpicture} &
    \begin{tikzpicture}
      \globalCoords
      \draw[thick] (p)--(r);
      \draw[thick] (s)--(q)--(t);
      \draw[thick](-1,1.25)--(5,1.25);
    \end{tikzpicture}
  \end{tabular}
  \caption[]{An unsuccessful global transition.}
  \label{fig:global-invalid}
\end{figure}

% -----------------------------------------------------------------------------
\subsection{Spatial Subdivision}
\label{ssec:algorithms:spatial-subdivision}
% -----------------------------------------------------------------------------
We have applied a spatial subdivision strategy described below to
solve the problem at hand, subdividing the original problem into
several subproblems once, and combining the solutions of the
subproblems to yield the final solution. The input set of $n$ points
is subdivided into $k = \lceil (n-1)/(m-1) \rceil$ subsets of
approximately $m$ points each for a given integer number $m$, based on
the lexicographic ordering of the points; see
Figure~\ref{fig:spatial-subdivision-1D}. Every two subsets of points
located in two adjacent vertical slabs, respectively, share a common
point. We use a greedy approach to make sure that the rightmost
segment of the lower hull of every subset, except for the last subset,
is strictly monotone increasing, and similarly, that the leftmost
segment of the lower hull of every subset, except for the first
subset, is strictly monotone decreasing. More precisely, let
$\mathcal{S}_i$ and $\mathcal{S}_{i+1}$ be two subsets of points
located in two adjacent vertical slabs, $0 \leq i < k-1$, and let
$q_i$ be their common point. We make sure that there exist points
$p_i \in \mathcal{S}_i$ and $r_i \in \mathcal{S}_{i+1}$, such that
$y(p_i) < y(q_i)$ and $y(q_i) > y(r_i)$, where $y(p)$ denotes the
$y$-coordinate of $p$. When we construct the subset $\mathcal{S}_i$,
if $i = 0$, we initialize it with the first $m$ points in the
lexicographic ordering; otherwise, we initialize it with the rightmost
point of $\mathcal{S}_{i-1}$, and append the first remaining $m-1$
points in the lexicographic ordering. We incrementally extend
$\mathcal{S}_i, 0 \leq i$ as long as the conditions above are not
met. If the number of remaining points at the end is below some
threshold, we append the remaining points to the last subset as well.
%
%% Observe, that the expected number of such extension iterations per
%% subset is a small constant, as the probability that the $y$-coordinate
%% of a point $p$ is smaller then the $y$-coordinate of a point $q$,
%% where $p$ and $q$ are consecuitive in the lexicographic ordering,
%% is~$1/2$.
%
Once the construction of the subsets is complete, we mark the
rightmost segment of the lower hull of every subset except for the
last subset, and the leftmost segment of the lower hull of every
subset except for the first subset.  Then, we solve the polygonization
problem for each subset using simulated annealing, under the
constraint that the marked segments appear in the partial results. We
force the marked segments to be in the initial state and we never
apply a transition that compromises this constraint. Finally, we
easily merge the solutions of the subproblems.  Let $\overline{pq}$
and $\overline{qr}$ be the marked segments in $S_i$ and $S_{i+1}$,
respectively. We remove the segments $\overline{pq}$ and
$\overline{qr}$ and instead introduce the segment $\overline{pr}$. We
have not implemented other subdivision and merging
strategies. Naturally, applying more sophisticated strategies may
improve the final results; see
Section~\ref{ssec:outlook:dq-subdivision} and
Section~\ref{ssec:outlook:dq-merging}.

\begin{comment}
For inputs with points distributed uniformly in a square, a local
maximum is found very quickly on average. Can add probabilistic
analysis.
\end{comment}

%% The simple merging approach, which forces specific segments to be part of
%% the resulting polygonization, is straightforward and free of degenerate
%% cases, and thus, easily implemented. Naturally, lifting the constraint

%% Naturally, any constraint that forces specific segments to be part of
%% the resulting polygonization may turn out to

%% the naive merging approach, which forces
%% , while easily implemented, may come at the expense of a polygonization of
%% better quality.


%% This strategy allows running simulated annealing with general steps on
%% the split sets to quickly achieve good results for each of them, and
%% after the merge the resulting area is still quite good, despite the
%% potential loss/gain in area relative to the area of the convex hull of
%% the union of points in the sets merged.

\begin{figure}[!ht]
  \begin{tikzpicture}[scale=0.9]
    \slab
    \begin{scope}[xshift=2.5cm]
      \draw[dashed] (1.25,2)--(1.25,-2);
      \slab
    \end{scope}
    \begin{scope}[xshift=5cm]
      \draw[fill=black] (-0.5,0) circle (0.1);
      \draw[fill=black] (0,0) circle (0.1);
      \draw[fill=black] (0.5,0) circle (0.1);
    \end{scope}
    \begin{scope}[xshift=7.5cm]
      \draw[dashed] (1.25,2)--(1.25,-2);
      \slab
    \end{scope}
    \node at (0.97,-0.25) {$q$};
    \node at (1.0,-1.35) {$p$};
    \node at (1.65,-0.75) {$r$};
  \end{tikzpicture}
  %
  \caption[]{Spatial Subdivision.}
  \label{fig:spatial-subdivision-1D}
\end{figure}

%% \begin{figure}[!ht]
%% \end{figure}
