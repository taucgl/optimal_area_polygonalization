// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <iostream>
#include <string>

#include "OAP/basic.hpp"
#include "OAP/info_messengers.hpp"
#include "OAP/message_box.hpp"

OAP_BEGIN_NAMESPACE

//! default info message handler.
Info_function s_info_messenger(standard_info_messenger);

//! \brief sets the info message handler function.
Info_function set_info_messenger(Info_function handler)
{
  auto result = get_info_messenger();
  s_info_messenger = handler;
  return result;
}

//! \brief obtains the info message handler function.
Info_function& get_info_messenger() { return s_info_messenger; }

//! \brief standard info handler.
void standard_info_messenger(const std::string& msg)
{ std::cout << msg << std::endl; }

//! \brief handles info using a message box.
void message_box_info_messenger(const std::string& msg)
{ message_box(msg); }

OAP_END_NAMESPACE
