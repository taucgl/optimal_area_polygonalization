// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <iostream>
#include <string>

#include "OAP/basic.hpp"
#include "OAP/failure_behaviours.hpp"
#include "OAP/failure_messengers.hpp"
#include "OAP/message_box.hpp"

OAP_BEGIN_NAMESPACE

// default messenger.
Failure_function s_error_messenger(standard_error_messenger);
Failure_function s_warning_messenger(standard_warning_messenger);

//! \brief sets the error messenger function.
Failure_function set_error_messenger(Failure_function messenger)
{
  Failure_function result = get_error_messenger();
  s_error_messenger = messenger;
  return result;
}

//! \brief obtains the error messenger function.
Failure_function& get_error_messenger() { return s_error_messenger; }

//! \brief standard error messenger
void standard_error_messenger(const char* what, const char* expr,
                              const char* file, int line, const char* msg)
{
#if defined(__GNUG__) && !defined(__llvm__)
  // After g++ 3.4, std::terminate defaults to printing to std::cerr itself.
  if (get_error_behaviour() == Failure_behaviour::THROW_EXCEPTION) return;
#endif
  std::cerr << "OAP error  : " << what << " violation!" << std::endl
            << "Expression : " << expr << std::endl
            << "File       : " << file << std::endl
            << "Line       : " << line << std::endl
            << "Explanation: " << msg << std::endl
            << std::endl;
}

//! \brief handles errors using a message box.
void message_box_error_messenger(const char* what, const char* expr,
                                 const char* file, int line,
                                 const char* explanation)
{
#if defined(__GNUG__) && !defined(__llvm__)
  // After g++ 3.4, std::terminate defaults to printing to std::cerr itself.
  if (get_error_behaviour() == Failure_behaviour::THROW_EXCEPTION) return;
#endif
  std::string msg("OAP error: ");
  msg.append(what).append(" violation!\n").
    append("Expression : ").append(expr).append("\n").
    append("File       : ").append(file).append("\n").
    append("Line       : ").append(std::to_string(line)).append("\n").
    append("Explanation: ").append(explanation).append("\n");
  message_box(msg);
}

//! \brief sets the warning messenger function.
Failure_function set_warning_messenger(Failure_function messenger)
{
  Failure_function result = get_warning_messenger();
  s_warning_messenger = messenger;
  return result;
}

//! \brief obtains the warning messenger function.
Failure_function& get_warning_messenger() { return s_warning_messenger; }

//! \brief standard warning messenger.
void standard_warning_messenger(const char*, const char* expr,
                                const char* file, int line,
                                const char* explanation)
{
#if defined(__GNUG__) && !defined(__llvm__)
  // After g++ 3.4, std::terminate defaults to printing to std::cerr itself.
  if (get_warning_behaviour() == Failure_behaviour::THROW_EXCEPTION) return;
#endif
  std::cerr << "OAP warning: check violation!" << std::endl
            << "Expression : " << expr << std::endl
            << "File       : " << file << std::endl
            << "Line       : " << line << std::endl
            << "Explanation: " << explanation << std::endl
            << std::endl;
}

//! \brief handles warning using a message box.
void message_box_warning_messenger(const char*, const char* expr,
                                   const char* file, int line,
                                   const char* explanation)
{
#if defined(__GNUG__) && !defined(__llvm__)
  // After g++ 3.4, std::terminate defaults to printing to std::cerr itself.
  if (get_warning_behaviour() == Failure_behaviour::THROW_EXCEPTION) return;
#endif
  std::string msg("OAP warning: check violation!\n");
  msg.append("Expression : ").append(expr).append("\n").
    append("File       : ").append(file).append("\n").
    append("Line       : ").append(std::to_string(line)).append("\n").
    append("Explanation: ").append(explanation).append("\n");
  message_box(msg);
}

OAP_END_NAMESPACE
