// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include "OAP/basic.hpp"
#include "OAP/Failure_behaviour.hpp"
#include "OAP/failure_behaviours.hpp"

OAP_BEGIN_NAMESPACE

// default behaviour settings
Failure_behaviour s_error_behaviour(Failure_behaviour::THROW_EXCEPTION);
Failure_behaviour s_warning_behaviour(Failure_behaviour::CONTINUE);

//! \brief sets the error behaviour mode.
Failure_behaviour set_error_behaviour(Failure_behaviour mode)
{
  Failure_behaviour result = get_error_behaviour();
  s_error_behaviour = mode;
  return result;
}

//! \brief obtains the error behaviour mode.
Failure_behaviour& get_error_behaviour() { return s_error_behaviour; }

//! \brief sets the warning behaviour mode.
Failure_behaviour set_warning_behaviour(Failure_behaviour mode)
{
  Failure_behaviour result = get_warning_behaviour();
  s_warning_behaviour = mode;
  return result;
}

//! \brief obtains the warning behaviour mode.
Failure_behaviour& get_warning_behaviour() { return s_warning_behaviour; }

OAP_END_NAMESPACE
