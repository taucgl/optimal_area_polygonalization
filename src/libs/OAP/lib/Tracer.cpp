// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with OAP.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <string>
#include <iostream>

#include "OAP/basic.hpp"
#include "OAP/Tracer.hpp"

OAP_BEGIN_NAMESPACE

//! The trace singleton.
Tracer* Tracer::s_instance = nullptr;

//! The names of the codes.
const std::vector<std::string> Tracer::s_code_names = {
  "option1",
  "option2"
};

//! \brief obtains a trace singleton.
Tracer* Tracer::get_instance()
{
  if (!s_instance) s_instance = new Tracer();
  return s_instance;
}

const std::string& Tracer::code_name(size_t i) { return Tracer::s_code_names[i]; }
Tracer::Name_const_iterator Tracer::names_begin() { return Tracer::s_code_names.begin(); }
Tracer::Name_const_iterator Tracer::names_end() { return Tracer::s_code_names.end(); }

OAP_END_NAMESPACE
