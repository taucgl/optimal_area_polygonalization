// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <iostream>

#include "OAP/config.hpp"
#include "OAP/failure_behaviours.hpp"
#include "OAP/failure_messengers.hpp"
#include "OAP/assertions.hpp"
#include "OAP/exceptions.hpp"

OAP_BEGIN_NAMESPACE

// failure functions
// -----------------
void assertion_fail(const char* expr, const char* file, int line,
                    const char* msg)
{
  get_error_messenger()("assertion", expr, file, line, msg);
  switch (get_error_behaviour()) {
   case ABORT: std::abort();
   case EXIT: std::exit(1);
   case EXIT_WITH_SUCCESS: std::exit(0);
   case CONTINUE:       // not a valid option
   case THROW_EXCEPTION:
   default: throw Assertion_exception("METICX", expr, file, line, msg);
  }
}

void precondition_fail(const char* expr, const char* file, int line,
                       const char* msg)
{
  get_error_messenger()("precondition", expr, file, line, msg);
  switch (get_error_behaviour()) {
   case ABORT: std::abort();
   case EXIT: std::exit(1);
   case EXIT_WITH_SUCCESS: std::exit(0);
   case CONTINUE:       // not a valid option
   case THROW_EXCEPTION:
   default: throw Precondition_exception("METICX", expr, file, line, msg);
  }
}

void postcondition_fail(const char* expr, const char* file, int line,
                        const char* msg)
{
  get_error_messenger()("postcondition", expr, file, line, msg);
  switch (get_error_behaviour()) {
   case ABORT: std::abort();
   case EXIT: std::exit(1);
   case EXIT_WITH_SUCCESS: std::exit(0);
   case CONTINUE:        // not a valid option
   case THROW_EXCEPTION:
   default: throw Postcondition_exception("METICX", expr, file, line, msg);
  }
}

// warning function
// ----------------
void warning_fail(const char* expr, const char* file, int line, const char* msg)
{
  get_warning_messenger()("warning", expr, file, line, msg);
  switch (get_warning_behaviour()) {
   case ABORT: std::abort();
   case EXIT: std::exit(1);
   case EXIT_WITH_SUCCESS: std::exit(0);
   case CONTINUE: break;
   case THROW_EXCEPTION:
    throw Postcondition_exception("METICX", expr, file, line, msg);
   default: break;
  }
}

OAP_END_NAMESPACE
