// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include "OAP/basic.hpp"
#include "OAP/message_box.hpp"

OAP_BEGIN_NAMESPACE

//! \brief displays a message in a box.
// Types:
// 0 - On top, Message waits for user apporved (OK)
// 1 - On top, Message automatically disappears timeout=1 sec
// 2 - test
void message_box(const std::string& msg, int type)
{
  std::string full_msg;

  switch (type) {
  case 0:
   full_msg = "(wmctrl -F -a \"I am on top\" -b add, above) & (zenity --info --text=\'" + msg + "\')";
   break;

   case 1:
    full_msg = "(wmctrl -F -a \"I am on top\" -b add, above) & (zenity --info --timeout=2 --text=\'" + msg + "\')";
    break;

   case 2:
    full_msg = "notify-send -t 5 \" " + msg + "\"";
    break;
  }


  auto rc = system(full_msg.c_str());
}

OAP_END_NAMESPACE
