// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <nirgoren@mail.tau.ac.il>
//                              <efifogel@gmail.com>

#ifndef OAP_HALF_CONVEX_HPP
#define OAP_HALF_CONVEX_HPP

/*! \file
 *
 */

#include <vector>

#include <CGAL/basic.h>
#include <CGAL/convex_hull_2.h>
#include <CGAL/Polygon_2.h>

#include "OAP/basic.hpp"
#include "OAP/signed_distance_from_line_comparator.hpp"

OAP_BEGIN_NAMESPACE

template <typename InputIterator, typename OutputIterator, typename Kernel_>
OutputIterator max_area_half_convex_fp(InputIterator begin, InputIterator end,
  OutputIterator res, Kernel_& kernel)
{
  typedef Kernel_                             Kernel;
  typedef typename Kernel::Point_2            Point_2;
  typedef typename Kernel::Segment_2          Segment_2;
  typedef typename CGAL::Polygon_2<Kernel>    Polygon_2;

  std::vector<Point_2> ch;
  CGAL::convex_hull_2(begin, end, std::back_inserter(ch));
  std::vector<Point_2> max;
  typename Kernel::FT max_area = 0;
  auto size = ch.size();
  for (auto i = 0; i < size; ++i)
  {
    std::vector<Point_2> current;
    auto p0 = ch[i];
    auto p1 = ch[(i + 1) % size];
    auto s = Segment_2(p0, p1);
    //auto s = Segment_2(Point_2(0, 0), Point_2(0, 1));
    half_convex(begin, end, current, kernel, s);
    auto poly = Polygon_2(current.begin(), current.end());
    auto current_area = poly.area();
    auto simple = poly.is_simple();
    //std::cout << simple << std::endl;
    if (simple && current_area > max_area)
    {
      max_area = current_area;
      max = current;
      //std::cout << "new max: " << max_area << std::endl;
    }
  }
  for (auto it = max.begin(); it != max.end(); ++it) {
    *res = it->index();
    ++res;
  }

  return res;
}

template <typename InputIterator, typename Kernel_>
InputIterator half_convex(InputIterator begin, InputIterator end,
  std::vector<typename Kernel_::Point_2>& res, Kernel_& kernel, typename Kernel_::Segment_2 s)
{
  typedef Kernel_                       Kernel;
  typedef typename Kernel::Point_2      Point_2;
  std::vector<Point_2> v(begin, end);

  //auto compare_distance = OAP::signed_distance_from_line_comparator<Kernel>(s, kernel);
  //std::sort(v.begin(), v.end(), compare_distance);
  std::sort(v.begin(), v.end());
  //get lower ch points

  for (auto it = v.begin(); it != v.end();)
  {
    //std::cout << it->index() << std::endl;
    if (res.size() < 2 || CGAL::orientation(*(res.end() - 2), *(res.end() - 1), *it) != CGAL::RIGHT_TURN)
      res.push_back(*(it++));
    else
      res.pop_back();
  }
  //tag the points as part of the lower CH
  for (auto it = res.begin(); it != res.end(); ++it) {
    it->data()->set_lower_ch(true);
  }
  res.begin()->data()->set_glue(true);
  std::next(res.begin())->data()->set_glue(true);
  res.rbegin()->data()->set_glue(true);
  std::next(res.rbegin())->data()->set_glue(true);

  auto i = 0;
  for (auto it = v.rbegin(); it != v.rend(); ++it) {
    if (it->data()->lower_ch() == false)
    {
      res.push_back(*it);
    }
  }

  auto rightmost = v.begin()->x();
  auto leftmost = v.rbegin()->x();

  for (auto it = v.begin(); it != v.end(); ++it)
  {
    if (it->x() == rightmost || it->x() == leftmost)
    {
      it->data()->set_glue(true);
    }
  }
  return res.begin();
}

//template <typename InputIterator, typename Kernel_>
//InputIterator half_convex_plus(InputIterator begin, InputIterator end,
//  std::vector<typename Kernel_::Point_2>& res, Kernel_& kernel, typename Kernel_::Segment_2 s)
//{
//  typedef Kernel_                       Kernel;
//  typedef typename Kernel::Point_2      Point_2;
//  std::vector<Point_2> v(begin, end);
//  std::vector<Point_2> uh, lh;
//  CGAL::upper_hull_points_2(begin, end, std::back_inserter(uh));
//  CGAL::lower_hull_points_2(begin, end, std::back_inserter(lh));
//  //tag points for glue (to allow merge)
//  lh.begin()->data()->set_glue(true);
//  std::next(lh.begin())->data()->set_glue(true);
//  lh.rbegin()->data()->set_glue(true);
//  //uh.begin()->data()->set_glue(true);
//
//  //res.push_back(*(uh.begin()));
//  for (auto it = lh.begin(); it != lh.end(); ++it)
//  {
//    res.push_back(*it);
//    it->data()->set_lower_ch(true);
//  }
//  auto it = v.rbegin();
//  it->data()->set_glue(true);
//  std::vector<Point_2> rightmost;
//  rightmost.push_back(*it);
//  ++it;
//  while (rightmost.front().x() == it->x())
//  {
//    it->data()->set_glue(true);
//    rightmost.push_back(*it);
//    ++it;
//  }
//  for (auto rit = rightmost.rbegin(); rit != rightmost.rend(); ++rit)
//  {
//    if (rit->data()->lower_ch() == false) res.push_back(*rit);
//  }
//  for (; it != v.rend(); ++it) {
//    if (it->data()->lower_ch() == false) res.push_back(*it);
//  }
//  //res.push_back(*(uh.rbegin()));
//  return res.begin();
//  
//}

template <typename InputIterator, typename OutputIterator, typename Kernel_>
OutputIterator half_convex_fp(InputIterator begin, InputIterator end,
  OutputIterator res, Kernel_& kernel, typename Kernel_::Segment_2 s)
{
  typedef Kernel_                       Kernel;
  typedef typename Kernel::Point_2      Point_2;
  std::vector<Point_2> result;
  half_convex(begin, end, result, kernel, s);
  for (auto it = result.begin(); it != result.end(); ++it) {
    *res = it->index();
    ++res;
  }
  return res;
}

template <typename InputIterator, typename OutputIterator, typename Kernel_>
OutputIterator half_convex_fp(InputIterator begin, InputIterator end,
  OutputIterator res, Kernel_& kernel)
{
  typedef Kernel_                       Kernel;
  typedef typename Kernel::Point_2      Point_2;
  typedef typename Kernel::Segment_2    Segment_2;
  Segment_2 s = Segment_2(Point_2(0, 0), Point_2(0, 1)); //default
  return half_convex_fp(begin, end, res, kernel, s);
}

template <typename InputIterator, typename OutputIterator, typename Kernel>
OutputIterator half_convex_fp(InputIterator begin, InputIterator end,
  OutputIterator res)
{
  Kernel kernel;
  return half_convex_fp(begin, end, res, kernel);
}


OAP_END_NAMESPACE

#endif
