// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <nirgoren@mail.tau.ac.il>
//                              <efifogel@gmail.com>

#ifndef OAP_STAR_HPP
#define OAP_STAR_HPP

/*! \file
 *
 */

#include <CGAL/basic.h>

#include "OAP/basic.hpp"
#include "OAP/angle_comparator.hpp"

OAP_BEGIN_NAMESPACE

// assumes OutputIterator value_type to be indexed Point_2
template <typename InputIterator, typename OutputIterator, typename Kernel_>
OutputIterator star_fp(InputIterator begin, InputIterator end,
                       OutputIterator res, Kernel_& kernel)
{
  typedef Kernel_                       Kernel;
  typedef typename Kernel::Point_2      Point_2;

  // generates an invalid polygon if the pivot is the leftmost point and there
  // are points directly above it
  auto compare_xy = kernel.compare_xy_2_object();
  auto midpoint = kernel.construct_midpoint_2_object();
  std::vector<Point_2> v(begin, end);
  auto not_larger_xy =
    [&](const Point_2& p, const Point_2& q) -> bool
    { return (CGAL::LARGER != compare_xy(p, q)); };
  auto min_it = std::min_element(v.begin(), v.end(), not_larger_xy);
  auto max_it = std::max_element(v.begin(), v.end(), not_larger_xy);
  auto pivot = midpoint(*min_it, *max_it);
  std::sort(v.begin(), v.end(), angle_comparator<Kernel>(pivot, kernel));
  for (auto it = v.begin(); it != v.end(); ++it) *res++ = it->index();
  return res;
}

// assumes OutputIterator value_type to be indexed Point_2
template <typename InputIterator, typename OutputIterator, typename Kernel>
OutputIterator star_fp(InputIterator begin, InputIterator end,
                       OutputIterator res)
{
  Kernel kernel;
  return star_fp(begin, end, res, kernel);
}

OAP_END_NAMESPACE

#endif
