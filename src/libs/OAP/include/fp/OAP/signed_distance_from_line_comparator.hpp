// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <nirgoren@mail.tau.ac.il>
//                              <efifogel@gmail.com>

#ifndef OAP_SIGNED_DISTANCE_FROM_LINE_COMPARATOR_HPP
#define OAP_SIGNED_DISTANCE_FROM_LINE_COMPARATOR_HPP

#include "OAP/Extended_kernel.hpp"
#include <CGAL/basic.h>
#include "OAP/basic.hpp"

OAP_BEGIN_NAMESPACE

template <typename Kernel_>
class OAP_OAP_DECL signed_distance_from_line_comparator {
public:
  typedef Kernel_                       Kernel;
  typedef typename Kernel::Point_2      Point_2;
  typedef typename Kernel::Segment_2    Segment_2;
  typedef typename Kernel::Line_2       Line_2;

  /*! Construct from a line. */
  signed_distance_from_line_comparator(const Segment_2& segment, 
    const Kernel& kernel);

  /*! Compare the signed distance ...
   */
  bool operator()(const Point_2& b, const Point_2& c);

private:
  const Kernel& m_kernel;
  const Segment_2& m_segment;
  Line_2 m_line;
  enum Direction { up, down, right, left };
  short m_direction;
 
};

//! \brief construct from a line.
template <typename Kernel>
signed_distance_from_line_comparator<Kernel>::
signed_distance_from_line_comparator(const Segment_2& segment,
  const Kernel& kernel) :
  m_kernel(kernel),
  m_segment(segment)
{
  m_line = segment.supporting_line();
  if (m_segment.vertex(0).y() < m_segment.vertex(1).y())
  {
    m_direction = 1;
    return;
  }
  if (m_segment.vertex(0).y() == m_segment.vertex(1).y())
  {
    if (m_segment.vertex(0).x() < m_segment.vertex(1).x())
    {
      m_direction = 1;
    }
    else
    {
      m_direction = -1;
    }
    return;
  }
  m_direction = -1;


}

//! \brief compares the signed distance ...
//true iff p is "more to the right of m_segment" than q
template <typename Kernel>
bool signed_distance_from_line_comparator<Kernel>::operator()
(const Point_2& p, const Point_2& q)
{
  auto res = CGAL::compare_signed_distance_to_line(m_line, p, q)*m_direction;
  if (res > 0) return true;
  if (res == 0) return p > q;
  return false;
}

OAP_END_NAMESPACE

#endif
