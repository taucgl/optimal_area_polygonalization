// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <nirgoren@mail.tau.ac.il>
//                              <efifogel@gmail.com>

#ifndef OAP_ANGLE_COMPARATOR_HPP
#define OAP_ANGLE_COMPARATOR_HPP

#include <CGAL/basic.h>

#include "OAP/basic.hpp"

OAP_BEGIN_NAMESPACE

template <typename Kernel_>
class OAP_OAP_DECL angle_comparator {
public:
  typedef Kernel_                       Kernel;
  typedef typename Kernel::Point_2      Point_2;

  /*! Construct from a pivot point. */
  angle_comparator(const Point_2& p, const Kernel& kernel);

  /*! Compare the angle ...
   */
  bool operator()(const Point_2& b, const Point_2& c);

private:
  const Kernel& m_kernel;
  const Point_2& m_pivot;
};

//! \brief construct from a pivot point.
template <typename Kernel>
angle_comparator<Kernel>::angle_comparator(const Point_2& p,
                                           const Kernel& kernel) :
  m_kernel(kernel),
  m_pivot(p)
{}

//! \brief compares the angle ...
template <typename Kernel>
bool angle_comparator<Kernel>::operator()(const Point_2& p, const Point_2& q)
{
  auto compare_xy = m_kernel.compare_xy_2_object();

  if (compare_xy(m_pivot, p) == CGAL::EQUAL)
  {
    if (q.y() >= p.y() && q.x() <= p.x()) return true;
    return false;
  }

  if (compare_xy(m_pivot, q) == CGAL::EQUAL)
  {
    if (p.y() >= q.y() && p.x() <= q.x()) return false;
    return true;
  }

  auto orientation = m_kernel.orientation_2_object();
  auto o = orientation(m_pivot, q, p);

  if (o == CGAL::LEFT_TURN)
  {
    if (q.y() >= m_pivot.y() && (p.y() < m_pivot.y())) return true;
    return false;
  }
  if (o == CGAL::RIGHT_TURN)
  {
    if (q.y() < m_pivot.y() && (p.y() >= m_pivot.y())) return false;
    return true;
  }

  if (o == CGAL::COLLINEAR)
  {
    if (p.x() == m_pivot.x()) return (q.y() > p.y());
    if (p.y() == m_pivot.y()) return (q.x() < p.x());
    if (p.y() > m_pivot.y() && q.y() > m_pivot.y())
    {
      if (p.x() < m_pivot.x()) return (q.y() > p.y());
      return (q.y() < p.y());
    }
    if (p.y() > m_pivot.y() && q.y() < m_pivot.y()) return false;
    if (p.y() < m_pivot.y() && q.y() > m_pivot.y()) return true;
    return (q.y() > p.y());
  }
  return false;
}

OAP_END_NAMESPACE

#endif
