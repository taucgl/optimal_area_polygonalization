// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with OAP.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <nirgoren@mail.tau.ac.il>
//                              <efifogel@gmail.com>

#ifndef POINT_DATA_HPP
#define POINT_DATA_HPP


#include <CGAL/Constrained_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include "OAP/basic.hpp"
#include "OAP/Face_info.hpp"

OAP_BEGIN_NAMESPACE
template<typename Kernel>
struct Point_data {
  typedef typename std::list<typename Kernel::Point_2>::iterator list_iterator;
  typedef CGAL::Triangulation_vertex_base_2<Kernel>                       Vb;
  typedef CGAL::Triangulation_face_base_with_info_2<OAP::Face_info, Kernel>    Fbb;
  typedef CGAL::Constrained_triangulation_face_base_2<Kernel, Fbb>        Fb;
  typedef CGAL::Triangulation_data_structure_2<Vb, Fb>                    TDS;
  typedef CGAL::Exact_predicates_tag                                      Itag;
  typedef CGAL::Constrained_triangulation_2<Kernel, TDS, Itag>            Triangulation;
  typedef typename Triangulation::Vertex_handle                           Vertex_handle;

  Point_data(size_t index) : m_index(index) {}
  bool m_glue = false;
  bool m_lower_ch = false;
  size_t m_index;
  size_t m_position;
  list_iterator m_iterator;
  Vertex_handle m_vertex_handle;
  size_t index() const { return m_index; }
  size_t position() const {return m_position; }
  void set_position(size_t position) {m_position = position; }
  list_iterator iterator() const { return m_iterator; }
  void set_iterator(list_iterator it) { m_iterator = it; }
  Vertex_handle const vertex_handle() { return m_vertex_handle; }
  void set_vertex_handle(Vertex_handle h) { m_vertex_handle = h; }
  bool const glue() { return m_glue; }
  void set_glue(bool val) { m_glue = val; }
  bool const lower_ch() { return m_lower_ch; }
  void set_lower_ch(bool val) { m_lower_ch = val; }
  
};

OAP_END_NAMESPACE

#endif
