// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_FAILURE_BEHAVIOURS_HPP
#define OAP_FAILURE_BEHAVIOURS_HPP

#include "OAP/config.hpp"
#include "OAP/Failure_behaviour.hpp"

OAP_BEGIN_NAMESPACE

/*! Set the error behaviour mode.
 * \param[in] the new error behaviour mode.
 */
extern Failure_behaviour set_error_behaviour(Failure_behaviour mode);

/*! Obtain the error behaviour mode.
 * \return the current error behaviour mode.
 */
extern Failure_behaviour& get_error_behaviour();

/*! Set the warning behaviour mode.
 * \param[in] the new warning behaviour mode.
 */
extern Failure_behaviour set_warning_behaviour(Failure_behaviour mode);

/*! Obtain the warning behaviour mode.
 * \return the current warning behaviour mode.
 */
extern Failure_behaviour& get_warning_behaviour();

OAP_END_NAMESPACE

#endif
