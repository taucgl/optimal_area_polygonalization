// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <nirgoren@mail.tau.ac.il>
//                              <efifogel@gmail.com>

#ifndef OAP_BIGINT_KERNEL_HPP
#define OAP_BIGINT_KERNEL_HPP

#include "CGAL/Gmpz.hpp"

#include "OAP/basic.hpp"

OAP_BEGIN_NAMESPACE

typedef MyKernel<Gmpz>          Bigint_kernel;
typedef Bigint_kernel::Point_2  Bigint_point_2;

OAP_END_NAMESPACE

#endif
