// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_ASSERTIONS_HPP
#define OAP_ASSERTIONS_HPP

#include "OAP/config.hpp"
#include "OAP/Failure_behaviour.hpp"

OAP_BEGIN_NAMESPACE

// function declarations
// =====================
// failure functions
// -----------------
OAP_OAP_DECL void assertion_fail(const char*, const char*, int,
                                   const char* = 0);
OAP_OAP_DECL void precondition_fail(const char*, const char*, int,
                                      const char* = 0);
OAP_OAP_DECL void postcondition_fail(const char*, const char*, int,
                                       const char* = 0);

// warning function
// ----------------
OAP_OAP_DECL void warning_fail(const char*, const char*, int,
                                 const char* = 0);
// macro definitions
// =================
// assertions
// ----------

#if defined(OAP_NO_ASSERTIONS) || defined(NDEBUG)
#  define OAP_assertion(EX) (static_cast<void>(0))
#  define OAP_assertion_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_assertion_code(CODE)
#else
#  define OAP_assertion(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::assertion_fail(# EX , __FILE__, __LINE__))
#  define OAP_assertion_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::assertion_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_assertion_code(CODE) CODE
#endif

#if defined(OAP_NO_ASSERTIONS) || !defined(OAP_CHECK_EXACTNESS) \
  || defined(NDEBUG)
#  define OAP_exactness_assertion(EX) (static_cast<void>(0))
#  define OAP_exactness_assertion_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_exactness_assertion_code(CODE)
#else
#  define OAP_exactness_assertion(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::assertion_fail(# EX , __FILE__, __LINE__))
#  define OAP_exactness_assertion_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::assertion_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_exactness_assertion_code(CODE) CODE
#endif

#if defined(OAP_NO_ASSERTIONS) || !defined(OAP_CHECK_EXPENSIVE) \
  || defined(NDEBUG)
#  define OAP_expensive_assertion(EX) (static_cast<void>(0))
#  define OAP_expensive_assertion_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_expensive_assertion_code(CODE)
#else
#  define OAP_expensive_assertion(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::assertion_fail(# EX , __FILE__, __LINE__))
#  define OAP_expensive_assertion_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::assertion_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_expensive_assertion_code(CODE) CODE
#endif

#if defined(OAP_NO_ASSERTIONS) \
  || !defined(OAP_CHECK_EXACTNESS) \
  || !defined(OAP_CHECK_EXPENSIVE) \
  || defined(NDEBUG)
#  define OAP_expensive_exactness_assertion(EX) (static_cast<void>(0))
#  define OAP_expensive_exactness_assertion_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_expensive_exactness_assertion_code(CODE)
#else
#  define OAP_expensive_exactness_assertion(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::assertion_fail(# EX , __FILE__, __LINE__))
#  define OAP_expensive_exactness_assertion_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::assertion_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_expensive_exactness_assertion_code(CODE) CODE
#endif


// preconditions
// -------------

#if defined(OAP_NO_PRECONDITIONS) || defined(NDEBUG)
#  define OAP_precondition(EX) (static_cast<void>(0))
#  define OAP_precondition_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_precondition_code(CODE)
#else
#  define OAP_precondition(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::precondition_fail(# EX , __FILE__, __LINE__))
#  define OAP_precondition_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::precondition_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_precondition_code(CODE) CODE
#endif

#if defined(OAP_NO_PRECONDITIONS) || !defined(OAP_CHECK_EXACTNESS) \
  || defined(NDEBUG)
#  define OAP_exactness_precondition(EX) (static_cast<void>(0))
#  define OAP_exactness_precondition_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_exactness_precondition_code(CODE)
#else
#  define OAP_exactness_precondition(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::precondition_fail(# EX , __FILE__, __LINE__))
#  define OAP_exactness_precondition_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::precondition_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_exactness_precondition_code(CODE) CODE
#endif

#if defined(OAP_NO_PRECONDITIONS) || !defined(OAP_CHECK_EXPENSIVE) \
  || defined(NDEBUG)
#  define OAP_expensive_precondition(EX) (static_cast<void>(0))
#  define OAP_expensive_precondition_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_expensive_precondition_code(CODE)
#else
#  define OAP_expensive_precondition(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::precondition_fail(# EX , __FILE__, __LINE__))
#  define OAP_expensive_precondition_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::precondition_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_expensive_precondition_code(CODE) CODE
#endif

#if defined(OAP_NO_PRECONDITIONS) \
  || !defined(OAP_CHECK_EXACTNESS) \
  || !defined(OAP_CHECK_EXPENSIVE) \
  || defined(NDEBUG)
#  define OAP_expensive_exactness_precondition(EX) (static_cast<void>(0))
#  define OAP_expensive_exactness_precondition_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_expensive_exactness_precondition_code(CODE)
#else
#  define OAP_expensive_exactness_precondition(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::precondition_fail(# EX , __FILE__, __LINE__))
#  define OAP_expensive_exactness_precondition_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::precondition_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_expensive_exactness_precondition_code(CODE) CODE
#endif


// postconditions
// --------------

#if defined(OAP_NO_POSTCONDITIONS) || defined(NDEBUG)
#  define OAP_postcondition(EX) (static_cast<void>(0))
#  define OAP_postcondition_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_postcondition_code(CODE)
#else
#  define OAP_postcondition(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::postcondition_fail(# EX , __FILE__, __LINE__))
#  define OAP_postcondition_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::postcondition_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_postcondition_code(CODE) CODE
#endif

#if defined(OAP_NO_POSTCONDITIONS) || !defined(OAP_CHECK_EXACTNESS) \
  || defined(NDEBUG)
#  define OAP_exactness_postcondition(EX) (static_cast<void>(0))
#  define OAP_exactness_postcondition_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_exactness_postcondition_code(CODE)
#else
#  define OAP_exactness_postcondition(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::postcondition_fail(# EX , __FILE__, __LINE__))
#  define OAP_exactness_postcondition_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::postcondition_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_exactness_postcondition_code(CODE) CODE
#endif

#if defined(OAP_NO_POSTCONDITIONS) || !defined(OAP_CHECK_EXPENSIVE) \
  || defined(NDEBUG)
#  define OAP_expensive_postcondition(EX) (static_cast<void>(0))
#  define OAP_expensive_postcondition_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_expensive_postcondition_code(CODE)
#else
#  define OAP_expensive_postcondition(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::postcondition_fail(# EX , __FILE__, __LINE__))
#  define OAP_expensive_postcondition_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::postcondition_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_expensive_postcondition_code(CODE) CODE
#endif

#if defined(OAP_NO_POSTCONDITIONS) \
  || !defined(OAP_CHECK_EXACTNESS) \
  || !defined(OAP_CHECK_EXPENSIVE) \
  || defined(NDEBUG)
#  define OAP_expensive_exactness_postcondition(EX) (static_cast<void>(0))
#  define OAP_expensive_exactness_postcondition_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_expensive_exactness_postcondition_code(CODE)
#else
#  define OAP_expensive_exactness_postcondition(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::postcondition_fail(# EX , __FILE__, __LINE__))
#  define OAP_expensive_exactness_postcondition_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::postcondition_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_expensive_exactness_postcondition_code(CODE) CODE
#endif


// warnings
// --------

#if defined(OAP_NO_WARNINGS) || defined(NDEBUG)
#  define OAP_warning(EX) (static_cast<void>(0))
#  define OAP_warning_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_warning_code(CODE)
#else
#  define OAP_warning(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::warning_fail(# EX , __FILE__, __LINE__))
#  define OAP_warning_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::warning_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_warning_code(CODE) CODE
#endif

#if defined(OAP_NO_WARNINGS) || !defined(OAP_CHECK_EXACTNESS) \
  || defined(NDEBUG)
#  define OAP_exactness_warning(EX) (static_cast<void>(0))
#  define OAP_exactness_warning_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_exactness_warning_code(CODE)
#else
#  define OAP_exactness_warning(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::warning_fail(# EX , __FILE__, __LINE__))
#  define OAP_exactness_warning_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::warning_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_exactness_warning_code(CODE) CODE
#endif

#if defined(OAP_NO_WARNINGS) || !defined(OAP_CHECK_EXPENSIVE) \
  || defined(NDEBUG)
#  define OAP_expensive_warning(EX) (static_cast<void>(0))
#  define OAP_expensive_warning_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_expensive_warning_code(CODE)
#else
#  define OAP_expensive_warning(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::warning_fail(# EX , __FILE__, __LINE__))
#  define OAP_expensive_warning_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::warning_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_expensive_warning_code(CODE) CODE
#endif

#if defined(OAP_NO_WARNINGS) \
  || !defined(OAP_CHECK_EXACTNESS) \
  || !defined(OAP_CHECK_EXPENSIVE) \
  || defined(NDEBUG)
#  define OAP_expensive_exactness_warning(EX) (static_cast<void>(0))
#  define OAP_expensive_exactness_warning_msg(EX,MSG) (static_cast<void>(0))
#  define OAP_expensive_exactness_warning_code(CODE)
#else
#  define OAP_expensive_exactness_warning(EX) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::warning_fail(# EX , __FILE__, __LINE__))
#  define OAP_expensive_exactness_warning_msg(EX,MSG) \
  ((EX) ? (static_cast<void>(0)): \
   ::OAP::warning_fail(# EX , __FILE__, __LINE__, MSG))
#  define OAP_expensive_exactness_warning_code(CODE) CODE
#endif


// failure handler declarations
// ==========================
// failure handler
// ---------------
typedef void (*Failure_function)(const char*, const char*, const char*,
                                 int, const char*);

Failure_function set_error_handler(Failure_function handler);
Failure_function set_warning_handler(Failure_function handler);

// failure behaviour handler
// -------------------------
Failure_behaviour set_error_behaviour(Failure_behaviour eb);
Failure_behaviour set_warning_behaviour(Failure_behaviour eb);

// OAP error
#define OAP_error_msg(MSG) ::OAP::assertion_fail( "", __FILE__, __LINE__, MSG )
#define OAP_error()        ::OAP::assertion_fail( "", __FILE__, __LINE__ )

OAP_END_NAMESPACE

#endif
