// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_INFO_MESSENGERS_HPP
#define OAP_INFO_MESSENGERS_HPP

#include "OAP/basic.hpp"

OAP_BEGIN_NAMESPACE

#define OAP_info_msg(MSG) get_info_messenger()(MSG)

#define OAP_cond_msg(COND, MSG) \
  ((COND) ? OAP_info_msg(MSG) : (static_cast<void>(0)))

#define OAP_verbose_msg(LEVEL, MSG) \
  OAP_cond_msg(m_settings.verbose_level() >= LEVEL, MSG)

typedef void (*Info_function)(const std::string&);

/*! Set the info messenger function.
 * \param[in] messenger the new messenger function.
 */
extern Info_function set_info_messenger(Info_function messenger);

/*! Obtain the info messenger function.
 * \return the current info messenger function.
 */
extern Info_function& get_info_messenger();

/*! Handle info messages in a standard way.
 */
void standard_info_messenger(const std::string& msg);

/*! Handle info messages using a message box.
 */
void message_box_info_messenger(const std::string& msg);

OAP_END_NAMESPACE

#endif
