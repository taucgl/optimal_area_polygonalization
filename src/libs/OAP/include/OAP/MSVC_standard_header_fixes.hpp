// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SGAL_MSVC_STANDARD_HEADER_FIXES_HPP
#define SGAL_MSVC_STANDARD_HEADER_FIXES_HPP

#pragma warning ( disable : 4503 )
// #pragma warning ( once : 4503 )

#include <cmath>
namespace std {
  using ::sqrt;
}

#include <cstddef>
namespace std {
  using ::size_t;
  using ::ptrdiff_t;
}

#include <ctime>
namespace std {
  using ::time_t;
}

#endif
