// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_MESSAGE_BOX_HPP
#define OAP_MESSAGE_BOX_HPP

#include "OAP/basic.hpp"

OAP_BEGIN_NAMESPACE

/*! Display a message in a box.
 * \param[i] msg the message to display.
 * \param[i] type the message type:
 *           0 OK button with timeout=1 (not working!),
 *           1 OK button w/o timeout, 2 - message w/o buttons
 */
void message_box(const std::string& msg, int type = 0);

OAP_END_NAMESPACE

#endif
