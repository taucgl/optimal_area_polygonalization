// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_CONFIG_HPP
#define OAP_CONFIG_HPP

#include <boost/config.hpp>

#include "OAP/version.hpp"

//----------------------------------------------------------------------//
//             include platform specific workaround flags (OAP_CFG_...)
//----------------------------------------------------------------------//

// #include <OAP/compiler_config.h>

//----------------------------------------------------------------------//
//             do some post processing for the flags
//----------------------------------------------------------------------//

#ifdef OAP_CFG_NO_NAMESPACE
#  define OAP_USING_NAMESPACE_STD
#  define OAP_STD
#  define OAP std
#else
#  define OAP_USING_NAMESPACE_STD using namespace std;
#  define OAP_STD std
#  ifndef OAP_USE_NAMESPACE
#    define OAP_USE_NAMESPACE 1
#  endif
#endif

#if OAP_USE_NAMESPACE
#  define OAP_BEGIN_NAMESPACE namespace OAP {
#  define OAP_END_NAMESPACE }
#else
#  define OAP_BEGIN_NAMESPACE
#  define OAP_END_NAMESPACE
#endif

#ifdef OAP_CFG_NO_STDC_NAMESPACE
#  define OAP_CLIB_STD
#else
#  define OAP_CLIB_STD std
#endif

#ifndef OAP_CFG_NO_LONG_LONG
#  define OAP_USE_LONG_LONG
#endif

// FIXME: what is the problem with Sun 5.5? MATCHING_BUG_4 is not
// triggered, but there are runtime errors, e.g., with Distance_3,
// that do not appear when using the wrapper...
#if defined(OAP_CFG_MATCHING_BUG_4) || \
  (defined(__sun) && defined(__SUNPRO_CC))
namespace OAP {
    template < typename T >
    struct Self { typedef T Type; };
}
#  define OAP_WRAP(K) OAP::Self<K>::Type
#else
#  define OAP_WRAP(K) K
#endif

//----------------------------------------------------------------------//
//             include separate workaround files
//----------------------------------------------------------------------//

#ifdef _MSC_VER
#  include "OAP/MSVC_standard_header_fixes.hpp"
#endif
#if defined(__BORLANDC__) && __BORLANDC__ > 0x520
#include "OAP/Borland_fixes.hpp"
#endif
#if defined(__sun) && defined(__SUNPRO_CC)
#include "OAP/Sun_fixes.hpp"
#endif

//--------------------------------------------------------------------//
// This addresses a bug in VC++ 7.0 that (re)defines min(a, b)
// and max(a, b) in windows.h and windef.h
//-------------------------------------------------------------------//

#ifdef _MSC_VER
#define NOMINMAX 1
#endif

//-------------------------------------------------------------------//
// When the global min and max are no longer defined (as macros)
// because of NOMINMAX flag definition, we define our own global
// min/max functions to make the Microsoft headers compile. (afxtempl.h)
// Users that does not want the global min/max
// should define OAP_NOMINMAX
//-------------------------------------------------------------------//
#include <algorithm>
#if defined NOMINMAX && !defined OAP_NOMINMAX
using std::min;
using std::max;
#endif

// Is Geomview usable ?
#if !defined(__BORLANDC__) && !defined(_MSC_VER) && !defined(__MINGW32__)
#  define OAP_USE_GEOMVIEW
#endif

///////////////////////////////////////////////////////////////////////////////
// Windows DLL suport
#ifdef BOOST_HAS_DECLSPEC
#if defined(OAP_ALL_DYN_LINK) || defined(OAP_OAP_DYN_LINK)
// export if this is our own source, otherwise import:
//#ifdef OAP_OAP_SOURCE
# define OAP_OAP_DECL __declspec(dllexport)
//#else
//# define OAP_OAP_DECL __declspec(dllimport)
//#endif  // OAP_OAP_SOURCE
#endif  // DYN_LINK
#endif  // OAP_HAS_DECLSPEC

#ifndef OAP_OAP_DECL
#define OAP_OAP_DECL
#endif

#endif
