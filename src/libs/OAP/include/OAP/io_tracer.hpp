// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_IO_TRACER_HPP
#define OAP_IO_TRACER_HPP

#include "OAP/basic.hpp"
#include "OAP/Tracer.hpp"
#include "OAP/Attribute_error.hpp"

OAP_BEGIN_NAMESPACE

/*! Export a trace type to an output stream.
 */
template <typename OutputStream>
inline OutputStream& operator<<(OutputStream& os, const Tracer::Code& nt)
{
  os << nt;
  return os;
}

/*! Import a trace type from an input stream.
 */
template <typename InputStream>
inline InputStream& operator>>(InputStream& is, Tracer::Code& nt)
{
  std::string str;
  is >> str;
  for (size_t j = 0; j < static_cast<size_t>(Tracer::Code::CODE_SIZE); ++j) {
    if (Tracer::code_name(j) == str) {
      nt = static_cast<Tracer::Code>(j);
      return is;
    }
  }

  throw Attribute_error(std::string("Unrecognized trace type \"").append(str).append("\"!"));

  return is;
}

OAP_END_NAMESPACE

#endif
