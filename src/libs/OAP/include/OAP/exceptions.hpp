// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_EXCEPTIONS_HPP
#define OAP_EXCEPTIONS_HPP

#include <string>
#include <stdexcept>

#include "OAP/config.hpp"

OAP_BEGIN_NAMESPACE

// Failure types and exceptions
// ============================

//! Exception base class for all failure types of assertions etc.
class Failure_exception : public std::logic_error {
  std::string m_lib;
  std::string m_expr; // can be empty
  std::string m_file;
  int m_line;
  std::string m_msg;  // can be empty

public:
  //! initializes local members and the <tt>std::logic_error</tt> with
  //! a suitable message.
  Failure_exception(std::string lib, std::string expr, std::string file,
                    int line, std::string msg,
                    std::string kind = "Unknown kind") :
    std::logic_error(lib + std::string(" ERROR: ") + kind + std::string("!") +
                     ((expr.empty()) ? (std::string("")) :
                      (std::string("\nExpr: ") + expr)) +
                     std::string("\nFile: ") + file +
                     std::string("\nLine: ") + std::to_string(line) +
                     ((msg.empty()) ? (std::string("")) :
                      (std::string("\nExplanation: ") + msg))),
    m_lib(lib),
    m_expr(expr),
    m_file(file),
    m_line(line),
    m_msg(msg)
  {}

  ~Failure_exception() throw() {}

  //! the name of the library that issues this message.
  std::string library() const { return m_lib; }

  //! expression that failed in assertion, pre-, or postcondition.
  std::string expression() const { return m_expr; }

  //! source code filename where the failure was detected.
  std::string filename() const { return m_file; }

  //! line number in source code file where the failure was detected.
  int line_number() const { return m_line; }

  //! an optional message explaining the kind of failure.
  std::string message() const { return m_msg; }
};

//! Exception thrown for \c OAP_error_msg.
class Error_exception : public Failure_exception {
public:
  Error_exception(std::string lib, std::string msg, std::string file, int line) :
    Failure_exception(lib, "", file, line, msg, "failure") {}
};

//! Exception thrown for \c OAP_precond.
class Precondition_exception : public Failure_exception {
public:
  Precondition_exception(std::string lib, std::string expr, std::string file,
                         int line, std::string msg) :
    Failure_exception(lib, expr, file, line, msg, "precondition violation") {}
};

//! Exception thrown for \c OAP_postcond.
class Postcondition_exception : public Failure_exception {
public:
  Postcondition_exception(std::string lib, std::string expr, std::string file,
                          int line, std::string msg) :
    Failure_exception(lib, expr, file, line, msg, "postcondition violation") {}
};

//! Exception thrown for \c OAP_assert.
class Assertion_exception : public Failure_exception {
public:
  Assertion_exception(std::string lib, std::string expr, std::string file,
                      int line, std::string msg) :
    Failure_exception(lib, expr, file, line, msg, "assertion violation") {}
};

//! Exception thrown for \c OAP_warning.
class Warning_exception : public Failure_exception {
public:
  Warning_exception(std::string lib, std::string expr, std::string file,
                    int line, std::string msg) :
    Failure_exception(lib, expr, file, line, msg, "warning condition failed") {}
};

OAP_END_NAMESPACE

#endif
