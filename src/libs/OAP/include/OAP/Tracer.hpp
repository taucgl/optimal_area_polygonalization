// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_TRACER_HPP
#define OAP_TRACER_HPP

#include <string>
#include <vector>
#include <iostream>

#include "OAP/basic.hpp"

OAP_BEGIN_NAMESPACE

#ifdef TRACE
#undef TRACE
#endif

class OAP_OAP_DECL Tracer {
public:
  //! Supported Tracer codes.
  enum Code {
    OPTION1 = 0,
    OPTION2,
    CODE_SIZE
  };

  typedef std::vector<std::string>::const_iterator      Name_const_iterator;

  /*! Obtain the trace singleton.
   * \return the trace singleton.
   */
  static Tracer* get_instance();

  /*! Enable the trace for the given code.
   * \param code (in) the given code.
   */
  void enable(Code code);

  /*! Enable the trace for the given code-signature.
   * \param my_signature (in) the given code-signature.
   */
  void enable(size_t my_signature);

  /*! Disable the trace for the given code.
   * \param code (in) the given code.
   */
  void disable(Code code);

  /*! Disable the trace for the given code-signature.
   * \param my_signature (in) the given code-signature.
   */
  void disable(size_t my_signature);

  /*! Determine whether the trace for the given code is enabled.
   * \param code (in) the given code.
   * \return a flag that indicates whether the trace is enabled.
   */
  bool is_enabled(Code code) const;

  static const std::string& code_name(size_t i);

  /*! Obtain the begin iterator of option names.
   */
  static Name_const_iterator names_begin();

  /*! Obtain the past-the-end iterator of option names.
   */
  static Name_const_iterator names_end();
  //! The names of the codes.
  // We must use a container as opposed to an array on unknown size to
  // enable the validation of the command parsing.
  static const std::vector<std::string> s_code_names;

private:
  /*! Construct default.
   */
  Tracer();

  /*! Obtain the signature of the given trace-code.
   */
  size_t signature(Code code) const;

  //! The signature.
  size_t m_signature;

  //! The singleton.
  static Tracer* s_instance;
};

//! \brief enables the trace of a module identified by a the module code.
inline bool TRACE(Tracer::Code code)
{ return Tracer::get_instance()->is_enabled(code); }

//! \brief enables the trace for the given code.
inline void Tracer::enable(Code code) { enable(signature(code)); }

//! \brief enables the trace for the given code-signature.
inline void Tracer::enable(size_t my_signature) { m_signature |= my_signature; }

//! \brief disables the trace for the given code.
inline void Tracer::disable(Code code) { disable(signature(code)); }

//! \brief disables the trace for the given code-signature.
inline void Tracer::disable(size_t my_signature) { m_signature &= !my_signature; }

//! \brief determines whether the trace for the given code is enabled.
inline bool Tracer::is_enabled(Code code) const
{ return ((m_signature & signature(code)) != 0x0); }

//! \brief constructor.
inline Tracer::Tracer() : m_signature(0x0) {}

//! \brief obtains the signature of the given trace-code.
inline size_t Tracer::signature(Code code) const { return 0x1 << code; }

#if defined(NDEBUG) && !defined(OAP_TRACE)
#define OAP_TRACE_MSG(key, msg)
#define OAP_TRACE_CODE(key, code)
#else
#define OAP_TRACE_MSG(key, msg)     if (OAP::TRACE(key)) std::cout << msg
#define OAP_TRACE_CODE(key, code)   if (OAP::TRACE(key)) { code } else {}
#endif

OAP_END_NAMESPACE

#endif
