// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_FAILURE_MESSENGERS_HPP
#define OAP_FAILURE_MESSENGERS_HPP

#include "OAP/config.hpp"

OAP_BEGIN_NAMESPACE

typedef void (*Failure_function)(const char*, const char*, const char*,
                                 int, const char*);

/*! Set the error messenger function.
 * \param[in] messenger the new messenger function.
 */
extern Failure_function set_error_messenger(Failure_function messenger);

/*! Obtain the error messenger function.
 * \return the current error messenger function.
 */
extern Failure_function& get_error_messenger();

/*! Handle errors in a standard way.
 */
void standard_error_messenger(const char* what, const char* expr,
                              const char* file, int line, const char* msg);

/*! Handle errors using a message box.
 */
void message_box_error_messenger(const char* what, const char* expr,
                                 const char* file, int line,
                                 const char* explanation);

/*! Set the warning messenger function.
 * \param[in] messenger the new messenger function.
 */
extern Failure_function set_warning_messenger(Failure_function messenger);

/*! Obtain the warning messenger function.
 * \return the current warning messenger function.
 */
extern Failure_function& get_warning_messenger();


/*! Handle warning in a standard way.
 */
void standard_warning_messenger(const char *, const char* expr,
                                const char* file, int line, const char* msg);

/*! Handle warning using a message box.
 */
void message_box_warning_messenger(const char *, const char* expr,
                                   const char* file, int line,
                                   const char* explanation);

OAP_END_NAMESPACE

#endif
