// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_ERRORS_HPP
#define OAP_ERRORS_HPP

/*! \file Error exceptions for OAP
 */

#include <string>
#include <stdexcept>

#include "OAP/basic.hpp"

OAP_BEGIN_NAMESPACE

#if defined(_MSC_VER)
#pragma warning( push )
#pragma warning( disable: 4275 )
#endif

/*! Base class for all errors in the library */
class OAP_OAP_DECL error : public std::logic_error {
public:
  error(const std::string& what) : std::logic_error(what) {}
};

#if defined(_MSC_VER)
#pragma warning( pop )
#endif

/*! Class thrown when the input file cannot be found. */
class OAP_OAP_DECL File_not_found_error : public error {
public:
  File_not_found_error(const std::string& filename) :
    error(std::string("File ").append(filename).append(" not found!")) {}

  ~File_not_found_error() OAP_NOTHROW {}
};

/*! Class thrown when a required extension is not supported. */
class OAP_OAP_DECL Extension_not_supported_error : public error {
public:
  Extension_not_supported_error(const std::string& extension) :
    error(std::string("Extension ").append(extension).append(" not supported!"))
  {}

  ~Extension_not_supported_error() OAP_NOTHROW {}
};

/*! Class thrown when a required frame buffer is not allocated. */
class OAP_OAP_DECL Frame_buffer_not_allocated_error : public error {
public:
  Frame_buffer_not_allocated_error(const std::string& name) :
    error(std::string("Frame buffer ").append(name).append(" not allocated!"))
  {}

  ~Frame_buffer_not_allocated_error() OAP_NOTHROW {}
};

/*! Class thrown when a GLX version cannot be obtained. */
class OAP_OAP_DECL Glx_version_error : public error {
public:
  Glx_version_error() :
    error(std::string("Failed to obtain GLX version!")) {}

  ~Glx_version_error() OAP_NOTHROW {}
};

/*! Class thrown when a visual cannot be selected. */
class OAP_OAP_DECL Visual_selection_error : public error {
public:
  Visual_selection_error() :
    error(std::string("Failed to choose Visual!")) {}

  ~Visual_selection_error() OAP_NOTHROW {}
};

/*! Class thrown when a visual configuration fails. */
class OAP_OAP_DECL Visual_configuration_error : public error {
public:
  Visual_configuration_error() :
    error(std::string("Failed to configure Visual!")) {}

  ~Visual_configuration_error() OAP_NOTHROW {}
};

OAP_END_NAMESPACE

#endif
