#ifndef MYKERNEL_H
#define MYKERNEL_H

#include <boost/shared_ptr.hpp>

#include <CGAL/Cartesian.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Filtered_kernel.h>

#include "OAP/basic.hpp"
#include "OAP/Point_data.hpp"

OAP_BEGIN_NAMESPACE

// Kernel_ is the new kernel, and K_Base is the old kernel
template <typename Kernel_, typename KernelBase>
class My_cartesian_base : public KernelBase::template Base<Kernel_>::Type {
public:
  typedef Kernel_                                                  Kernel;
  typedef KernelBase                                               Kernel_base;

  typedef typename Kernel_base::template Base<Kernel>::Type        Old_kernel;
  typedef typename Old_kernel::Point_2                             Old_point_2;
  typedef typename Old_kernel::RT                                  RT;

  
  /*! The new point type */
  class New_point_2 : public Old_point_2 {

  private:
    typedef boost::shared_ptr<Point_data<Kernel>>  Shared_point_data;
    Shared_point_data m_data;
  public:
    // Constructors
    New_point_2() {}
    New_point_2(CGAL::Origin origin) : Old_point_2(origin) {}
    New_point_2(const RT& x, const RT& y) : Old_point_2(x, y) {}
    New_point_2(const RT& x, const RT& y, const RT& w) : Old_point_2(x, y, w) {}

    // Getters & setters
    void set_data(Shared_point_data data) { m_data = data; }
    Shared_point_data data() const { return m_data; }
    size_t index() const { return m_data->index(); }

    // Operators
    bool operator==(const New_point_2& p) const
    { return Old_point_2(*this) == Old_point_2(p); }

    bool operator!=(const New_point_2& p) const { return !(*this == p); }
  };

  template <typename KernelT, typename OldKernel>
  class New_construct_point_2 {
    typedef KernelT                     Kernel;

    typedef typename Kernel::RT         RT;
    typedef typename Kernel::Point_2    Point_2;
    typedef typename Point_2::Rep       Rep;
  public:
    typedef Point_2                     result_type;

    // Note : the CGAL::Return_base_tag is really internal CGAL stuff.
    // Unfortunately it is needed for optimizing away copy-constructions,
    // due to current lack of delegating constructors in the C++ standard.
    Rep operator()(CGAL::Return_base_tag) const { return Rep(); }
    Rep operator()(CGAL::Return_base_tag, CGAL::Origin origin) const
    { return Rep(origin); }
    Rep operator()(CGAL::Return_base_tag, const RT& x, const RT& y) const
    { return Rep(x, y); }
    Rep operator()(CGAL::Return_base_tag, const RT& x, const RT& y, const RT& w)
      const
    { return Rep(x, y, w); }

    Point_2 operator()() const { return New_point_2(); }
    Point_2 operator()(CGAL::Origin origin) const
    { return New_point_2(origin); }
	Point_2 operator() (const Point_2& p) const
	  {
      auto np = New_point_2(p);
      np.set_data(p.data());
      return New_point_2(np);
    }
    Point_2 operator()(const RT& x, const RT& y) const
    { return New_point_2(x, y); }
    Point_2 operator()(const RT& x, const RT& y, const RT& w) const
    { return New_point_2(x, y, w); }
  };

public:
  typedef New_point_2                                   Point_2;
  typedef New_construct_point_2<Kernel, Old_kernel>     Construct_point_2;

  Construct_point_2 construct_point_2_object() const
  { return Construct_point_2(); }

  template <typename KernelT>
  struct Base { typedef My_cartesian_base<KernelT, Kernel_base>  Type; };
};

template <typename FT_>
struct Extended_kernel : public CGAL::Type_equality_wrapper<
  My_cartesian_base<Extended_kernel<FT_>, CGAL::Cartesian<FT_> >,
  Extended_kernel<FT_> >
{};

template <typename FT_>
struct Extended_filtered_kernel : public CGAL::Filtered_kernel_adaptor<
  CGAL::Type_equality_wrapper<
    My_cartesian_base<Extended_filtered_kernel<FT_>,
                      CGAL::Simple_cartesian<FT_> >,
    Extended_filtered_kernel<FT_> >,
#ifdef CGAL_NO_STATIC_FILTERS
               false >
#else
               true >
#endif
{};

OAP_END_NAMESPACE

#endif
