// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_VERSION_HPP
#define OAP_VERSION_HPP

/*! \file
 * Caution, this is the only OAP header that is guarenteed
 * to change with every OAP release, including this header
 * will cause a recompile every time a new OAP version is
 * released.
 *
 * OAP_VERSION % 100 is the sub-minor version
 * OAP_VERSION / 100 % 1000 is the minor version
 * OAP_VERSION / 100000 is the major version
 */

#define OAP_VERSION 100001

//
//  OAP_LIB_VERSION must be defined to be the same as OAP_VERSION
//  but as a *string* in the form "x_y" where x is the major version
//  number and y is the minor version number.  This is used by
//  <config/auto_link.hpp> to select which library version to link to.

#define OAP_LIB_VERSION "1_01"

#endif
