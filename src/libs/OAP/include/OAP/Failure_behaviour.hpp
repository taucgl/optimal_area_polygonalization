// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_FAILURE_BEHAVIOUR_HPP
#define OAP_FAILURE_BEHAVIOUR_HPP

#include "OAP/config.hpp"

OAP_BEGIN_NAMESPACE

enum Failure_behaviour {
  ABORT = 0,
  EXIT,
  EXIT_WITH_SUCCESS,
  CONTINUE,
  THROW_EXCEPTION,
  FAILURE_BEHAVIOUR_SIZE
};

static const char* s_failure_behaviour_names[] = {
  "abort", "exit", "exit with success", "continue", "throw exception"
};

OAP_END_NAMESPACE

#endif
