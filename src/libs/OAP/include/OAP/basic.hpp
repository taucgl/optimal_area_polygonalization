// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_BASIC_HPP
#define OAP_BASIC_HPP

#include "OAP/config.hpp"

#include <iostream>
#include <cstdlib>

// Big endian or little endian machine.
// ====================================
#ifdef OAP_CFG_NO_BIG_ENDIAN
#  define OAP_LITTLE_ENDIAN 1
#else
#  define OAP_BIG_ENDIAN 1
#endif

#include "OAP/config.hpp"
#include "OAP/assertions.hpp"

// Symbolic constants to tailor inlining. Inlining Policy.
// =======================================================
#ifndef OAP_MEDIUM_INLINE
#  define OAP_MEDIUM_INLINE inline
#endif

#ifndef OAP_LARGE_INLINE
#  define OAP_LARGE_INLINE
#endif

#ifndef OAP_HUGE_INLINE
#  define OAP_HUGE_INLINE
#endif

// throw exception specification.
// ==============================
#if OAP_ENABLE_NOTHROW_EXCEPTION_SPECS
#  define OAP_NOTHROW throw ()
#else
#  define OAP_NOTHROW
#endif

#endif
