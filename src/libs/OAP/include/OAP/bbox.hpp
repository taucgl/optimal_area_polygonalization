// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <nirgoren@mail.tau.ac.il>
//                              <efifogel@gmail.com>

#ifndef OAP_SBOUNDING_BOX_HPP
#define OAP_SBOUNDING_BOX_HPP

#include "OAP/basic.hpp"

OAP_BEGIN_NAMESPACE

/*! Compute the bounding box of point in a ranges.
 */
template <typename Points, typename Kernel>
void bbox(const Points& points,
          typename Kernel::FT& min_x, typename Kernel::FT& min_y,
          typename Kernel::FT& max_x, typename Kernel::FT& max_y,
          const Kernel& kernel)
{
  typedef typename Kernel::FT   Ft;
  min_x = min_y = std::numeric_limits<Ft>::max();
  max_x = max_y = std::numeric_limits<Ft>::min();
  for (const auto& point : points) {
    min_x = std::min(min_x, point.x());
    min_y = std::min(min_y, point.y());
    max_x = std::max(max_x, point.x());
    max_y = std::max(max_y, point.y());
  }
}

OAP_END_NAMESPACE

#endif
