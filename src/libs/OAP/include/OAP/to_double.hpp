// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_TO_DOUBLE_HPP
#define OAP_TO_DOUBLE_HPP

#include <CGAL/basic.h>

#include "OAP/basic.hpp"

OAP_BEGIN_NAMESPACE

inline double to_double(long num) { return static_cast<double>(num); }
inline double to_double(long long num) { return static_cast<double>(num); }
inline double to_double(const CGAL::Gmpz& num) { return CGAL::to_double(num); }
inline double to_double(const CGAL::Gmpq& num) { return CGAL::to_double(num); }
inline double to_double(double num) {return num; }

OAP_END_NAMESPACE

#endif
