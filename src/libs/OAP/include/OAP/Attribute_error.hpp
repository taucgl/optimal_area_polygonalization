// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with OAP.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OAP_ATTRIBUTE_ERROR_HPP
#define OAP_ATTRIBUTE_ERROR_HPP

#include "OAP/basic.hpp"
#include "OAP/errors.hpp"

OAP_BEGIN_NAMESPACE

/*! Class thrown when an attribute is erroneous. */
class OAP_OAP_DECL Attribute_error : public error {
public:
  Attribute_error(const std::string& message) : error(message) {}
  ~Attribute_error() OAP_NOTHROW {}
};

OAP_END_NAMESPACE

#endif
