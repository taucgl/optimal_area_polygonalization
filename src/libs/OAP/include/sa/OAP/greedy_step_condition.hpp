// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <nirgoren@mail.tau.ac.il>
//                              <efifogel@gmail.com>

#ifndef GREEDY_STEP_CONDITION_HPP
#define GREEDY_STEP_CONDITION_HPP

/*! \file
 *
 */

#include <random>
#include <math.h>
#include "OAP/basic.hpp"
#include "OAP/to_double.hpp"

OAP_BEGIN_NAMESPACE

template<typename FT> class GreedyStepCondition
{
private:
  float m_temprature = 0;
  size_t m_size = 1;
  FT m_ch_area = 0;
public:
  GreedyStepCondition()
  {
    
  }
  GreedyStepCondition(double temprature, FT ch_area) : GreedyStepCondition()
  {
    m_temprature = temprature;
    m_ch_area = ch_area;
  }
  void set_temprature(double temprature)  {m_temprature = temprature; }
  void set_ch_area(FT ch_area) { m_ch_area = ch_area; }
  void set_size(size_t size) { m_size = size; }
  bool operator()(FT area_diff)
  {
    return (area_diff > 0);
  }
};
OAP_END_NAMESPACE

#endif
