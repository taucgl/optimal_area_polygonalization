// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <nirgoren@mail.tau.ac.il>
//                              <efifogel@gmail.com>

#ifndef OAP_SA_HPP
#define OAP_SA_HPP

/*! \file
 *
 */
#include <math.h>
#include <random>
#include <list>

#include <boost/optional.hpp>

#include <CGAL/convex_hull_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Kd_tree.h>
#include <CGAL/Orthogonal_k_neighbor_search.h>
#include <CGAL/Search_traits_2.h>
#include <CGAL/Fuzzy_iso_box.h>
#include <CGAL/intersections.h>
#include <CGAL/Constrained_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>

#include "OAP/basic.hpp"
#include "OAP/Face_info.hpp"
#include "OAP/bbox.hpp"

auto global_counter = 0;
OAP_BEGIN_NAMESPACE
// modifies the current polygon into a valid polygon where the order of a single
// pair of a point and its following point are switched
template <typename SA, typename StepCondition>
void trivial_step(SA& sa, StepCondition& sc) {
  typedef typename SA::kernel_type Kernel;
  size_t range = sa.size();

  size_t random_integer = 0;
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<size_t> uni(0,
                                            range - 1);  // guaranteed unbiased
  typename SA::iterator_type it;
  auto& points = sa.input_points();
  // obtain a valid step
  while (true) {
    random_integer = uni(rng);
    it = points[random_integer].data()->iterator();
    if (sa.validate_trivial_step(it)) break;
  }

  // check if the condition for the step is met
  auto area_diff = sa.trivial_step_area_diff(it);
  if (sc(area_diff)) {
    if(std::next(it) == sa.end())  sa.swap_positions(it, sa.begin());
    else sa.swap_positions(it, std::next(it));
    auto new_area = sa.area() + area_diff;
    //if (sa.max_area() > new_area) sa.permutation(std::back_inserter(perm));//std::cout << "new best area: " << new_area << std::endl;
    sa.set_area(new_area);

  }
}

//only checking pairs of points in close proximity
template <typename SA, typename StepCondition>
void move_after_step(SA& sa, StepCondition& sc) {
  typedef typename SA::kernel_type Kernel;
  size_t range = sa.size();
  typename SA::kernel_type::FT length = sa.query_square_length();
  size_t i = 0;
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<size_t> uni(0,
    range - 1);  // guaranteed unbiased
  auto trivial = -1;
  typename SA::iterator_type it0, it1;
  auto& points = sa.input_points();
  // obtain a valid step
  while (trivial < 0) {
    i = uni(rng);
    auto proximity_points = std::vector<typename Kernel::Point_2>();
    it0 = points[i].data()->iterator();
    //sa.proximity_points_k(it0, size_t(std::log(range)), proximity_points);
    //sa.proximity_points_k(it0, 10, proximity_points);
    sa.proximity_points(it0, length, proximity_points);
    for (auto it = proximity_points.begin(); it != proximity_points.end(); ++it)
    {
      it1 = it->data()->iterator();
      if (std::next(it0) == it1 ||
        std::next(it0) == sa.end() && it1 == sa.begin())
      {
        if (sa.validate_trivial_step(it0))
        {
          trivial = 1;
          break;
        }
        else continue;
      }
      if (std::next(it1) == it0 ||
        std::next(it1) == sa.end() && it0 == sa.begin())
      {
        if (sa.validate_trivial_step(it1))
        {
          trivial = 2;
          break;
        }
        else continue;
      }
      if (sa.validate_move_after_step(it0, it1))
      {
        trivial = 0;
        break;
      }
    }
  }

  typename SA::iterator_type it;
  if (trivial == 1) it = it0;
  if (trivial == 2) it = it1;
  if (trivial >= 1)
  {
    auto area_diff = sa.trivial_step_area_diff(it);
    if (sc(area_diff))
    {
      if (std::next(it) == sa.end())  sa.swap_positions(it, sa.begin());
      else sa.swap_positions(it, std::next(it));
      auto new_area = sa.area() + area_diff;
      sa.set_area(new_area);
    }
  }
  else
  {
    // check if the condition for the step is met
    auto area_diff = sa.move_after_step_area_diff(it0, it1);
    if (sc(area_diff)) {
      sa.move_after(it0, it1);
      auto new_area = sa.area() + area_diff;
      sa.set_area(new_area);
    }
  }
}

template <typename SA, typename StepCondition>
void move_after_step_old(SA& sa, StepCondition& sc) {
  typedef typename SA::kernel_type Kernel;

  size_t range = sa.size();

  size_t i = 0;
  size_t j = 0;
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<size_t> uni(0,
    range - 1);  // guaranteed unbiased
  auto trivial = -1;
  typename SA::iterator_type it0, it1;
  auto& points = sa.input_points();
// obtain a valid step
  while (true) {
    i = uni(rng);
    j = uni(rng);
    it0 = points[i].data()->iterator();
    it1 = points[j].data()->iterator();
    if (it0->data()->glue() || it1->data()->glue()) continue;
    if (std::next(it0) == it1 ||
      std::next(it0) == sa.end() && it1 == sa.begin())
    {
      if (sa.validate_trivial_step(it0))
      {
        trivial = 1;
        break;
      }
      else continue;
    }
    if (std::next(it1) == it0 ||
      std::next(it1) == sa.end() && it0 == sa.begin())
    {
      if (sa.validate_trivial_step(it1))
      {
        trivial = 2;
        break;
      }
      else continue;
    }
    if (sa.validate_move_after_step(it0, it1))
    {
      trivial = 0;
      break;
    }
  }
  typename SA::iterator_type it;
  if (trivial == 1) it = it0;
  if (trivial == 2) it = it1;
  if (trivial >= 1)
  {
    auto area_diff = sa.trivial_step_area_diff(it);
    if (sc(area_diff))
    {
      if (std::next(it) == sa.end())  sa.swap_positions(it, sa.begin());
      else sa.swap_positions(it, std::next(it));
      auto new_area = sa.area() + area_diff;
      sa.set_area(new_area);
    }
  }
  else
  {
    // check if the condition for the step is met
    auto area_diff = sa.move_after_step_area_diff(it0, it1);
    if (sc(area_diff)) {
      sa.move_after(it0, it1);
      auto new_area = sa.area() + area_diff;
      sa.set_area(new_area);
    }
  }
}

template <typename SA, typename StepCondition>
void swap_faces_step(SA& sa, StepCondition& sc) {
  typedef typename SA::kernel_type Kernel;
  typedef typename SA::Face_handle Face_handle;

  size_t range = sa.size();

  size_t i = 0;
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<size_t> uni(0,
    range - 1);  // guaranteed unbiased
  auto& points = sa.input_points();
  // obtain a valid step
  boost::optional<std::pair<Face_handle, Face_handle>> faces;
  while (true) {
    i = uni(rng);
    auto it = points[i].data()->vertex_handle();
    if (faces = sa.find_faces_for_swap_around_vertex(it)) break;
  }
  auto area_diff = sa.faces_area_diff(faces->first, faces->second);
  if (sc(area_diff)) {
    sa.swap_faces(faces->first, faces->second);
    auto new_area = sa.area() + area_diff;
    sa.set_area(new_area);
  }
}

template <typename SA, typename StepCondition>
void mixed_step(SA& sa, StepCondition& sc)
{
  float prob = sa.generic_step_frequency();
  if (prob > 1) return move_after_step_old(sa, sc);
  if (prob == 0) return trivial_step(sa, sc);
  if (prob == 1) return move_after_step(sa, sc);
  if (sa.generic_step_frequency() == -1) prob = 1.0 / sa.size();
  std::mt19937 rng;
  std::random_device rd;
  rng = std::mt19937(rd());
  auto uni_float = std::uniform_real_distribution<float>(0, 1);
  if (uni_float(rng) < prob) move_after_step(sa, sc);
  else trivial_step(sa, sc);
}

template <typename Points, typename Kernel_>
class SA {
  typedef Kernel_                                                              Kernel;
  typedef typename Kernel::FT                                                  FT;
  typedef typename Kernel::Point_2                                             Point_2;
  typedef typename std::list<Point_2>                                          list;
  typedef typename Kernel::Segment_2                                           Segment_2;
  typedef CGAL::Polygon_2<Kernel>                                              Polygon_2;
  typedef CGAL::Search_traits_2<Kernel>                                        Search_traits;
  typedef CGAL::Kd_tree<Search_traits>                                         Kd_tree;
  typedef CGAL::Orthogonal_k_neighbor_search<Search_traits>                    Neighbor_search;
  typedef typename Neighbor_search::Tree                                       Neighbor_search_tree;
  typedef CGAL::Triangulation_vertex_base_2<Kernel>                            Vb;
  typedef CGAL::Triangulation_face_base_with_info_2<OAP::Face_info, Kernel>    Fbb;
  typedef CGAL::Constrained_triangulation_face_base_2<Kernel, Fbb>             Fb;
  typedef CGAL::Triangulation_data_structure_2<Vb, Fb>                         TDS;
  typedef CGAL::Exact_predicates_tag                                           Itag;
  typedef CGAL::Constrained_triangulation_2<Kernel, TDS, Itag>                 Triangulation;


private:
  Points m_input_points;
  list m_points;
  const Kernel& m_kernel;
  Kd_tree m_kd_tree;
  Neighbor_search_tree m_neighbor_search_tree;
  double m_temprature;
  FT m_area;
  FT m_max_area;
  FT m_max_orthogonal_distance;
  FT m_query_square_length;
  size_t m_iterations;
  size_t m_new_record_counter = 0;
  size_t m_same_record_counter = 0;
  double m_generic_step_frequency;
  std::vector<size_t> m_saved_permutation;
  Triangulation tri;

public:
  typedef typename list::iterator iterator_type;
  typedef typename list::reverse_iterator reverse_iterator_type;
  typedef typename list::const_iterator const_iterator_type;
  typedef Kernel kernel_type;
  typedef typename TDS::Face_handle                                       Face_handle;
  typedef typename Triangulation::Vertex_handle                           Vertex_handle;

  SA(Points& points, const Kernel& kernel, size_t steps, double generic_step_frequency) :
    m_input_points(points), m_kernel(kernel), m_iterations(steps), m_generic_step_frequency(generic_step_frequency)
  {
    set_max_orthogonal_distance();
    m_kd_tree.insert(m_input_points.begin(), m_input_points.end());
    m_kd_tree.build();
    m_neighbor_search_tree.insert(m_input_points.begin(), m_input_points.end());
    //m_neighbor_search_tree = Neighbor_search_tree();
    tri = Triangulation();
  }

  double get_temprature() const { return m_temprature; }

  double generic_step_frequency() const { return m_generic_step_frequency; }

  Points& input_points() { return m_input_points; }

  iterator_type begin() { return m_points.begin(); }
  const_iterator_type begin() const { return m_points.begin(); }
  reverse_iterator_type rbegin() { return m_points.rbegin(); }

  iterator_type end() { return m_points.end(); }
  const_iterator_type end() const { return m_points.end(); }
  reverse_iterator_type rend() { return m_points.rend(); }

  size_t size() const { return m_input_points.size(); }

  FT query_square_length() const { return m_query_square_length; }

  void set_max_orthogonal_distance()
  {
    FT min_x, min_y, max_x, max_y;
    bbox(m_input_points, min_x, min_y, max_x, max_y, m_kernel);
    m_max_orthogonal_distance = std::max(max_x - min_x, max_y - min_y);
    //std::cout << m_max_orthogonal_distance << std::endl;
    //m_query_square_length = std::sqrt(std::log(size())/(size()*std::_Pi)) * m_max_orthogonal_distance;
    m_query_square_length = m_max_orthogonal_distance / (std::log(size()) * 2);
    //std::cout << m_query_square_length << std::endl;
  }

  FT area() const { return m_area; }

  void set_area(FT area)  {
    m_area = area;
    if (m_area > m_max_area)
    {
      m_max_area = m_area;
      m_same_record_counter = 0;
      ++m_new_record_counter;
      //std::cout << "new max after: " << global_counter << std::endl;
      //global_counter = 0;
    }
    else
    {
      ++m_same_record_counter;
    }
  }

  FT max_area() const { return m_max_area; }

  // compute the area difference between the current polygon and one where we
  // switch the order of the ith point and its following point
  FT trivial_step_area_diff(iterator_type it) {
    Point_2 a;
    if (it == begin()) a = *rbegin();
    else a = *std::prev(it);
    auto b = *it;
    Point_2 c;
    if (std::next(it) == end()) c = *begin();
    else c = *std::next(it);
    Point_2 d;
    if (std::next(it) == end()) d = *std::next(begin());
    else if(std::next(it, 2) == end()) d = *begin();
    else d = *std::next(it, 2);
    Point_2 origin(0, 0);

    Points t0 = {origin, a, b};
    Points t1 = {origin, b, c};
    Points t2 = {origin, c, d};

    Points s0 = {origin, a, c};
    Points s1 = {origin, c, b};
    Points s2 = {origin, b, d};
    auto curr_area = CGAL::polygon_area_2(t0.begin(), t0.end(), m_kernel) +
                     CGAL::polygon_area_2(t1.begin(), t1.end(), m_kernel) +
                     CGAL::polygon_area_2(t2.begin(), t2.end(), m_kernel);
    auto step_area = CGAL::polygon_area_2(s0.begin(), s0.end(), m_kernel) +
                     CGAL::polygon_area_2(s1.begin(), s1.end(), m_kernel) +
                     CGAL::polygon_area_2(s2.begin(), s2.end(), m_kernel);
    return (step_area - curr_area);
  }

  Points& proximity_points(iterator_type pit, FT length, Points& res)
  {
    auto p = *pit;
    Points points;
    auto smallest = Point_2(p.x() - length, p.y() - length);
    auto largest = Point_2(p.x() + length, p.y() + length);
    CGAL::Fuzzy_iso_box<Search_traits> rec(smallest, largest);
    m_kd_tree.search(std::back_inserter(points), rec);
    for (auto it = points.begin(); it != points.end(); ++it)
    {
      if(CGAL::squared_distance(p, *it) <= length * length)
        res.push_back(*it);
    }
    std::random_shuffle(res.begin(), res.end());
    return res;
  }

  Points& proximity_points_k(iterator_type pit, size_t k, Points& res)
  {
    auto p = *pit;
    Neighbor_search search(m_neighbor_search_tree, p, k);
    for (auto it = search.begin(); it != search.end(); ++it)
    {
      res.push_back(it->first);
    }
    std::random_shuffle(res.begin(), res.end());
    return res;
  }

  bool validate_trivial_step(iterator_type it)
  {
    Point_2 a;
    if (it == begin())
    {
      a = *rbegin();
    }
    else a = *std::prev(it);
    auto b = *it;
    Point_2 c;
    if (std::next(it) == end()) c = *begin();
    else c = *std::next(it);
    Point_2 d;
    if (std::next(it) == end()) d = *std::next(begin());
    else if (std::next(it, 2) == end()) d = *begin();
    else d = *std::next(it, 2);

    Points v = { a, b, c, d };
    for (const auto& point : v)
    {
      //std::cout << "involved: " << point << std::endl;
    }
    auto bbox = CGAL::bbox_2(v.begin(), v.end());
    auto s0 = Segment_2(a, c);
    auto s1 = Segment_2(b, d);
    if (CGAL::do_intersect(s0, s1))
    {
      //std::cout << "self intersection! s0: " << s0 << " s1: " << s1 << std::endl;
      return false;
    }
    std::vector<Segment_2> new_segments = { s0, s1 };
    Points res;
    auto smallest = Point_2(bbox.xmin(), bbox.ymin());
    auto largest = Point_2(bbox.xmax(), bbox.ymax());
    CGAL::Fuzzy_iso_box<Search_traits> rec(smallest, largest);
    m_kd_tree.search(std::back_inserter(res), rec);
    for (auto it = res.begin(); it != res.end(); ++it)
    {
      auto lit = it->data()->iterator();
      auto curr = *lit;
      Point_2 prev;
      if(lit == begin()) prev = *rbegin();
      else prev = *std::prev(lit);
      Point_2 next;
      if (std::next(lit) == end()) next = *begin();
      else next = *std::next(lit);
      Points points = { prev, curr, next };
      for (auto i = 0; i < 2; ++i)
      {
        auto s = Segment_2(points[i], points[i + 1]);
        for (const auto& new_segment : new_segments)
        {
          auto result = intersection(s, new_segment);
          if (result) {
            if (const Segment_2* seg = boost::get<Segment_2>(&*result))
            {
              //std::cout << *seg << std::endl;
              return false;
            }
            else
            {
              if (s.point(0) != new_segment.point(0) && s.point(0) != new_segment.point(1)
                && s.point(1) != new_segment.point(0) && s.point(1) != new_segment.point(1))
              {
                //std::cout << "intersection" << std::endl;
               return false;
              }
            }
          }

        }
      }
    }
    //std::cout << "ok!" << std::endl;
    return true;
  }

  bool validate_move_after_step(iterator_type it0, iterator_type it1)
  {
    if (it0 == it1) return false;
    Point_2 a;
    if (it0 == begin())
    {
      a = *rbegin();
    }
    else a = *std::prev(it0);
    auto b = *it0;
    Point_2 c;
    if (std::next(it0) == end()) c = *begin();
    else c = *std::next(it0);

    auto d = *it1;
    Point_2 e;
    if (std::next(it1) == end()) e = *begin();
    else e = *std::next(it1);

    auto s0 = Segment_2(a, c);
    auto s1 = Segment_2(d, b);
    auto s2 = Segment_2(b, e);
    std::vector<Segment_2> new_segments = { s0, s1, s2 };

    if (CGAL::do_intersect(s0, s1) || CGAL::do_intersect(s0, s2)) return false;
    for (auto it = begin(); it != end(); ++it)
    {
      Segment_2 s;
      if (std::next(it) == end()) s = Segment_2(*it, *begin());
      else s = Segment_2(*it, *std::next(it));
      for (const auto& new_segment : new_segments)
      {
        auto result = intersection(s, new_segment);
        if (result) {
          if (const Segment_2* seg = boost::get<Segment_2>(&*result))
          {
            return false;
          }
          else
          {
            if (s.point(0) != new_segment.point(0) && s.point(0) != new_segment.point(1)
              && s.point(1) != new_segment.point(0) && s.point(1) != new_segment.point(1))
            {
              //std::cout << "intersection" << std::endl;
              return false;
            }
          }
        }
      }
    }
    return true;
  }

  FT move_after_step_area_diff(iterator_type it0, iterator_type it1)
  {
    Point_2 a;
    if (it0 == begin())
    {
      a = *rbegin();
    }
    else a = *std::prev(it0);
    auto b = *it0;
    Point_2 c;
    if (std::next(it0) == end()) c = *begin();
    else c = *std::next(it0);

    auto d = *it1;
    Point_2 e;
    if (std::next(it1) == end()) e = *begin();
    else e = *std::next(it1);

    Points t = { a, b, c };
    Points s = { d, b, e };

    auto loss = CGAL::polygon_area_2(t.begin(), t.end(), m_kernel);
    //std::cout << "loss: " << loss << std::endl;
    auto gain = CGAL::polygon_area_2(s.begin(), s.end(), m_kernel);
    //std::cout << "gain: " << gain << std::endl;
    return gain - loss;
  }

  FT faces_area_diff(Face_handle inside, Face_handle outside)
  {
    auto t1 = tri.triangle(inside);
    auto t2 = tri.triangle(outside);
    // std::cout << t1.area() << " " << t2.area() << std::endl;
    return std::abs(t2.area()) - std::abs(t1.area());
  }

  void swap_positions(iterator_type it0, iterator_type it1)
  {
    auto temp = it0->data()->iterator();
    it0->data()->set_iterator(it1->data()->iterator());
    it1->data()->set_iterator(temp);
    std::iter_swap(it0, it1);
  }

  //move it0 to follow it1
  void move_after(iterator_type it0, iterator_type it1)
  {
    //std::cout << "moved " << *it0  << " " << *it1 << std::endl;
    auto p = *it0;
    if (std::next(it1) == end()) it1 = begin();
    else it1 = std::next(it1);
    auto it2 = m_points.insert(it1, *it0);
    m_points.erase(it0);
    p.data()->set_iterator(it2);
  }

  bool is_simple()
  {
    auto poly = CGAL::Polygon_2<Kernel>(begin(), end());
    for (auto it = m_points.begin(); it != m_points.end(); ++it)
    {
      if(*((*it).data()->iterator()) != *it) std::cout << "position error";
    }
    std::cout << std::endl;
    return poly.is_simple();
  }

  void first_position(std::vector<size_t>& p) {
    m_saved_permutation = p;
    auto shift = *(std::min_element(p.begin(), p.end()));
    m_points = list(size());
    auto lit = m_points.begin();
    for (auto it = p.begin(); it != p.end(); ++it) {
      m_input_points[*it - shift].data()->set_iterator(lit);
      *lit = m_input_points[*it - shift];
      ++lit;
    }
    m_area = CGAL::polygon_area_2(begin(), end(), m_kernel);
    m_max_area = m_area;
  }

  void save_state()
  {
    m_saved_permutation = std::vector<size_t>(size());
    permutation(m_saved_permutation.begin());
  }

  void restore_saved_state()
  {
    first_position(m_saved_permutation);
    m_new_record_counter = 0;
    m_same_record_counter = 0;
  }

  FT ch_area() const
  {
    Points ch;
    CGAL::convex_hull_2(begin(), end(), std::back_inserter(ch));
    return CGAL::polygon_area_2(ch.begin(), ch.end(), m_kernel);
  }


  template <typename StepFunction, typename StepCondition>
  void run_sa(StepFunction step, StepCondition sc) {
    m_temprature = 1.0f;
    sc.set_ch_area(ch_area());
    sc.set_size(size());
    for (size_t i = 0; i < m_iterations; ++i) {
      /*if ((m_iterations >= 20) && (i % (m_iterations/20) == 0))
        std::cout << "%: " << (100 * i) /m_iterations << std::endl;*/
      m_temprature = double(m_iterations - i) / m_iterations;
      sc.set_temprature(m_temprature);
      step(*this, sc);
      //if (m_new_record_counter == m_iterations / 1000)
      //{
      //  save_state();
      //  //std::cout << "saved state" << std::endl;
      //}
      //if (m_same_record_counter == m_iterations / 10)
      //{
      //  restore_saved_state();
      //  std::cout << "restored saved state" << std::endl;
      //  m_iterations += m_iterations / 20;
      //}
    }
  }

  template <typename OutputIterator>
  OutputIterator permutation(OutputIterator res) {
    for (auto it = begin(); it != end(); ++it) {
      *res = it->index();
      ++res;
    }
    return res;
  }

  void construct_triangulation()
  {
    tri = Triangulation();
    for (auto it = begin(); it != end(); ++it)
    {
      it->data()->set_vertex_handle(tri.insert(*it));
    }
    tri.insert_constraint(begin(), end(), true);
    mark_domains();
    for (auto it = tri.finite_vertices_begin(); it != tri.finite_vertices_end(); ++it)
    {
      //std::cout << it->point().index() << std::endl;
    }

    //std::cout << std::endl << std::endl;

    for (auto it = tri.finite_faces_begin(); it != tri.finite_faces_end(); ++it)
    {
      //std::cout << it->info().nesting_level << std::endl;
    }

    //std::cout << std::endl << std::endl;
    /*auto edge = tri.constrained_edges_begin();
    tri.remove_constrained_edge(edge->first, edge->second);
    mark_domains();
    for (auto it = tri.finite_faces_begin(); it != tri.finite_faces_end(); ++it)
    {
      std::cout << it->info().nesting_level << std::endl;
    }*/
  }

  void swap_faces(Face_handle of, Face_handle nf)
  {
    //std::cout << "info: " << std::endl;
    //std::cout << of->has_neighbor(nf) << std::endl;
    //std::cout << of->info().nesting_level << std::endl;
    //std::cout << nf->info().nesting_level << std::endl;

    Vertex_handle v;
    auto sign = 1;
    Face_handle fh = of;
    auto counter = 0;
    for (auto j = 0; j < 3; j++)
    {
      //std::cout << of->vertex(j)->point().index() << " ";
      typename Triangulation::Edge e(of, j);
      if (tri.is_constrained(e)) ++counter;
    }
    if (counter > 1)
    {
      fh = nf;
      sign = 2;
    }
    //std::cout << std::endl;
    //std::cout << "constraints: " << counter << std::endl;
    auto counter1 = 0;
    for (auto j = 0; j < 3; j++)
    {
      //std::cout << nf->vertex(j)->point().index() << " ";
      typename Triangulation::Edge e(nf, j);
      if (tri.is_constrained(e)) ++counter1;
    }
    //std::cout << std::endl;
    //std::cout << "constraints: " << counter << std::endl;


    int i;
    for (i = 0; i < 3; i++) {
      typename Triangulation::Edge e(fh, i);
      if (tri.is_constrained(e))
      {
        v = fh->vertex(i);
        break;
      }
    }
    auto next = fh->vertex((i + sign) % 3);
    auto it0 = v->point().data()->iterator();
    auto it1 = next->point().data()->iterator();
    move_after(it0, it1);
    construct_triangulation();
  }

  void mark_domains()
  {
    for (auto it = tri.all_faces_begin(); it != tri.all_faces_end(); ++it) {
      it->info().nesting_level = -1;
    }
    tri.infinite_face()->info().nesting_level = 0;
    std::list<typename Triangulation::Face_handle> queue =
      { tri.infinite_face() };
    while (!queue.empty()) {
      auto fh = queue.front();
      queue.pop_front();
      for (int i = 0; i < 3; i++) {
        typename Triangulation::Edge e(fh, i);
        typename Triangulation::Face_handle n = fh->neighbor(i);
        if (n->info().nesting_level == -1) {
          if (tri.is_constrained(e))
            n->info().nesting_level = (fh->info().nesting_level + 1) % 2;
          else n->info().nesting_level = fh->info().nesting_level;
          queue.push_back(n);
        }
      }
    }
    for (auto it = tri.all_faces_begin(); it != tri.all_faces_end(); ++it)
    {
      bool flag = false;
      for (int i = 0; i < 3; i++) {
        typename Triangulation::Edge e(it, i);
        if (tri.is_constrained(e)) flag = true;
      }
      if (!flag)
      {
        if (it->info().nesting_level == 0) it->info().nesting_level = -1;
        if (it->info().nesting_level == 1) it->info().nesting_level = 2;
      }
    }
  }

  boost::optional<std::pair<Face_handle, Face_handle>> find_faces_for_swap()
  {
    for (auto it = tri.finite_vertices_begin(); it != tri.finite_vertices_end(); ++it)
    {
      if (auto res = find_faces_for_swap_around_vertex(it))
      {
        return *res;
      }
    }
    return boost::none;

  }

  boost::optional<std::pair<Face_handle, Face_handle>> find_faces_for_swap_around_vertex(Vertex_handle vh)
  {
    auto first = tri.incident_faces(vh);
    auto curr = first;
    do
    {
      if (curr->info().nesting_level == 1)
      {
        //check if "it" is a good pivot
        auto i = curr->index(vh);
        int ear = -1;
        if (curr->neighbor((i + 1) % 3)->info().nesting_level <= 0 && curr->neighbor((i + 2) % 3)->info().nesting_level <= 0)
          ear = 1;
        if (curr->neighbor((i + 1) % 3)->info().nesting_level > 0 && curr->neighbor((i + 2) % 3)->info().nesting_level > 0)
          ear = 0;
        if (ear >= 0)
        {
          auto next = std::next(curr);
          while (next != curr)
          {
            if (!(tri.is_infinite(next)))
            {
              if (!(next->has_neighbor(curr))
                && next->info().nesting_level == 0)
              {
                auto j = next->index(vh);
                if ((ear + 1) % 2 == next->neighbor((j + 1) % 3)->info().nesting_level
                  && next->neighbor((j + 1) % 3)->info().nesting_level == next->neighbor((j + 2) % 3)->info().nesting_level)
                {
                  std::pair<Face_handle, Face_handle> val = { curr, next };
                  return val;
                }
              }
            }
            ++next;
          }
        }
      }
      ++curr;
    } while (curr != first);
    return boost::none;
  }

};

OAP_END_NAMESPACE

#endif
