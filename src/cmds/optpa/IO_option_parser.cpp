// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <iostream>
#include <boost/filesystem.hpp>

#include "IO_option_parser.hpp"
#include "utils.hpp"
#include "Settings.hpp"

namespace fi = boost::filesystem;

namespace std {

  template <typename OutputStream>
  OutputStream& operator<<(OutputStream& os, const std::vector<fi::path>& paths)
  {
    for (const auto& path : paths) {
      os << path.string();
      if (path != paths.back()) os << " ";
    }
    return os;
  }

}

//! \brief construct default.
IO_option_parser::IO_option_parser() :
  m_io_opts("IO options")
{
  typedef std::vector<fi::path>         Paths;

  // Options allowed on the command line, config file, or env. variables
  m_io_opts.add_options()
    ("input-path", po::value<Paths>()->composing()->
     default_value({Settings::s_def_input_path}),
     "input path")
    ("output-path", po::value<fi::path>()->
     default_value(Settings::s_def_output_path),
     "output path")
    ;
}

//! \brief Destruct.
IO_option_parser::~IO_option_parser() {}

//! \brief applies the options.
void IO_option_parser::apply() {}

//! \brief finds the input file.
std::string IO_option_parser::find_input_file(const std::string& const_filename)
{
  typedef std::vector<fi::path>         Paths;
  return find_file_fullname(variable_map()["input-path"].as<Paths>(),
                            const_filename);
}
