// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef IO_NUMBER_TYPE_HPP
#define IO_NUMBER_TYPE_HPP

#include "Number_type.hpp"

#include "OAP/basic.hpp"
#include "OAP/Attribute_error.hpp"

/*! Export a number type to an output stream.
 */
template <typename OutputStream>
inline OutputStream& operator<<(OutputStream& os, const Number_type& nt)
{
  os << s_number_type_names[static_cast<size_t>(nt)];
  return os;
}

/*! Import a number type from an input stream.
 */
template <typename InputStream>
inline InputStream& operator>>(InputStream& is, Number_type& nt)
{
  std::string str;
  is >> str;
  size_t size = sizeof(s_number_type_names) / sizeof(char*);
  auto it = std::find(&s_number_type_names[0], &s_number_type_names[size], str);
  if (&s_number_type_names[size] == it)
    throw OAP::Attribute_error(std::string("Unrecognized number type \"").
                               append(str).append("\"!"));

  nt = static_cast<Number_type>(std::distance(&s_number_type_names[0], it));
  return is;
}

#endif
