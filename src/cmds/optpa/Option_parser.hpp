// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef OPTION_PARSER_HPP
#define OPTION_PARSER_HPP

#include <boost/program_options.hpp>

#include "OAP/basic.hpp"

#include "Generic_option_parser.hpp"
#include "Number_type.hpp"
#include "IO_option_parser.hpp"

namespace po = boost::program_options;

class Settings;

class Option_parser : public Generic_option_parser, public IO_option_parser {
 public:
  /*! Construct default. */
  Option_parser();

  /*! Desstruct. */
  virtual ~Option_parser();

  /*! Parse the options.
   * \param[in] argc
   * \param[in] argv
   */
  void operator()(int argc, char* argv[]);

  /*! Apply the options.
   */
  void apply();

  /*! Add options to command line options.
   * \param options
   */
  void add_command_line_options(po::options_description& options);

  /*! Add options to config file options.
   * \param options
   */
  void add_config_file_options(po::options_description& options);

  /*! Add options to environment variable options.
   * \param options
   */
  void add_environment_options(po::options_description& options);

  /*! Add options to visible options.
   * \param options
   */
  void add_visible_options(po::options_description& options);

  /*! Obtain the name of an input file.
   * \param[in] the id of the file name to return;
   * \return the name of an input file.
   */
  const std::string& input_file(size_t id) const;

  /*! Obtain the number of names of input files.
   * \return the number of names of input files.
   */
  size_t num_input_files() const;

  /*! Determine whether an initial-polygon file has been provided
   */
  bool initial_polygon_file_count() const;

  /*! Obtain the initial-polygon file assuming one has been provide.
   */
  const std::string& initial_polygon_file() const;

  /*! Configure.
   * \param[in,out] settings.
   */
  void configure(Settings& settings);

  /*! Obtain the 'quite' mode.
   */
  bool quite_mode() const;

  /*! Obtain the verbose level.
   */
  size_t verbose_level() const;

  /*! Obtain the number of simulated-annealing steps.
   */
  size_t steps() const;

  /*! Obtain the SA constant.
*/
  double sa_constant() const;

  /*! Obtain the frequency of general steps.
 */
  double generic_step_frequency() const;

  /*! If true, runs SA on all supported initial polygons.
   */
  bool run_all_initial_polygons() const;

  /*! If true, runs SA on a star shaped initial polygon.
 */
  bool star_shaped_initial_polygon() const;

  /*! If true, runs SA using only trivial steps.
 */
  size_t trivial() const;

  /*! If true, uses merge strategy.
*/
  size_t merge() const;

  /*! Obtain the number type.
   */
  Number_type number_type() const;

  /*! Determine whether to use a filtered kernel.
   */
  bool filtered_kernel() const;

  /*! Obtain the variable map.
   */
  virtual const po::variables_map& variable_map() const;

 protected:
  //! Command line options.
  po::options_description m_cmd_line_opts;

  //! Config file options.
  po::options_description m_config_file_opts;

  //! Environment variable options.
  po::options_description m_environment_opts;

  //! Visible options.
  po::options_description m_visible_opts;

  //! The configuration option description.
  po::options_description m_config_opts;

  //! The hidden option description.
  po::options_description m_hidden_opts;

  //! Positional option description.
  po::positional_options_description m_positional_opts;

  //! The variable map.
  po::variables_map m_variable_map;

#if 0
  std::pair<char*, char*> m_env_var_option_names;

  const std::string& name_mapper<std::string, std::string>(std::string& src) { }
#endif

  /*! Obtain the visible options.
   */
  virtual const po::options_description& get_visible_opts() const;

 private:
  /*! Indicates whether to run in quite mode */
  bool m_quite;

  /*! Indicates whether to run SA on all initial polygons */
  bool m_run_all_initial_polygons;

  /*! Indicates whether to runs SA on a star shaped initial polygon */
  bool m_star_shaped_initial_polygon;

  /*! Indicates whether to only use trivial steps */
  size_t m_use_trivial_step;

  /*! Indicates whether to use merge strategy and subset size */
  size_t m_merge;
};

//! \brief adds options to command line options.
inline void Option_parser::add_command_line_options(
    po::options_description& options) {
  m_cmd_line_opts.add(options);
}

//! \brief adds options to config file options.
inline void Option_parser::add_config_file_options(
    po::options_description& options) {
  m_config_file_opts.add(options);
}

//! \brief adds options to environment variable options.
inline void Option_parser::add_environment_options(
    po::options_description& options) {
  m_environment_opts.add(options);
}

//! \brief adds options to visible options.
inline void Option_parser::add_visible_options(
    po::options_description& options) {
  m_visible_opts.add(options);
}

//! \brief obtains the variable map.
inline const po::variables_map& Option_parser::variable_map() const {
  return m_variable_map;
}

// \brief obtains the visible options.
inline const po::options_description& Option_parser::get_visible_opts() const {
  return m_visible_opts;
}

//! \brief obtains the 'quite' mode */
inline bool Option_parser::quite_mode() const { return m_quite; }

//! \brief obtains the verbose level */
inline size_t Option_parser::verbose_level() const {
  return m_variable_map["verbose-level"].as<size_t>();
}

//! \brief obtains the number of simulated-annealing steps.
inline size_t Option_parser::steps() const {
  return m_variable_map["steps"].as<size_t>();
}

//! \brief obtains the number of simulated-annealing steps.
inline double Option_parser::generic_step_frequency() const {
  return m_variable_map["generic_step_frequency"].as<double>();
}

//! \brief obtains SA constant.
inline double Option_parser::sa_constant() const {
  return m_variable_map["sa_constant"].as<double>();
}


//! \brief determines whether to use only trivial steps.
inline size_t Option_parser::trivial() const {
  return m_use_trivial_step;
}

//! \brief determines whether to use merge strategy.
inline size_t Option_parser::merge() const {
  return m_merge;
}

//! \brief determines whether to run on all initial polygons.
inline bool Option_parser::run_all_initial_polygons() const {
  return m_run_all_initial_polygons;
}

//! \brief determines whether to run on all initial polygons.
inline bool Option_parser::star_shaped_initial_polygon() const {
  return m_star_shaped_initial_polygon;
}

//! \brief obtains the number type.
inline Number_type Option_parser::number_type() const {
  return m_variable_map["number-type"].as<Number_type>();
}

//! \brief determines whether to use a filtered kernel.
inline bool Option_parser::filtered_kernel() const {
  return m_variable_map["filtered-kernel"].as<bool>();
}

#endif
