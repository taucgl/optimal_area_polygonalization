// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef GENERIC_OPTION_PARSER_HPP
#define GENERIC_OPTION_PARSER_HPP

#include <boost/program_options.hpp>

namespace po = boost::program_options;

class Generic_option_parser {
public:
  enum Generic_option_id { HELP, LICENSE, VERSION };
  struct Generic_option_exception {
    Generic_option_exception(Generic_option_id option) : m_option(option) {}
    Generic_option_id m_option;
  };

  /*! Construct. */
  Generic_option_parser();

  /*! Destruct. */
  virtual ~Generic_option_parser();

  /*! Obtain the variable map.
   * \return the variable map.
   */
  virtual const po::variables_map& variable_map() const = 0;

  /*! Apply the options.
   */
  void apply();

  /*! Obtain the generic-option description. */
  const po::options_description& get_generic_opts() const;

protected:
  //! The generic options.
  po::options_description m_generic_opts;

private:
  virtual const po::options_description& get_visible_opts() const = 0;

  // The assignment operator cannot be generated (because some of the data
  // members are const pointers), so we suppress it explicitly.
  // We also suppress the copy constructor.
  Generic_option_parser& operator=(const Generic_option_parser&) = delete;
  Generic_option_parser(const Generic_option_parser&) = delete;
};

//! \brief obtains the generic-option description.
inline const po::options_description&
Generic_option_parser::get_generic_opts() const
{ return m_generic_opts; }

#endif
