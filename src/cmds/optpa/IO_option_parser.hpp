// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with OAP.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef IO_OPTION_PARSER_HPP
#define IO_OPTION_PARSER_HPP

#include <string>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

class IO_option_parser {
public:
  /*! Construct. */
  IO_option_parser();

  /*! Destruct. */
  virtual ~IO_option_parser();

  /*! Obtain the variable map.
   * \return the variable map.
   */
  virtual const po::variables_map& variable_map() const = 0;

  /*! Apply the options.
   */
  void apply();

  /*! Obtain the IO-option description.
   * \return the IO-option description.
   */
  const po::options_description& get_io_opts() const;

  /*! Find the input file.
  * \return the input file full name if exists. Otherwise the empty string.
   */
  std::string find_input_file(const std::string& filename);

protected:
  //! The options.
  po::options_description m_io_opts;

private:
  // The assignment operator cannot be generated (because some of the data
  // members are const pointers), so we suppress it explicitly.
  // We also suppress the copy constructor.
  IO_option_parser& operator=(const IO_option_parser&) = delete;
  IO_option_parser(const IO_option_parser&) = delete;
};

#if defined(_MSC_VER)
#pragma warning( pop )
#endif

//! \brief obtains the IO-option description.
inline const po::options_description& IO_option_parser::get_io_opts() const
{ return m_io_opts; }

#endif
