// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include "Settings.hpp"

const size_t Settings::s_def_verbose_level(0);
const fi::path Settings::s_def_output_path("./output");
const fi::path Settings::s_def_input_path("./input");

//! \brief constructs.
Settings::Settings() :
  m_verbose_level(s_def_verbose_level),
  m_output_path(s_def_output_path)
{}
