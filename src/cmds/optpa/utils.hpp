// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with OAP.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef UTILS_HPP
#define UTILS_HPP

#include <iostream>
#include <cstdlib>

#include <boost/filesystem.hpp>

#include "OAP/assertions.hpp"

namespace fi = boost::filesystem;

//! \brief finds the input file.
template <typename Paths>
inline std::string find_file_fullname(const Paths& paths,
                                      const std::string& const_filename)
{
  std::string filename(const_filename);

  OAP_assertion(!filename.empty());

#if (defined _MSC_VER)
  // Convert the ROOT from cygwin path to windows path, if relevant:
  auto cygdrive = filename.substr(0, 10);
  if (cygdrive == std::string("/cygdrive/")) {
    filename.erase(0, 10);
    filename.insert(1, ":");
  }
#endif

  fi::path file_path(filename);
  if (file_path.is_complete()) {
    if (fi::exists(file_path)) return file_path.string();
    return std::string();
  }

  for (const auto& path : paths) {
    auto full_file_path = path / file_path;
    if (fi::exists(full_file_path)) return full_file_path.string();
  }
  return std::string();
}

//! \brief obtains a config file fullname.
inline const std::string get_config_file_fullname(const char* name)
{
  const auto* home = getenv("HOME");
  if (! home) {
    std::cerr << "HOME environment variable not set!" << std::endl;
    std::vector<fi::path> paths{"./.oap/"};
    return find_file_fullname(paths, name);
  }
  std::vector<fi::path> paths{"./.oap/", std::string(home) + "/.oap/"};
  return find_file_fullname(paths, name);
}

#endif
