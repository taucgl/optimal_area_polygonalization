// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <boost/filesystem.hpp>

namespace fi = boost::filesystem;

class Settings {
public:
  friend class Option_parser;
  friend class IO_option_parser;

  /*! Construct.
   */
  Settings();

  /*! Set the verbosity level.
   * \param[in] level the new verbosity level.
   */
  void set_verbose_level(size_t level);

  /*! Obtain the verbosity level.
   * \return the verbosity level.
   */
  size_t verbose_level() const;

  /*! Set the full output path.
   * \param[in] output_path the new output path.
   */
  void set_output_path(const fi::path& output_path);

  /*! Obtain the full output path.
   * \return the full output path.
   */
  const fi::path& output_path() const;

private:
  //! The verbosity level.
  size_t m_verbose_level;

  //! The full output path
  fi::path m_output_path;

  // Default value
  static const size_t s_def_verbose_level;
  static const fi::path s_def_output_path;
  static const fi::path s_def_input_path;
};

//! \brief sets the verbosity level.
inline void Settings::set_verbose_level(size_t level)
{ m_verbose_level = level; }

//! \brief obtains the verbosity level.
inline size_t Settings::verbose_level() const { return m_verbose_level; }

//! \brief sets the full output path.
inline void Settings::set_output_path(const fi::path& output_path)
{ m_output_path = output_path; }

//! \brief obtains the full output path.
inline const fi::path& Settings::output_path() const
{ return m_output_path; }

#endif
