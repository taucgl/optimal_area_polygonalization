// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#if defined(_MSC_VER)
#pragma warning ( disable : 4512 )
#endif

#include <iostream>
#include <fstream>

#include "OAP/version.hpp"

#include "Generic_option_parser.hpp"

namespace po = boost::program_options;

//! \brief construct.
Generic_option_parser::Generic_option_parser() :
  m_generic_opts("Generic Options")
{
  // Options allowed only on the command line
  m_generic_opts.add_options()
    ("help", "print help message")
    ("license", "print licence information")
    ("version", "print version string")
    ;
}

//! \brief destructor.
Generic_option_parser::~Generic_option_parser() {}

//! \brief applies the options.
void Generic_option_parser::apply()
{
  if (variable_map().count("help")) {
    std::cout << get_visible_opts() << std::endl;
    throw Generic_option_exception(HELP);
    return;
  }

  if (variable_map().count("version")) {
    std::cout << OAP_VERSION << std::endl;
    throw Generic_option_exception(VERSION);
    return;
  }

  if (variable_map().count("license")) {
    std::cout << "GPL-3.0+" << std::endl;
    throw Generic_option_exception(LICENSE);
    return;
  }
}
