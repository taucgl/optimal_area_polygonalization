// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with SGAL.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <string>
#include <vector>

#include <boost/filesystem.hpp>

#include "OAP/Tracer.hpp"
#include "OAP/assertions.hpp"
#include "OAP/basic.hpp"
#include "OAP/io_tracer.hpp"
#if 0
#include "OAP/Message_handler.hpp"
#include "OAP/failure_behaviours.hpp"
#include "OAP/failure_messengers.hpp"
#include "OAP/info_messengers.hpp"
#include "OAP/io_failure_behaviour.hpp"
#include "OAP/io_message_handler.hpp"
#endif

#include "Option_parser.hpp"
#include "Settings.hpp"
#include "utils.hpp"
#include "Number_type.hpp"
#include "io_number_type.hpp"

namespace fi = boost::filesystem;

//! \brief constructs.
Option_parser::Option_parser()
    : m_config_opts("Configuration options"),
      m_hidden_opts("Hidden options"),
      m_quite(true) {
  typedef std::vector<std::string> vs;
  typedef std::vector<OAP::Tracer::Code> vtc;

  // Extract trace options:
  auto* tracer = OAP::Tracer::get_instance();
  std::string trace_msg("trace options:\n");
  for (auto it = tracer->names_begin(); it != tracer->names_end(); ++it)
    if (!it->empty()) trace_msg += "  " + *it + "\n";

  // Options allowed on the command line, config file, or env. variables
  // clang-format off
  m_config_opts.add_options()
    ("quite,q", po::value<bool>(&m_quite)->default_value(true),
     "quite mode")
    ("verbose-level,v", po::value<size_t>()->
     default_value(Settings::s_def_verbose_level),
     "verbose level")
    ("trace,T",         po::value<vtc>()->composing(), trace_msg.c_str())
#if 0
    ("error-behaviour",
     po::value<OAP::Failure_behaviour>()->
     default_value(OAP::Failure_behaviour::THROW_EXCEPTION),
     "error behaviour options\n"
     "  abort\n  exit\n  exit with success\n  continue\n  throw exception")
    ("warning-behaviour",
     po::value<OAP::Failure_behaviour>()->
     default_value(OAP::Failure_behaviour::CONTINUE),
     "warning behaviour options\n"
     "  abort\n  exit\n  exit with success\n  continue\n  throw exception")
    ("error-messenger",
     po::value<OAP::Message_handler>()->
     default_value(OAP::Message_handler::STANDARD),
     "error messenger options\n  standard\n  message box")
    ("warning-messenger",
     po::value<OAP::Message_handler>()->
     default_value(OAP::Message_handler::STANDARD),
     "warning messenger options\n  standard\n  message box")
    ("info-messenger",
     po::value<OAP::Message_handler>()->
     default_value(OAP::Message_handler::STANDARD),
     "info messenger options\n  standard\n  message box")
#endif
    ("steps,s", po::value<size_t>()->default_value(1000),
     "No. of simulated-annealing steps")
    ("generic_step_frequency,g", po::value<double>()->default_value(-1),
      "Determines frequency of generic steps")
    ("sa_constant,c", po::value<double>()->default_value(1),
      "Determines the SA constant for the step condition (negative values (e.g -1) for minimum area)")
    ("run_all_initial_polygons,a",
     po::value<bool>(&m_run_all_initial_polygons)->default_value(false),
     "If true, runs SA on all supported initial polygons")
    ("star_shaped_initial_polygon,r",
      po::value<bool>(&m_star_shaped_initial_polygon)->default_value(false),
      "If true, runs SA on a star shaped initial polygon")
    ("initial-polygon-file,i", po::value<std::string>(),
     "Use initial polygon file")
    ("use_trivial_step,t",
      po::value<size_t>(&m_use_trivial_step)->default_value(0),
      "If true, runs SA using only trivial steps")
    ("merge,m",
      po::value<size_t>(&m_merge)->default_value(0),
      "split the input into subsets of the size of the value, run SA and merge the reults")
    ("number-type,n",
     po::value<Number_type>()->default_value(Number_type::NT_LONG_LONG),
     "number type")
    ("filtered-kernel,f", po::value<bool>()->default_value(false),
     "filtered kernel")
    ;
  // clang-format on

  // Options hidden to the user. Allowed only on the command line:
  m_hidden_opts.add_options()("input-file", po::value<vs>(), "input file");

  m_visible_opts.add(m_generic_opts).add(m_io_opts).add(m_config_opts);
  m_cmd_line_opts.add(m_visible_opts).add(m_hidden_opts);
  m_config_file_opts.add(m_config_opts).add(m_io_opts).add(m_hidden_opts);
  m_environment_opts.add(m_config_opts).add(m_io_opts);

  m_positional_opts.add("input-file", -1);
}

//! \brief desstructs.
Option_parser::~Option_parser() {}

//! \brief parses the options.
void Option_parser::operator()(int argc, char* argv[]) {
  po::store(po::command_line_parser(argc, argv)
                .options(m_cmd_line_opts)
                .positional(m_positional_opts)
                .run(),
            m_variable_map);

  auto fullname = get_config_file_fullname("oap.cfg");
  if (!fullname.empty()) {
    std::ifstream ifs;
    ifs.open(fullname);
    if (ifs.is_open()) {
      po::store(po::parse_config_file(ifs, m_config_file_opts), m_variable_map);
      ifs.close();
    }
  }
  po::notify(m_variable_map);

  // std::pair<Char*,Char*> Option_parser::s_env_var_option_names[] = {
  // {"OAP_QUITE", "quite"},
  // {"OAP_VERBOSE", "verbose"}
  // };
}

//! \brief applies the options.
void Option_parser::apply() {
  Generic_option_parser::apply();
  IO_option_parser::apply();
}

//! \brief configures the scene graph.
void Option_parser::configure(Settings& settings) {
  typedef std::vector<OAP::Tracer::Code> vtc;
  if (m_variable_map.count("trace")) {
    const auto& trace_codes = m_variable_map["trace"].as<vtc>();
    for (const auto& code : trace_codes)
      OAP::Tracer::get_instance()->enable(code);
  }

  settings.set_verbose_level(m_variable_map["verbose-level"].as<size_t>());

  // Output path
  auto& output_path = m_variable_map["output-path"].as<fi::path>();
  // The following will generate a unique path, but we prefer readable time &
  // date.
  // fi::path full_output_path(fi::unique_path(output_path / "%%%%-%%%%-%%%%"));
  auto t = std::time(nullptr);
  auto timeinfo = std::localtime(&t);
  char buffer[32];
  strftime(buffer, 80, "%Y-%m-%d_%I-%M-%S", timeinfo);
  fi::path full_output_path(output_path / buffer);
  settings.set_output_path(full_output_path);

#if 0
  OAP::set_error_behaviour(m_variable_map["error-behaviour"].
                      as<OAP::Failure_behaviour>());
  set_warning_behaviour(m_variable_map["warning-behaviour"].
                        as<OAP::Failure_behaviour>());

  // Set the error messenger
  switch (m_variable_map["error-messenger"].as<OAP::Message_handler>()) {
   case OAP::Message_handler::STANDARD:
    OAP::set_error_messenger(OAP::standard_error_messenger);
    break;

   case OAP::Message_handler::MESSAGE_BOX:
    OAP::set_error_messenger(OAP::message_box_error_messenger);
    break;

   default: OAP_error();
  }

  // Set the warning messenger
  switch (m_variable_map["warning-messenger"].as<OAP::Message_handler>()) {
   case OAP::Message_handler::STANDARD:
    OAP::set_warning_messenger(OAP::standard_warning_messenger);
    break;

   case OAP::Message_handler::MESSAGE_BOX:
    OAP::set_warning_messenger(OAP::message_box_warning_messenger);
    break;

   default: OAP_error();
  }

  // Set the info handler
  switch (m_variable_map["info-messenger"].as<OAP::Message_handler>()) {
   case OAP::Message_handler::STANDARD:
    OAP::set_info_messenger(OAP::standard_info_messenger);
    break;

   case OAP::Message_handler::MESSAGE_BOX:
    OAP::set_info_messenger(OAP::message_box_info_messenger);
    break;

   default: OAP_error();
  }
#endif
}

//! \brief obtains the number of names of input files.
size_t Option_parser::num_input_files() const {
  return m_variable_map.count("input-file");
}

//! \brief obtains the base file-name.
const std::string& Option_parser::input_file(size_t id) const {
  OAP_assertion(id < m_variable_map.count("input-file"));
  typedef std::vector<std::string> vs;
  return m_variable_map["input-file"].as<vs>()[id];
}

//! \brief determines whether an initial-polygon file has been provided
bool Option_parser::initial_polygon_file_count() const
{ return m_variable_map.count("initial-polygon-file"); }

//! \brief obtains the initial-polygon file assuming one has been provide.
const std::string& Option_parser::initial_polygon_file() const
{
  OAP_assertion(0 != m_variable_map.count("initial-polygon-file"));
  return m_variable_map["initial-polygon-file"].as<std::string>();
}
