// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University
//
// This file is part of Tel Aviv University; you can redistribute it or
// modify it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the License.
// See the file LICENSE.LGPL distributed with OAP.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>
//            Nir Goren         <nirgoren@mail.tau.ac.il>

#include <fstream>
#include <list>
#include <sstream>
#include <string>
#include <vector>
#include <chrono>

#include <boost/filesystem/path.hpp>
#include <boost/shared_ptr.hpp>

#include "OAP/Extended_kernel.hpp"
#include "OAP/basic.hpp"
#include "OAP/half_convex.hpp"
#include "OAP/sa.hpp"
#include "OAP/some_step_condition.hpp"
#include "OAP/greedy_step_condition.hpp"
#include "OAP/star.hpp"
#include "OAP/Point_data.hpp"
#include "OAP/to_double.hpp"
#include "OAP/bbox.hpp"

#include "Option_parser.hpp"
#include "Settings.hpp"
#include "utils.hpp"
#include "Number_type.hpp"
#include "io_number_type.hpp"

namespace fi = boost::filesystem;

typedef std::vector<size_t>                             Permutation;
typedef std::pair<std::string, const Permutation*>      Initial_polygon;
typedef std::chrono::high_resolution_clock              Clock;

Settings settings;
Option_parser option_parser;

enum Error_id {
  FILE_NOT_FOUND,
  ILLEGAL_EXTENSION,
  UNABLE_TO_LOAD,
  INVALID_INITIAL_POLYGON,
  UNSUPPORTED
};

struct Illegal_input : public std::logic_error {
  Illegal_input(Error_id /* err */, const std::string &msg,
    const std::string &filename) :
    std::logic_error(std::string(msg).append(" (").append(filename).
      append(")!"))
  {}

  Illegal_input(Error_id /* err */, const std::string &msg) :
    std::logic_error(std::string(msg).append("!"))
  {}
};

struct Input_file_missing_error : public std::logic_error {
  Input_file_missing_error(std::string &str) : std::logic_error(str) {}
};

void init(int argc, char *argv[]) {
  option_parser(argc, argv);
  option_parser.apply();
  option_parser.configure(settings);
}

/*! Read points from an input stream.
 */
template <typename Points, typename Kernel>
void read(std::ifstream& infile, Points& points, const Kernel& kernel)
{
  typedef typename Kernel::FT Ft;
  typedef typename Kernel::Point_2 Point_2;
  std::string line;
  while (std::getline(infile, line)) {
    std::istringstream iss(line);
    size_t i;
    Ft x, y;
    if (!(iss >> i >> x >> y)) break;

    boost::shared_ptr<OAP::Point_data<Kernel>> data{new OAP::Point_data<Kernel>(i)};
    Point_2 p = Point_2(x, y);
    p.set_data(data);
    points.push_back(p);
  }

}

/*! Run the simulated-annealing process for a given number of steps.
 */
template <typename Points, typename Kernel>
Permutation run_sa(Points& points, const Initial_polygon& initial_polygon,
  size_t steps, const Kernel& kernel, bool trivial, double generic_step_frequency, double c = 1.0, bool print = true, bool greedy = false, int time = 0)
{
  typedef typename Kernel::FT                           Ft;
  typedef typename OAP::SomeStepCondition<Ft>           Some_step_condition;
  typedef typename OAP::GreedyStepCondition<Ft>         Greedy_step_condition;
  typedef OAP::SA<Points, Kernel>                       Sa;

  if (option_parser.verbose_level() > 0)
  {
    std::cout << "Running SA with initial polygon " << initial_polygon.first
      << std::endl;
  }
  auto t1 = Clock::now();
  // run sa on the permutation
  auto step = OAP::mixed_step<Sa, Some_step_condition>;
  if (trivial) step = OAP::trivial_step<Sa, Some_step_condition>;
  auto sc = Some_step_condition(c);
  auto gsc = Greedy_step_condition();
  auto greedy_step = OAP::trivial_step<Sa, Greedy_step_condition>;
  auto perm = *(initial_polygon.second);
  Sa sa(points, kernel, steps, generic_step_frequency);
  sa.first_position(perm);
  //sa.construct_triangulation();
  double ch_area = OAP::to_double(sa.ch_area());
  double initial_area = OAP::to_double(sa.area());
  double ratio = initial_area / ch_area;
  if (option_parser.verbose_level() > 0)
  {
    std::cout << "Convex Hull area: " << ch_area << std::endl;
    std::cout << "Initial polygon area: " << initial_area << std::endl;
    std::cout << "Ratio(pc): " << ratio * 100 << std::endl;
  }
  if (greedy) sa.run_sa(greedy_step, gsc);
  else sa.run_sa(step, sc);
  double post_sa_area = OAP::to_double(sa.area());
  double post_sa_ratio = post_sa_area / ch_area;
  auto t2 = Clock::now();
  if (option_parser.verbose_level() > 0)
  {
    std::cout << "Post SA polygon area: " << post_sa_area << std::endl;
    std::cout << "Post SA ratio(pc): " << post_sa_ratio * 100 << std::endl;
    std::cout << "Area of CH gained(pc): " << (post_sa_ratio - ratio) * 100
      << std::endl;
    std::cout << "simple: " << sa.is_simple() << std::endl;
    std::cout << "SA running time: "
      << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() + time
      << " milliseconds" << std::endl;
  }
  else if(print)
  {
    std::cout << "# Running time: "
      << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() + time
      << " milliseconds" << std::endl;
    std::cout << "# Convex Hull area: " << ch_area << std::endl;
    std::cout << "# Final polygon area: " << post_sa_area << std::endl;
    std::cout << "# Final ratio(pc): " << post_sa_ratio * 100 << std::endl;
  }

  //std::cout << "area " << CGAL::polygon_area_2(sa.begin(), sa.end(), kernel) << std::endl;
  //print permutation
  //sa.construct_triangulation();
  //for (auto i = 0; i < 9; i++)
  //{
  //  auto v = sa.input_points()[i].data()->vertex_handle();
  //  if (auto faces = sa.find_faces_for_swap_around_vertex(v))
  //  {
  //    std::cout << "found faces for swap" << std::endl;
  //    sa.swap_faces(faces->first, faces->second);
  //  }
  //}
  //std::cout << "area: " << CGAL::polygon_area_2(sa.begin(), sa.end(), kernel) << std::endl;
  Permutation p;
  sa.permutation(std::back_inserter(p));
  if (print)
  {
    for (auto it = p.begin(); it != p.end(); ++it)
    {
      std::cout << *it << std::endl;
    }
  }
  return p;
  //sa.move_after(std::next(sa.begin()), std::prev(sa.end()));
  //for (auto i = 0; i < 1; i++)
  //{
  //  auto v = sa.input_points()[i].data()->vertex_handle();
  //  if (auto faces = sa.find_faces_for_swap())
  //  {
  //    std::cout << "found faces for swap" << std::endl;
  //    sa.swap_faces(faces->first, faces->second);
  //  }
  //}
  //p.clear();
  //sa.permutation(std::back_inserter(p));
  //for (auto it = p.begin(); it != p.end(); ++it)
  //{
  //  std::cout << *it << " ";
  //}
}



Permutation glue(Permutation l, Permutation r, size_t glue_index)
{
  Permutation res;
  auto it0 = l.begin();
  for (; it0 != l.end(); ++it0)
  {
    if (*it0 != glue_index) res.push_back(*it0);
    else break;
  }
  auto it1 = r.begin();
  for (; it1 != r.end(); ++it1)
  {
    if (*it1 == glue_index)
    {
      ++it1;
      break;
    }
  }
  for (; it1 != r.end(); ++it1)
  {
    res.push_back(*it1);
  }
  it1 = r.begin();
  for (; it1 != r.end(); ++it1)
  {
    if (*it1 != glue_index) res.push_back(*it1);
    else break;
  }
  for (; it0 != l.end(); ++it0)
  {
    res.push_back(*it0);
  }
  return res;
}

/*! Run with a specific kernel.
 */
template <typename Kernel_>
void run(std::ifstream& infile, const Kernel_& kernel)
{
  typedef Kernel_                                       Kernel;
  typedef typename Kernel::Point_2                      Point_2;
  typedef typename Kernel::FT                           Ft;
  typedef typename std::vector<Point_2>                 Points;

  auto t1 = Clock::now();
  // std::cout << sizeof(Ft) << std::endl;

  // Read the points:
  Points in;
  read(infile, in, kernel);
  // print coordinates of points
  // for (const auto& point : in) std::cout << it << std::endl;

  if (option_parser.initial_polygon_file_count()) {
    const auto& ip_filename = option_parser.initial_polygon_file();

    Permutation polygon(in.size());
    std::ifstream infile(ip_filename);
    std::string line;
    size_t index(0);
    while (std::getline(infile, line)) {
      if ('#' == line[0]) continue;
      std::istringstream iss(line);
      size_t i;
      if (!(iss >> i)) break;
      if (index >= in.size()) {
        throw Illegal_input(INVALID_INITIAL_POLYGON,
                            "number of indices does not match",
                            std::to_string(polygon.size()));
        return;
      }
      polygon[index++] = i;
    }
    if (index < in.size()) {
      throw Illegal_input(INVALID_INITIAL_POLYGON,
                          "number of indices does not match",
                          std::to_string(polygon.size()));
      return;
    }
    Initial_polygon initial_polygon("initial", &polygon);
    run_sa(in, initial_polygon, option_parser.steps(), kernel,
           option_parser.trivial(), option_parser.generic_step_frequency(), option_parser.sa_constant());
    return;
  }

  //use merge strategy
  if (option_parser.merge() > 0) {
    auto subset_size = option_parser.merge();
    auto splits = in.size()/subset_size;
    std::vector<Points> points_sets = std::vector<Points>(splits);
    Points glue_points = Points();
    auto it = in.begin();
    auto j = 0;
    for (size_t i = 0; i < splits; i++)
    {
      for (; j < (i + 1) * in.size() / splits; j++)
      {
        points_sets[i].push_back(*it);
        ++it;
      }
      if (i != splits - 1)
      {
        //guarentree valid merge
        while(true)
        {
          if (it->y() > (std::prev(it))->y() && it->y() > (std::next(it))->y()) break;
          points_sets[i].push_back(*it);
          ++it;
          ++j;
          if (j >= (i + 2) * in.size() / splits)
          {
            std::cout << "bad split" << std::endl;
            return;
          }
        }
        glue_points.push_back(*it);
        points_sets[i].push_back(*it);
      }
    }

    if (option_parser.verbose_level() == 3)
    {
      for (size_t i = 0; i < splits; i++)
      {
        for (auto it0 = points_sets[i].begin(); it0 != points_sets[i].end(); ++it0)
        {
          std::cout << it0->index();
        }
      }
    }


    std::vector<Permutation> half_convex_polygons = std::vector<Permutation>(splits);
    for (size_t i = 0; i < splits; i++)
    {
      half_convex_polygons[i] = Permutation(points_sets[i].size());
    }
    std::vector<Initial_polygon> initial_polygons;
    for (size_t i = 0; i < splits; i++)
    {
      OAP::half_convex_fp(points_sets[i].begin(), points_sets[i].end(), half_convex_polygons[i].begin(), kernel);
      initial_polygons.push_back({ "half_convex_" + std::to_string(i), &(half_convex_polygons[i]) });
    }

    std::vector<Permutation> results = std::vector<Permutation>(splits);

    //run SA on the splits
    for (size_t i = 0; i < splits; i++)
    {
      results[i] = run_sa(points_sets[i], initial_polygons[i], option_parser.steps(), kernel
        , 0, 2, option_parser.sa_constant(), false);
    }


    //merge the results
    auto res = results[0];
    auto res_points = points_sets[0];
    for (size_t i = 1; i < splits; i++)
    {
      res = glue(res, results[i], glue_points[i-1].index());
    }
    initial_polygons.push_back({ "merge", &res });
    //greedy run
    auto steps = option_parser.trivial();
    auto t2 = Clock::now();
    auto time = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
    run_sa(in, initial_polygons[splits], steps, kernel, 1, 0, option_parser.sa_constant(), true, true, time);
    return;
  }

  // Compute the bounding box:
  Ft min_x, min_y, max_x, max_y;
  OAP::bbox(in, min_x, min_y, max_x, max_y, kernel);
  /*std::cout << "Bounding box: (" << min_x << ", " << min_y << "), (" << max_x
    << ", " << max_y << ")" << std::endl;
  std::cout << "Bounding box area: " << (max_x - min_x) * (max_y - min_y)
    << std::endl;*/

  // get the permutation of the points that corrosponds to an initial star
  // polygon
  Permutation star_polygon(in.size());

  // get the permutation of the points that corrosponds to an initial
  // half-convex polygon
  Permutation half_convex_polygon(in.size());




  Permutation max_area_half_convex_polygon(in.size());

  std::vector<Initial_polygon> initial_polygons;
  if (option_parser.run_all_initial_polygons()) {
    //OAP::max_area_half_convex_fp(in.begin(), in.end(), max_area_half_convex_polygon.begin(), kernel);
    //initial_polygons.push_back({ "max_area_half_convex", &max_area_half_convex_polygon });
  }
  if (!option_parser.star_shaped_initial_polygon() || option_parser.run_all_initial_polygons())
  {
    OAP::half_convex_fp(in.begin(), in.end(), half_convex_polygon.begin(), kernel);
    initial_polygons.push_back({ "half_convex", &half_convex_polygon });
  }
  if(option_parser.star_shaped_initial_polygon() || option_parser.run_all_initial_polygons())
  {
    OAP::star_fp(in.begin(), in.end(), star_polygon.begin(), kernel);
    initial_polygons.push_back({ "star", &star_polygon });
  }

  for (const auto& initial_polygon : initial_polygons)
    run_sa(in, initial_polygon, option_parser.steps(), kernel, option_parser.trivial()
      ,option_parser.generic_step_frequency(), option_parser.sa_constant());
}

/*! Create the scene...
 */
void create() {
  if (0 == option_parser.num_input_files()) {
    std::string str("input file missing!");
    throw Input_file_missing_error(str);
    return;
  }
  const auto &filename = option_parser.input_file(0);
  std::string fullname = option_parser.find_input_file(filename);
  if (fullname.empty()) {
    throw Illegal_input(FILE_NOT_FOUND, "cannot find file", filename);
    return;
  }

  // Observe that long is insufficient for the larger samples in windows, as it
  // is defined to be 4 bytes.
  auto nt = option_parser.number_type();
  auto filtered_kernel = option_parser.filtered_kernel();
  if (option_parser.verbose_level() > 0)
  {
    std::cout << "Using number-type " << nt << std::endl;
    std::cout << "Using " << (filtered_kernel ? "" : "non-")
      << "filtered kernel" << std::endl;
  }

  // read input file
  std::string line;
  std::ifstream infile(fullname);
  std::getline(infile, line);
  std::getline(infile, line);

#if OAP_USE_LONG_LONG_NT
  if (Number_type::NT_LONG_LONG == nt) {
    if (filtered_kernel) {
      typedef OAP::Extended_filtered_kernel<long long>  Kernel;
      Kernel kernel;
      run(infile, kernel);
      return;
    }

    typedef OAP::Extended_kernel<long long>             Kernel;
    Kernel kernel;
    run(infile, kernel);
    return;
  }
#endif

#if OAP_USE_GMPZ_NT
  if (Number_type::NT_GMPZ == nt) {
    if (filtered_kernel) {
      typedef OAP::Extended_filtered_kernel<CGAL::Gmpz> Kernel;
      Kernel kernel;
      run(infile, kernel);
      return;
    }

    typedef OAP::Extended_kernel<CGAL::Gmpz>            Kernel;
    Kernel kernel;
    run(infile, kernel);
    return;
  }
#endif

#if OAP_USE_GMPQ_NT
  if (Number_type::NT_GMPQ == nt) {
    if (filtered_kernel) {
      typedef OAP::Extended_filtered_kernel<CGAL::Gmpq>  Kernel;
      Kernel kernel;
      run(infile, kernel);
      return;
    }

    typedef OAP::Extended_kernel<CGAL::Gmpq>            Kernel;
    Kernel kernel;
    run(infile, kernel);
    return;
  }
#endif

#if OAP_USE_DOUBLE_NT
  if (Number_type::NT_DOUBLE == nt) {
    if (filtered_kernel) {
      typedef OAP::Extended_filtered_kernel<double>     Kernel;
      Kernel kernel;
      run(infile, kernel);
      return;
    }

    typedef OAP::Extended_kernel<double>                Kernel;
    Kernel kernel;
    run(infile, kernel);
    return;
  }
#endif

  std::stringstream ss;
  ss << nt;
  std::string nt_str = ss.str();
  throw Illegal_input(UNSUPPORTED, "Unsupported number type", nt_str);
}

/*! Destroy the scene...
 */
void destroy() {}

/*! Clean the scene...
 */
void clean() {}

//! Main entry point.
int main(int argc, char *argv[]) {
  try {
    init(argc, argv);
  }
  catch (Option_parser::Generic_option_exception & /* e */) {
    return 0;
  }
  catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    std::cerr << "Try `" << argv[0] << " --help' for more information."
      << std::endl;
    return -1;
  }

  try {
    create();
  }
  catch (std::exception &e) {
    destroy();
    std::cerr << e.what() << std::endl;
    return -1;
  }
  clean();
  return 0;
}
