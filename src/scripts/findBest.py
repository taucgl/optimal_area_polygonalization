#!/usr/bin/python3

from __future__ import print_function
import sys
import os.path
import argparse
import re
import shutil

# Main function
if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Find best solutions.')
  parser.add_argument('dirname1', metavar="dirname1", nargs='?', default='best',
                      help='the directory with best solutions')
  parser.add_argument('dirname2', metavar="dirname2", nargs='?', default='out',
                      help='the directory with new solutions')
  parser.add_argument('dirname3', metavar="dirname3", nargs='?', default='pre',
                      help='the directory with better solutions')
  parser.add_argument('--op', help='The operation, either "max" or "min"')
  args = parser.parse_args()
  dirname1 = args.dirname1
  dirname2 = args.dirname2
  dirname3 = args.dirname3
  op = args.op

  print('Best solutions directory: ', dirname1)
  print('New solutions directory: ', dirname2)
  print('Better solutions directory: ', dirname3)
  if (op == 'max'):
    print('Maximal area')
  elif (op == 'min'):
    print('Minimal area')
  else:
    raise Exception('Invalid operation', op)
    exit()

  if (not os.path.exists(dirname1)):
    raise Exception('Directory of best results does not exist', dirname1)
    exit()
  if (not os.path.exists(dirname2)):
    raise Exception('Directory of new results does not exist', dirname2)
    exit()
  if (not os.path.exists(dirname3)):
    raise Exception('Directory of better results does not exist', dirname3)
    exit()

  cnt = 0
  for filename in os.listdir(dirname1):
    # print(filename)
    if not filename.endswith(".solution"):
      continue

    f1 = 0
    f2 = 0
    filename1 = os.path.join(dirname1, filename)
    # print(filename1)
    exists1 = os.path.isfile(filename1)
    if exists1:
      with open(filename1, 'r') as file1:
        for line1 in file1:
          m1 = re.search('ratio.*?(\d+\.\d+)', line1)
          if m1:
            f1 = float(m1.group(1))
            break

    filename2 = os.path.join(dirname2, filename)
    # print(filename2)
    exists2 = os.path.isfile(filename2)
    if exists2:
      with open(filename2, 'r') as file2:
        for line2 in file2:
          m2 = re.search('ratio.*?(\d+\.\d+)', line2)
          if m2:
            f2 = float(m2.group(1))
            break

    # print(f1, f2)
    if (op == 'max'):
      if (f2 > f1):
        cnt += 1
        print(f1, f2)
        shutil.copy2(filename2, dirname3)
    elif (op == 'min'):
      if (f2 < f1):
        cnt += 1
        print(f1, f2)
        shutil.copy2(filename2, dirname3)
    else:
      raise Exception('Error', 'Internal')
  print('Found files: ', cnt)
  exit()
