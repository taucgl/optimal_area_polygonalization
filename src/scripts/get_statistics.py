import os
import re

for variant in ["min", "max"]:
    path = os.getcwd()+r"/../../"+variant+r"/best"
    d = {}
    a = {}
    b = {}
    mySum = 0
    num = 0
    for filename in os.listdir(path):
        fullpath = path+'/'+filename
        if not ".solution" in filename: continue
        n = int(re.search(r"\d+", filename).group())
        # print(n)
        f = open(fullpath)
        f.readline()
        f.readline()
        f.readline()
        l = f.readline()
        pc = float(re.search(r"\d+\.\d+", l).group())
        # print(pc)
        if(n in d): d[n].append(pc)
        else: d[n] = [pc]
        mySum += pc
        num += 1

    for n in d:
        a[n] = min(d[n])
        b[n] = max(d[n])
        d[n] = sum(d[n])/len(d[n])
    f.close()
    key_min = min(a.keys(), key=(lambda k: a[k]))
    myMin = a[key_min]
    key_max = max(b.keys(), key=(lambda k: b[k]))
    myMax = b[key_max]

    f = open("statistics-"+variant+".csv", 'w')
    f.write("Index Count Min Average Max\n")
    i = 1;
    for n in sorted(d):
        l = str(i) + " " + str(n) + " " + str(a[n]) + " " + str(d[n]) + " " + str(b[n]) + "\n"
        f.write(l)
        i += 1
    f.close()
    print(mySum/100.0)
    print(myMin/100.0, mySum/(100.0*num), myMax/100.0)
