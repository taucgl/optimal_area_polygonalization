observations:
SA run time is linear in number of steps, independant of sample size
half_convex ~50%
max_area_half_convex ~50%
Post SA (steps at least 100 times input size) ~62% longer runs - better results
star shaped ~44%
Post SA (much slower) ~60%

the kd-tree query result is much larger than with half_convex(default) - this can be explained

results are similiar with almost all large samples from images

update: 20% gained when delta energy = %of ch area lost * input size (~70% with steps 1000 times input size)
on 50 points(uniform), was able to reach > 85% with 1M steps (about 1 minute run)
