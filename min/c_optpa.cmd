Universe   = vanilla
Executable = optpa

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000010.instance -g 2 -s 100000
Log       = min/log/euro-night-0000010.log
Output    = min/out/euro-night-0000010.solution
Error     = min/err/euro-night-0000010.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000015.instance -g 2 -s 100000
Log       = min/log/euro-night-0000015.log
Output    = min/out/euro-night-0000015.solution
Error     = min/err/euro-night-0000015.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000020.instance -g 2 -s 100000
Log       = min/log/euro-night-0000020.log
Output    = min/out/euro-night-0000020.solution
Error     = min/err/euro-night-0000020.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000025.instance -g 2 -s 100000
Log       = min/log/euro-night-0000025.log
Output    = min/out/euro-night-0000025.solution
Error     = min/err/euro-night-0000025.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000030.instance -g 2 -s 100000
Log       = min/log/euro-night-0000030.log
Output    = min/out/euro-night-0000030.solution
Error     = min/err/euro-night-0000030.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000035.instance -g 2 -s 100000
Log       = min/log/euro-night-0000035.log
Output    = min/out/euro-night-0000035.solution
Error     = min/err/euro-night-0000035.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000040.instance -g 2 -s 100000
Log       = min/log/euro-night-0000040.log
Output    = min/out/euro-night-0000040.solution
Error     = min/err/euro-night-0000040.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000045.instance -g 2 -s 100000
Log       = min/log/euro-night-0000045.log
Output    = min/out/euro-night-0000045.solution
Error     = min/err/euro-night-0000045.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000050.instance -g 2 -s 100000
Log       = min/log/euro-night-0000050.log
Output    = min/out/euro-night-0000050.solution
Error     = min/err/euro-night-0000050.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000060.instance -g 2 -s 100000
Log       = min/log/euro-night-0000060.log
Output    = min/out/euro-night-0000060.solution
Error     = min/err/euro-night-0000060.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000070.instance -g 2 -s 100000
Log       = min/log/euro-night-0000070.log
Output    = min/out/euro-night-0000070.solution
Error     = min/err/euro-night-0000070.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000080.instance -g 2 -s 100000
Log       = min/log/euro-night-0000080.log
Output    = min/out/euro-night-0000080.solution
Error     = min/err/euro-night-0000080.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000090.instance -g 2 -s 100000
Log       = min/log/euro-night-0000090.log
Output    = min/out/euro-night-0000090.solution
Error     = min/err/euro-night-0000090.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000100.instance -g 2 -s 100000
Log       = min/log/euro-night-0000100.log
Output    = min/out/euro-night-0000100.solution
Error     = min/err/euro-night-0000100.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000200.instance -g 2 -s 200000
Log       = min/log/euro-night-0000200.log
Output    = min/out/euro-night-0000200.solution
Error     = min/err/euro-night-0000200.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000300.instance -g 2 -s 300000
Log       = min/log/euro-night-0000300.log
Output    = min/out/euro-night-0000300.solution
Error     = min/err/euro-night-0000300.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000400.instance -g 2 -s 400000
Log       = min/log/euro-night-0000400.log
Output    = min/out/euro-night-0000400.solution
Error     = min/err/euro-night-0000400.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000500.instance -g 2 -s 500000
Log       = min/log/euro-night-0000500.log
Output    = min/out/euro-night-0000500.solution
Error     = min/err/euro-night-0000500.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000600.instance -m 300 -s 300000
Log       = min/log/euro-night-0000600.log
Output    = min/out/euro-night-0000600.solution
Error     = min/err/euro-night-0000600.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000700.instance -m 350 -s 350000
Log       = min/log/euro-night-0000700.log
Output    = min/out/euro-night-0000700.solution
Error     = min/err/euro-night-0000700.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000800.instance -m 400 -s 400000
Log       = min/log/euro-night-0000800.log
Output    = min/out/euro-night-0000800.solution
Error     = min/err/euro-night-0000800.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0000900.instance -m 450 -s 450000
Log       = min/log/euro-night-0000900.log
Output    = min/out/euro-night-0000900.solution
Error     = min/err/euro-night-0000900.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0001000.instance -m 500 -s 500000
Log       = min/log/euro-night-0001000.log
Output    = min/out/euro-night-0001000.solution
Error     = min/err/euro-night-0001000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0002000.instance -m 400 -s 400000
Log       = min/log/euro-night-0002000.log
Output    = min/out/euro-night-0002000.solution
Error     = min/err/euro-night-0002000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0003000.instance -m 300 -s 300000
Log       = min/log/euro-night-0003000.log
Output    = min/out/euro-night-0003000.solution
Error     = min/err/euro-night-0003000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0004000.instance -m 250 -s 250000
Log       = min/log/euro-night-0004000.log
Output    = min/out/euro-night-0004000.solution
Error     = min/err/euro-night-0004000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0005000.instance -m 200 -s 200000
Log       = min/log/euro-night-0005000.log
Output    = min/out/euro-night-0005000.solution
Error     = min/err/euro-night-0005000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0006000.instance -m 200 -s 20000
Log       = min/log/euro-night-0006000.log
Output    = min/out/euro-night-0006000.solution
Error     = min/err/euro-night-0006000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0007000.instance -m 200 -s 20000
Log       = min/log/euro-night-0007000.log
Output    = min/out/euro-night-0007000.solution
Error     = min/err/euro-night-0007000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0008000.instance -m 200 -s 20000
Log       = min/log/euro-night-0008000.log
Output    = min/out/euro-night-0008000.solution
Error     = min/err/euro-night-0008000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0009000.instance -m 200 -s 20000
Log       = min/log/euro-night-0009000.log
Output    = min/out/euro-night-0009000.solution
Error     = min/err/euro-night-0009000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0010000.instance -m 200 -s 20000
Log       = min/log/euro-night-0010000.log
Output    = min/out/euro-night-0010000.solution
Error     = min/err/euro-night-0010000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0020000.instance -m 200 -s 20000
Log       = min/log/euro-night-0020000.log
Output    = min/out/euro-night-0020000.solution
Error     = min/err/euro-night-0020000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0030000.instance -m 200 -s 20000
Log       = min/log/euro-night-0030000.log
Output    = min/out/euro-night-0030000.solution
Error     = min/err/euro-night-0030000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0040000.instance -m 200 -s 20000
Log       = min/log/euro-night-0040000.log
Output    = min/out/euro-night-0040000.solution
Error     = min/err/euro-night-0040000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0050000.instance -m 150 -s 20000
Log       = min/log/euro-night-0050000.log
Output    = min/out/euro-night-0050000.solution
Error     = min/err/euro-night-0050000.err
Queue

rem Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0060000.instance -m 150 -s 20000
rem Log       = min/log/euro-night-0060000.log
rem Output    = min/out/euro-night-0060000.solution
rem Error     = min/err/euro-night-0060000.err
rem Queue

Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0070000.instance -m 150 -s 20000
Log       = min/log/euro-night-0070000.log
Output    = min/out/euro-night-0070000.solution
Error     = min/err/euro-night-0070000.err
Queue

rem Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0080000.instance -m 150 -s 20000
rem Log       = min/log/euro-night-0080000.log
rem Output    = min/out/euro-night-0080000.solution
rem Error     = min/err/euro-night-0080000.err
rem Queue

rem Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0090000.instance -m 150 -s 20000
rem Log       = min/log/euro-night-0090000.log
rem Output    = min/out/euro-night-0090000.solution
rem Error     = min/err/euro-night-0090000.err
rem Queue

rem Arguments = -c -1 --input-path challenge_instances/data/images euro-night-0100000.instance -m 150 -s 20000
rem Log       = min/log/euro-night-0100000.log
rem Output    = min/out/euro-night-0100000.solution
rem Error     = min/err/euro-night-0100000.err
rem Queue

Arguments = -c -1 --input-path challenge_instances/data/images jupiter-0008000.instance -m 100 -s 100000
Log       = min/log/jupiter-0008000.log
Output    = min/out/jupiter-0008000.solution
Error     = min/err/jupiter-0008000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images jupiter-0009000.instance -m 100 -s 100000
Log       = min/log/jupiter-0009000.log
Output    = min/out/jupiter-0009000.solution
Error     = min/err/jupiter-0009000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images jupiter-0010000.instance -m 100 -s 100000
Log       = min/log/jupiter-0010000.log
Output    = min/out/jupiter-0010000.solution
Error     = min/err/jupiter-0010000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images jupiter-0020000.instance -m 100 -s 100000
Log       = min/log/jupiter-0020000.log
Output    = min/out/jupiter-0020000.solution
Error     = min/err/jupiter-0020000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images jupiter-0030000.instance -m 100 -s 100000
Log       = min/log/jupiter-0030000.log
Output    = min/out/jupiter-0030000.solution
Error     = min/err/jupiter-0030000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images jupiter-0040000.instance -m 100 -s 100000
Log       = min/log/jupiter-0040000.log
Output    = min/out/jupiter-0040000.solution
Error     = min/err/jupiter-0040000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000010.instance -g 2 -s 100000
Log       = min/log/london-0000010.log
Output    = min/out/london-0000010.solution
Error     = min/err/london-0000010.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000015.instance -g 2 -s 100000
Log       = min/log/london-0000015.log
Output    = min/out/london-0000015.solution
Error     = min/err/london-0000015.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000020.instance -g 2 -s 100000
Log       = min/log/london-0000020.log
Output    = min/out/london-0000020.solution
Error     = min/err/london-0000020.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000025.instance -g 2 -s 100000
Log       = min/log/london-0000025.log
Output    = min/out/london-0000025.solution
Error     = min/err/london-0000025.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000030.instance -g 2 -s 100000
Log       = min/log/london-0000030.log
Output    = min/out/london-0000030.solution
Error     = min/err/london-0000030.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000035.instance -g 2 -s 100000
Log       = min/log/london-0000035.log
Output    = min/out/london-0000035.solution
Error     = min/err/london-0000035.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000040.instance -g 2 -s 100000
Log       = min/log/london-0000040.log
Output    = min/out/london-0000040.solution
Error     = min/err/london-0000040.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000045.instance -g 2 -s 100000
Log       = min/log/london-0000045.log
Output    = min/out/london-0000045.solution
Error     = min/err/london-0000045.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000050.instance -g 2 -s 100000
Log       = min/log/london-0000050.log
Output    = min/out/london-0000050.solution
Error     = min/err/london-0000050.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000060.instance -g 2 -s 100000
Log       = min/log/london-0000060.log
Output    = min/out/london-0000060.solution
Error     = min/err/london-0000060.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000070.instance -g 2 -s 100000
Log       = min/log/london-0000070.log
Output    = min/out/london-0000070.solution
Error     = min/err/london-0000070.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000080.instance -g 2 -s 100000
Log       = min/log/london-0000080.log
Output    = min/out/london-0000080.solution
Error     = min/err/london-0000080.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000090.instance -g 2 -s 100000
Log       = min/log/london-0000090.log
Output    = min/out/london-0000090.solution
Error     = min/err/london-0000090.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images london-0000100.instance -g 2 -s 100000
Log       = min/log/london-0000100.log
Output    = min/out/london-0000100.solution
Error     = min/err/london-0000100.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images mona-lisa-1000000.instance -m 100 -s 10000
Log       = min/log/mona-lisa-1000000.log
Output    = min/out/mona-lisa-1000000.solution
Error     = min/err/mona-lisa-1000000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0000200.instance -g 2 -s 200000
Log       = min/log/paris-0000200.log
Output    = min/out/paris-0000200.solution
Error     = min/err/paris-0000200.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0000300.instance -g 2 -s 300000
Log       = min/log/paris-0000300.log
Output    = min/out/paris-0000300.solution
Error     = min/err/paris-0000300.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0000400.instance -g 2 -s 400000
Log       = min/log/paris-0000400.log
Output    = min/out/paris-0000400.solution
Error     = min/err/paris-0000400.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0000500.instance -g 2 -s 500000
Log       = min/log/paris-0000500.log
Output    = min/out/paris-0000500.solution
Error     = min/err/paris-0000500.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0000600.instance -m 300 -s 300000
Log       = min/log/paris-0000600.log
Output    = min/out/paris-0000600.solution
Error     = min/err/paris-0000600.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0000700.instance -m 350 -s 350000
Log       = min/log/paris-0000700.log
Output    = min/out/paris-0000700.solution
Error     = min/err/paris-0000700.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0000800.instance -m 400 -s 400000
Log       = min/log/paris-0000800.log
Output    = min/out/paris-0000800.solution
Error     = min/err/paris-0000800.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0000900.instance -m 450 -s 450000
Log       = min/log/paris-0000900.log
Output    = min/out/paris-0000900.solution
Error     = min/err/paris-0000900.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0001000.instance -m 500 -s 500000
Log       = min/log/paris-0001000.log
Output    = min/out/paris-0001000.solution
Error     = min/err/paris-0001000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0002000.instance -m 400 -s 400000
Log       = min/log/paris-0002000.log
Output    = min/out/paris-0002000.solution
Error     = min/err/paris-0002000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0003000.instance -m 300 -s 300000
Log       = min/log/paris-0003000.log
Output    = min/out/paris-0003000.solution
Error     = min/err/paris-0003000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0004000.instance -m 250 -s 250000
Log       = min/log/paris-0004000.log
Output    = min/out/paris-0004000.solution
Error     = min/err/paris-0004000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images paris-0005000.instance -m 200 -s 200000
Log       = min/log/paris-0005000.log
Output    = min/out/paris-0005000.solution
Error     = min/err/paris-0005000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images protein-0020000.instance -m 200 -s 20000
Log       = min/log/protein-0020000.log
Output    = min/out/protein-0020000.solution
Error     = min/err/protein-0020000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images protein-0030000.instance -m 200 -s 20000
Log       = min/log/protein-0030000.log
Output    = min/out/protein-0030000.solution
Error     = min/err/protein-0030000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images protein-0040000.instance -m 200 -s 20000
Log       = min/log/protein-0040000.log
Output    = min/out/protein-0040000.solution
Error     = min/err/protein-0040000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images protein-0050000.instance -m 200 -s 20000
Log       = min/log/protein-0050000.log
Output    = min/out/protein-0050000.solution
Error     = min/err/protein-0050000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images protein-0060000.instance -m 200 -s 20000
Log       = min/log/protein-0060000.log
Output    = min/out/protein-0060000.solution
Error     = min/err/protein-0060000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images protein-0070000.instance -m 200 -s 20000
Log       = min/log/protein-0070000.log
Output    = min/out/protein-0070000.solution
Error     = min/err/protein-0070000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images protein-0080000.instance -m 200 -s 20000
Log       = min/log/protein-0080000.log
Output    = min/out/protein-0080000.solution
Error     = min/err/protein-0080000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images protein-0090000.instance -m 200 -s 20000
Log       = min/log/protein-0090000.log
Output    = min/out/protein-0090000.solution
Error     = min/err/protein-0090000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images protein-0100000.instance -m 200 -s 20000
Log       = min/log/protein-0100000.log
Output    = min/out/protein-0100000.solution
Error     = min/err/protein-0100000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images skylake-0001000.instance -m 500 -s 500000
Log       = min/log/skylake-0001000.log
Output    = min/out/skylake-0001000.solution
Error     = min/err/skylake-0001000.err
Queue

rem Arguments = -c -1 --input-path challenge_instances/data/images skylake-0002000.instance -m 400 -s 400000
rem Log       = min/log/skylake-0002000.log
rem Output    = min/out/skylake-0002000.solution
rem Error     = min/err/skylake-0002000.err
rem Queue

Arguments = -c -1 --input-path challenge_instances/data/images skylake-0003000.instance -m 300 -s 300000
Log       = min/log/skylake-0003000.log
Output    = min/out/skylake-0003000.solution
Error     = min/err/skylake-0003000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images skylake-0004000.instance -m 300 -s 250000
Log       = min/log/skylake-0004000.log
Output    = min/out/skylake-0004000.solution
Error     = min/err/skylake-0004000.err
Queue

rem Arguments = -c -1 --input-path challenge_instances/data/images skylake-0005000.instance -m 300 -s 200000
rem Log       = min/log/skylake-0005000.log
rem Output    = min/out/skylake-0005000.solution
rem Error     = min/err/skylake-0005000.err
rem Queue

Arguments = -c -1 --input-path challenge_instances/data/images skylake-0006000.instance -m 300 -s 200000
Log       = min/log/skylake-0006000.log
Output    = min/out/skylake-0006000.solution
Error     = min/err/skylake-0006000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images skylake-0007000.instance -m 300 -s 200000
Log       = min/log/skylake-0007000.log
Output    = min/out/skylake-0007000.solution
Error     = min/err/skylake-0007000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000010.instance -g 2 -s 100000
Log       = min/log/stars-0000010.log
Output    = min/out/stars-0000010.solution
Error     = min/err/stars-0000010.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000015.instance -g 2 -s 100000
Log       = min/log/stars-0000015.log
Output    = min/out/stars-0000015.solution
Error     = min/err/stars-0000015.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000020.instance -g 2 -s 100000
Log       = min/log/stars-0000020.log
Output    = min/out/stars-0000020.solution
Error     = min/err/stars-0000020.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000025.instance -g 2 -s 100000
Log       = min/log/stars-0000025.log
Output    = min/out/stars-0000025.solution
Error     = min/err/stars-0000025.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000030.instance -g 2 -s 100000
Log       = min/log/stars-0000030.log
Output    = min/out/stars-0000030.solution
Error     = min/err/stars-0000030.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000035.instance -g 2 -s 100000
Log       = min/log/stars-0000035.log
Output    = min/out/stars-0000035.solution
Error     = min/err/stars-0000035.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000040.instance -g 2 -s 100000
Log       = min/log/stars-0000040.log
Output    = min/out/stars-0000040.solution
Error     = min/err/stars-0000040.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000045.instance -g 2 -s 100000
Log       = min/log/stars-0000045.log
Output    = min/out/stars-0000045.solution
Error     = min/err/stars-0000045.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000050.instance -g 2 -s 100000
Log       = min/log/stars-0000050.log
Output    = min/out/stars-0000050.solution
Error     = min/err/stars-0000050.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000060.instance -g 2 -s 100000
Log       = min/log/stars-0000060.log
Output    = min/out/stars-0000060.solution
Error     = min/err/stars-0000060.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000070.instance -g 2 -s 100000
Log       = min/log/stars-0000070.log
Output    = min/out/stars-0000070.solution
Error     = min/err/stars-0000070.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000080.instance -g 2 -s 100000
Log       = min/log/stars-0000080.log
Output    = min/out/stars-0000080.solution
Error     = min/err/stars-0000080.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000090.instance -g 2 -s 100000
Log       = min/log/stars-0000090.log
Output    = min/out/stars-0000090.solution
Error     = min/err/stars-0000090.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000100.instance -g 2 -s 100000
Log       = min/log/stars-0000100.log
Output    = min/out/stars-0000100.solution
Error     = min/err/stars-0000100.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000200.instance -g 2 -s 200000
Log       = min/log/stars-0000200.log
Output    = min/out/stars-0000200.solution
Error     = min/err/stars-0000200.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000300.instance -g 2 -s 300000
Log       = min/log/stars-0000300.log
Output    = min/out/stars-0000300.solution
Error     = min/err/stars-0000300.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000400.instance -g 2 -s 400000
Log       = min/log/stars-0000400.log
Output    = min/out/stars-0000400.solution
Error     = min/err/stars-0000400.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000500.instance -g 2 -s 500000
Log       = min/log/stars-0000500.log
Output    = min/out/stars-0000500.solution
Error     = min/err/stars-0000500.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000600.instance -m 300 -s 300000
Log       = min/log/stars-0000600.log
Output    = min/out/stars-0000600.solution
Error     = min/err/stars-0000600.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000700.instance -m 350 -s 350000
Log       = min/log/stars-0000700.log
Output    = min/out/stars-0000700.solution
Error     = min/err/stars-0000700.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000800.instance -m 400 -s 400000
Log       = min/log/stars-0000800.log
Output    = min/out/stars-0000800.solution
Error     = min/err/stars-0000800.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images stars-0000900.instance -m 500 -s 500000
Log       = min/log/stars-0000900.log
Output    = min/out/stars-0000900.solution
Error     = min/err/stars-0000900.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images sun-0050000.instance -m 200 -s 20000
Log       = min/log/sun-0050000.log
Output    = min/out/sun-0050000.solution
Error     = min/err/sun-0050000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images sun-0060000.instance -m 150 -s 20000
Log       = min/log/sun-0060000.log
Output    = min/out/sun-0060000.solution
Error     = min/err/sun-0060000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images sun-0070000.instance -m 150 -s 20000
Log       = min/log/sun-0070000.log
Output    = min/out/sun-0070000.solution
Error     = min/err/sun-0070000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images sun-0080000.instance -m 150 -s 20000
Log       = min/log/sun-0080000.log
Output    = min/out/sun-0080000.solution
Error     = min/err/sun-0080000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images sun-0090000.instance -m 150 -s 20000
Log       = min/log/sun-0090000.log
Output    = min/out/sun-0090000.solution
Error     = min/err/sun-0090000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images sun-0100000.instance -m 150 -s 20000
Log       = min/log/sun-0100000.log
Output    = min/out/sun-0100000.solution
Error     = min/err/sun-0100000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000010-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000010-1.log
Output    = min/out/uniform-0000010-1.solution
Error     = min/err/uniform-0000010-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000010-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000010-2.log
Output    = min/out/uniform-0000010-2.solution
Error     = min/err/uniform-0000010-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000015-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000015-1.log
Output    = min/out/uniform-0000015-1.solution
Error     = min/err/uniform-0000015-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000015-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000015-2.log
Output    = min/out/uniform-0000015-2.solution
Error     = min/err/uniform-0000015-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000020-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000020-1.log
Output    = min/out/uniform-0000020-1.solution
Error     = min/err/uniform-0000020-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000020-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000020-2.log
Output    = min/out/uniform-0000020-2.solution
Error     = min/err/uniform-0000020-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000025-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000025-1.log
Output    = min/out/uniform-0000025-1.solution
Error     = min/err/uniform-0000025-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000025-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000025-2.log
Output    = min/out/uniform-0000025-2.solution
Error     = min/err/uniform-0000025-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000030-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000030-1.log
Output    = min/out/uniform-0000030-1.solution
Error     = min/err/uniform-0000030-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000030-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000030-2.log
Output    = min/out/uniform-0000030-2.solution
Error     = min/err/uniform-0000030-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000035-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000035-1.log
Output    = min/out/uniform-0000035-1.solution
Error     = min/err/uniform-0000035-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000035-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000035-2.log
Output    = min/out/uniform-0000035-2.solution
Error     = min/err/uniform-0000035-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000040-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000040-1.log
Output    = min/out/uniform-0000040-1.solution
Error     = min/err/uniform-0000040-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000040-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000040-2.log
Output    = min/out/uniform-0000040-2.solution
Error     = min/err/uniform-0000040-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000045-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000045-1.log
Output    = min/out/uniform-0000045-1.solution
Error     = min/err/uniform-0000045-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000045-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000045-2.log
Output    = min/out/uniform-0000045-2.solution
Error     = min/err/uniform-0000045-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000050-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000050-1.log
Output    = min/out/uniform-0000050-1.solution
Error     = min/err/uniform-0000050-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000050-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000050-2.log
Output    = min/out/uniform-0000050-2.solution
Error     = min/err/uniform-0000050-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000060-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000060-1.log
Output    = min/out/uniform-0000060-1.solution
Error     = min/err/uniform-0000060-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000060-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000060-2.log
Output    = min/out/uniform-0000060-2.solution
Error     = min/err/uniform-0000060-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000070-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000070-1.log
Output    = min/out/uniform-0000070-1.solution
Error     = min/err/uniform-0000070-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000070-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000070-2.log
Output    = min/out/uniform-0000070-2.solution
Error     = min/err/uniform-0000070-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000080-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000080-1.log
Output    = min/out/uniform-0000080-1.solution
Error     = min/err/uniform-0000080-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000080-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000080-2.log
Output    = min/out/uniform-0000080-2.solution
Error     = min/err/uniform-0000080-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000090-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000090-1.log
Output    = min/out/uniform-0000090-1.solution
Error     = min/err/uniform-0000090-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000090-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000090-2.log
Output    = min/out/uniform-0000090-2.solution
Error     = min/err/uniform-0000090-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000100-1.instance -g 2 -s 100000
Log       = min/log/uniform-0000100-1.log
Output    = min/out/uniform-0000100-1.solution
Error     = min/err/uniform-0000100-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000100-2.instance -g 2 -s 100000
Log       = min/log/uniform-0000100-2.log
Output    = min/out/uniform-0000100-2.solution
Error     = min/err/uniform-0000100-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000200-1.instance -g 2 -s 200000
Log       = min/log/uniform-0000200-1.log
Output    = min/out/uniform-0000200-1.solution
Error     = min/err/uniform-0000200-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000200-2.instance -g 2 -s 200000
Log       = min/log/uniform-0000200-2.log
Output    = min/out/uniform-0000200-2.solution
Error     = min/err/uniform-0000200-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000300-1.instance -g 2 -s 300000
Log       = min/log/uniform-0000300-1.log
Output    = min/out/uniform-0000300-1.solution
Error     = min/err/uniform-0000300-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000300-2.instance -g 2 -s 300000
Log       = min/log/uniform-0000300-2.log
Output    = min/out/uniform-0000300-2.solution
Error     = min/err/uniform-0000300-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000400-1.instance -g 2 -s 400000
Log       = min/log/uniform-0000400-1.log
Output    = min/out/uniform-0000400-1.solution
Error     = min/err/uniform-0000400-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000400-2.instance -g 2 -s 400000
Log       = min/log/uniform-0000400-2.log
Output    = min/out/uniform-0000400-2.solution
Error     = min/err/uniform-0000400-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000500-1.instance -g 2 -s 500000
Log       = min/log/uniform-0000500-1.log
Output    = min/out/uniform-0000500-1.solution
Error     = min/err/uniform-0000500-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000500-2.instance -g 2 -s 500000
Log       = min/log/uniform-0000500-2.log
Output    = min/out/uniform-0000500-2.solution
Error     = min/err/uniform-0000500-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000600-1.instance -m 300 -s 300000
Log       = min/log/uniform-0000600-1.log
Output    = min/out/uniform-0000600-1.solution
Error     = min/err/uniform-0000600-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000600-2.instance -m 300 -s 300000
Log       = min/log/uniform-0000600-2.log
Output    = min/out/uniform-0000600-2.solution
Error     = min/err/uniform-0000600-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000700-1.instance -m 350 -s 350000
Log       = min/log/uniform-0000700-1.log
Output    = min/out/uniform-0000700-1.solution
Error     = min/err/uniform-0000700-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000700-2.instance -m 350 -s 350000
Log       = min/log/uniform-0000700-2.log
Output    = min/out/uniform-0000700-2.solution
Error     = min/err/uniform-0000700-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000800-1.instance -m 400 -s 400000
Log       = min/log/uniform-0000800-1.log
Output    = min/out/uniform-0000800-1.solution
Error     = min/err/uniform-0000800-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000800-2.instance -m 400 -s 400000
Log       = min/log/uniform-0000800-2.log
Output    = min/out/uniform-0000800-2.solution
Error     = min/err/uniform-0000800-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000900-1.instance -m 450 -s 450000
Log       = min/log/uniform-0000900-1.log
Output    = min/out/uniform-0000900-1.solution
Error     = min/err/uniform-0000900-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0000900-2.instance -m 450 -s 450000
Log       = min/log/uniform-0000900-2.log
Output    = min/out/uniform-0000900-2.solution
Error     = min/err/uniform-0000900-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0001000-1.instance -m 500 -s 500000
Log       = min/log/uniform-0001000-1.log
Output    = min/out/uniform-0001000-1.solution
Error     = min/err/uniform-0001000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0001000-2.instance -m 500 -s 500000
Log       = min/log/uniform-0001000-2.log
Output    = min/out/uniform-0001000-2.solution
Error     = min/err/uniform-0001000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0002000-1.instance -m 400 -s 400000
Log       = min/log/uniform-0002000-1.log
Output    = min/out/uniform-0002000-1.solution
Error     = min/err/uniform-0002000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0002000-2.instance -m 400 -s 400000
Log       = min/log/uniform-0002000-2.log
Output    = min/out/uniform-0002000-2.solution
Error     = min/err/uniform-0002000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0003000-1.instance -m 300 -s 300000
Log       = min/log/uniform-0003000-1.log
Output    = min/out/uniform-0003000-1.solution
Error     = min/err/uniform-0003000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0003000-2.instance -m 300 -s 300000
Log       = min/log/uniform-0003000-2.log
Output    = min/out/uniform-0003000-2.solution
Error     = min/err/uniform-0003000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0004000-1.instance -m 200 -s 200000
Log       = min/log/uniform-0004000-1.log
Output    = min/out/uniform-0004000-1.solution
Error     = min/err/uniform-0004000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0004000-2.instance -m 200 -s 200000
Log       = min/log/uniform-0004000-2.log
Output    = min/out/uniform-0004000-2.solution
Error     = min/err/uniform-0004000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0005000-1.instance -m 200 -s 100000
Log       = min/log/uniform-0005000-1.log
Output    = min/out/uniform-0005000-1.solution
Error     = min/err/uniform-0005000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0005000-2.instance -m 200 -s 100000
Log       = min/log/uniform-0005000-2.log
Output    = min/out/uniform-0005000-2.solution
Error     = min/err/uniform-0005000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0006000-1.instance -m 200 -s 50000
Log       = min/log/uniform-0006000-1.log
Output    = min/out/uniform-0006000-1.solution
Error     = min/err/uniform-0006000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0006000-2.instance -m 200 -s 50000
Log       = min/log/uniform-0006000-2.log
Output    = min/out/uniform-0006000-2.solution
Error     = min/err/uniform-0006000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0007000-1.instance -m 200 -s 40000
Log       = min/log/uniform-0007000-1.log
Output    = min/out/uniform-0007000-1.solution
Error     = min/err/uniform-0007000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0007000-2.instance -m 200 -s 40000
Log       = min/log/uniform-0007000-2.log
Output    = min/out/uniform-0007000-2.solution
Error     = min/err/uniform-0007000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0008000-1.instance -m 200 -s 40000
Log       = min/log/uniform-0008000-1.log
Output    = min/out/uniform-0008000-1.solution
Error     = min/err/uniform-0008000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0008000-2.instance -m 200 -s 40000
Log       = min/log/uniform-0008000-2.log
Output    = min/out/uniform-0008000-2.solution
Error     = min/err/uniform-0008000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0009000-1.instance -m 200 -s 30000
Log       = min/log/uniform-0009000-1.log
Output    = min/out/uniform-0009000-1.solution
Error     = min/err/uniform-0009000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0009000-2.instance -m 200 -s 30000
Log       = min/log/uniform-0009000-2.log
Output    = min/out/uniform-0009000-2.solution
Error     = min/err/uniform-0009000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0010000-1.instance -m 200 -s 20000
Log       = min/log/uniform-0010000-1.log
Output    = min/out/uniform-0010000-1.solution
Error     = min/err/uniform-0010000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0010000-2.instance -m 200 -s 20000
Log       = min/log/uniform-0010000-2.log
Output    = min/out/uniform-0010000-2.solution
Error     = min/err/uniform-0010000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0020000-1.instance -m 200 -s 20000
Log       = min/log/uniform-0020000-1.log
Output    = min/out/uniform-0020000-1.solution
Error     = min/err/uniform-0020000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0020000-2.instance -m 200 -s 20000
Log       = min/log/uniform-0020000-2.log
Output    = min/out/uniform-0020000-2.solution
Error     = min/err/uniform-0020000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0030000-1.instance -m 200 -s 20000
Log       = min/log/uniform-0030000-1.log
Output    = min/out/uniform-0030000-1.solution
Error     = min/err/uniform-0030000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0030000-2.instance -m 200 -s 20000
Log       = min/log/uniform-0030000-2.log
Output    = min/out/uniform-0030000-2.solution
Error     = min/err/uniform-0030000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0040000-1.instance -m 200 -s 20000
Log       = min/log/uniform-0040000-1.log
Output    = min/out/uniform-0040000-1.solution
Error     = min/err/uniform-0040000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0040000-2.instance -m 200 -s 20000
Log       = min/log/uniform-0040000-2.log
Output    = min/out/uniform-0040000-2.solution
Error     = min/err/uniform-0040000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0050000-1.instance -m 200 -s 20000
Log       = min/log/uniform-0050000-1.log
Output    = min/out/uniform-0050000-1.solution
Error     = min/err/uniform-0050000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0050000-2.instance -m 150 -s 20000
Log       = min/log/uniform-0050000-2.log
Output    = min/out/uniform-0050000-2.solution
Error     = min/err/uniform-0050000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0060000-1.instance -m 150 -s 20000
Log       = min/log/uniform-0060000-1.log
Output    = min/out/uniform-0060000-1.solution
Error     = min/err/uniform-0060000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0060000-2.instance -m 150 -s 20000
Log       = min/log/uniform-0060000-2.log
Output    = min/out/uniform-0060000-2.solution
Error     = min/err/uniform-0060000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0070000-1.instance -m 150 -s 20000
Log       = min/log/uniform-0070000-1.log
Output    = min/out/uniform-0070000-1.solution
Error     = min/err/uniform-0070000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0070000-2.instance -m 150 -s 20000
Log       = min/log/uniform-0070000-2.log
Output    = min/out/uniform-0070000-2.solution
Error     = min/err/uniform-0070000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0080000-1.instance -m 150 -s 20000
Log       = min/log/uniform-0080000-1.log
Output    = min/out/uniform-0080000-1.solution
Error     = min/err/uniform-0080000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0080000-2.instance -m 150 -s 20000
Log       = min/log/uniform-0080000-2.log
Output    = min/out/uniform-0080000-2.solution
Error     = min/err/uniform-0080000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0090000-1.instance -m 150 -s 20000
Log       = min/log/uniform-0090000-1.log
Output    = min/out/uniform-0090000-1.solution
Error     = min/err/uniform-0090000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0090000-2.instance -m 150 -s 20000
Log       = min/log/uniform-0090000-2.log
Output    = min/out/uniform-0090000-2.solution
Error     = min/err/uniform-0090000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0100000-1.instance -m 150 -s 20000
Log       = min/log/uniform-0100000-1.log
Output    = min/out/uniform-0100000-1.solution
Error     = min/err/uniform-0100000-1.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/uniform uniform-0100000-2.instance -m 150 -s 20000
Log       = min/log/uniform-0100000-2.log
Output    = min/out/uniform-0100000-2.solution
Error     = min/err/uniform-0100000-2.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000010.instance -g 2 -s 100000
Log       = min/log/us-night-0000010.log
Output    = min/out/us-night-0000010.solution
Error     = min/err/us-night-0000010.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000015.instance -g 2 -s 100000
Log       = min/log/us-night-0000015.log
Output    = min/out/us-night-0000015.solution
Error     = min/err/us-night-0000015.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000020.instance -g 2 -s 100000
Log       = min/log/us-night-0000020.log
Output    = min/out/us-night-0000020.solution
Error     = min/err/us-night-0000020.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000025.instance -g 2 -s 100000
Log       = min/log/us-night-0000025.log
Output    = min/out/us-night-0000025.solution
Error     = min/err/us-night-0000025.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000030.instance -g 2 -s 100000
Log       = min/log/us-night-0000030.log
Output    = min/out/us-night-0000030.solution
Error     = min/err/us-night-0000030.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000035.instance -g 2 -s 100000
Log       = min/log/us-night-0000035.log
Output    = min/out/us-night-0000035.solution
Error     = min/err/us-night-0000035.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000040.instance -g 2 -s 100000
Log       = min/log/us-night-0000040.log
Output    = min/out/us-night-0000040.solution
Error     = min/err/us-night-0000040.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000045.instance -g 2 -s 100000
Log       = min/log/us-night-0000045.log
Output    = min/out/us-night-0000045.solution
Error     = min/err/us-night-0000045.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000050.instance -g 2 -s 100000
Log       = min/log/us-night-0000050.log
Output    = min/out/us-night-0000050.solution
Error     = min/err/us-night-0000050.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000060.instance -g 2 -s 100000
Log       = min/log/us-night-0000060.log
Output    = min/out/us-night-0000060.solution
Error     = min/err/us-night-0000060.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000070.instance -g 2 -s 100000
Log       = min/log/us-night-0000070.log
Output    = min/out/us-night-0000070.solution
Error     = min/err/us-night-0000070.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000080.instance -g 2 -s 100000
Log       = min/log/us-night-0000080.log
Output    = min/out/us-night-0000080.solution
Error     = min/err/us-night-0000080.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000090.instance -g 2 -s 100000
Log       = min/log/us-night-0000090.log
Output    = min/out/us-night-0000090.solution
Error     = min/err/us-night-0000090.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000100.instance -g 2 -s 100000
Log       = min/log/us-night-0000100.log
Output    = min/out/us-night-0000100.solution
Error     = min/err/us-night-0000100.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000200.instance -g 2 -s 200000
Log       = min/log/us-night-0000200.log
Output    = min/out/us-night-0000200.solution
Error     = min/err/us-night-0000200.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000300.instance -g 2 -s 300000
Log       = min/log/us-night-0000300.log
Output    = min/out/us-night-0000300.solution
Error     = min/err/us-night-0000300.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000400.instance -g 2 -s 400000
Log       = min/log/us-night-0000400.log
Output    = min/out/us-night-0000400.solution
Error     = min/err/us-night-0000400.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000500.instance -g 2 -s 500000
Log       = min/log/us-night-0000500.log
Output    = min/out/us-night-0000500.solution
Error     = min/err/us-night-0000500.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000600.instance -m 300 -s 300000
Log       = min/log/us-night-0000600.log
Output    = min/out/us-night-0000600.solution
Error     = min/err/us-night-0000600.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000700.instance -m 350 -s 350000
Log       = min/log/us-night-0000700.log
Output    = min/out/us-night-0000700.solution
Error     = min/err/us-night-0000700.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000800.instance -m 400 -s 400000
Log       = min/log/us-night-0000800.log
Output    = min/out/us-night-0000800.solution
Error     = min/err/us-night-0000800.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0000900.instance -m 450 -s 450000
Log       = min/log/us-night-0000900.log
Output    = min/out/us-night-0000900.solution
Error     = min/err/us-night-0000900.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0001000.instance -m 500 -s 500000
Log       = min/log/us-night-0001000.log
Output    = min/out/us-night-0001000.solution
Error     = min/err/us-night-0001000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0002000.instance -m 400 -s 400000
Log       = min/log/us-night-0002000.log
Output    = min/out/us-night-0002000.solution
Error     = min/err/us-night-0002000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0003000.instance -m 300 -s 300000
Log       = min/log/us-night-0003000.log
Output    = min/out/us-night-0003000.solution
Error     = min/err/us-night-0003000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0004000.instance -m 250 -s 250000
Log       = min/log/us-night-0004000.log
Output    = min/out/us-night-0004000.solution
Error     = min/err/us-night-0004000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0005000.instance -m 250 -s 200000
Log       = min/log/us-night-0005000.log
Output    = min/out/us-night-0005000.solution
Error     = min/err/us-night-0005000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0006000.instance -m 250 -s 150000
Log       = min/log/us-night-0006000.log
Output    = min/out/us-night-0006000.solution
Error     = min/err/us-night-0006000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0007000.instance -m 250 -s 150000
Log       = min/log/us-night-0007000.log
Output    = min/out/us-night-0007000.solution
Error     = min/err/us-night-0007000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0008000.instance -m 250 -s 150000
Log       = min/log/us-night-0008000.log
Output    = min/out/us-night-0008000.solution
Error     = min/err/us-night-0008000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0009000.instance -m 250 -s 100000
Log       = min/log/us-night-0009000.log
Output    = min/out/us-night-0009000.solution
Error     = min/err/us-night-0009000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0010000.instance -m 250 -s 50000
Log       = min/log/us-night-0010000.log
Output    = min/out/us-night-0010000.solution
Error     = min/err/us-night-0010000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0020000.instance -m 250 -s 50000
Log       = min/log/us-night-0020000.log
Output    = min/out/us-night-0020000.solution
Error     = min/err/us-night-0020000.err
Queue

rem Arguments = -c -1 --input-path challenge_instances/data/images us-night-0030000.instance -m 250 -s 20000
rem Log       = min/log/us-night-0030000.log
rem Output    = min/out/us-night-0030000.solution
rem Error     = min/err/us-night-0030000.err
rem Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0040000.instance -m 250 -s 20000
Log       = min/log/us-night-0040000.log
Output    = min/out/us-night-0040000.solution
Error     = min/err/us-night-0040000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0050000.instance -m 200 -s 20000
Log       = min/log/us-night-0050000.log
Output    = min/out/us-night-0050000.solution
Error     = min/err/us-night-0050000.err
Queue

rem Arguments = -c -1 --input-path challenge_instances/data/images us-night-0060000.instance -m 150 -s 20000
rem Log       = min/log/us-night-0060000.log
rem Output    = min/out/us-night-0060000.solution
rem Error     = min/err/us-night-0060000.err
rem Queue

rem Arguments = -c -1 --input-path challenge_instances/data/images us-night-0070000.instance -m 150 -s 20000
rem Log       = min/log/us-night-0070000.log
rem Output    = min/out/us-night-0070000.solution
rem Error     = min/err/us-night-0070000.err
rem Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0080000.instance -m 150 -s 20000
Log       = min/log/us-night-0080000.log
Output    = min/out/us-night-0080000.solution
Error     = min/err/us-night-0080000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0090000.instance -m 150 -s 20000
Log       = min/log/us-night-0090000.log
Output    = min/out/us-night-0090000.solution
Error     = min/err/us-night-0090000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images us-night-0100000.instance -m 150 -s 20000
Log       = min/log/us-night-0100000.log
Output    = min/out/us-night-0100000.solution
Error     = min/err/us-night-0100000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images world-0006000.instance -m 250 -s 250000
Log       = min/log/world-0006000.log
Output    = min/out/world-0006000.solution
Error     = min/err/world-0006000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images world-0007000.instance -m 250 -s 250000
Log       = min/log/world-0007000.log
Output    = min/out/world-0007000.solution
Error     = min/err/world-0007000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images world-0008000.instance -m 250 -s 25000
Log       = min/log/world-0008000.log
Output    = min/out/world-0008000.solution
Error     = min/err/world-0008000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images world-0009000.instance -m 250 -s 250000
Log       = min/log/world-0009000.log
Output    = min/out/world-0009000.solution
Error     = min/err/world-0009000.err
Queue

Arguments = -c -1 --input-path challenge_instances/data/images world-0010000.instance -m 250 -s 250000
Log       = min/log/world-0010000.log
Output    = min/out/world-0010000.solution
Error     = min/err/world-0010000.err
Queue
